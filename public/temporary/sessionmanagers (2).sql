-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 23, 2018 at 04:26 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `feemgmt`
--

-- --------------------------------------------------------

--
-- Table structure for table `sessionmanagers`
--

CREATE TABLE `sessionmanagers` (
  `id` int(11) NOT NULL,
  `session_period` varchar(190) NOT NULL,
  `status` varchar(500) DEFAULT '-',
  `description` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sessionmanagers`
--

INSERT INTO `sessionmanagers` (`id`, `session_period`, `status`, `description`, `created_at`, `updated_at`) VALUES
(1, '2017-2018', 'active', 'first session', '2017-08-31 14:47:44', '2018-02-20 21:19:06'),
(2, '2018-2019', '-', 'second session', '2017-08-31 14:48:05', '2017-08-31 14:48:05'),
(3, '2019-2020', '-', 'N.A.', '2018-02-20 21:15:35', '2018-02-20 21:19:06'),
(4, '2020-2021', '-', 'comment NA', '2018-02-20 21:20:04', '2018-02-20 21:20:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sessionmanagers`
--
ALTER TABLE `sessionmanagers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `session` (`session_period`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sessionmanagers`
--
ALTER TABLE `sessionmanagers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

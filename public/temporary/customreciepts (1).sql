-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 23, 2018 at 04:25 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `feemgmt`
--

-- --------------------------------------------------------

--
-- Table structure for table `customreciepts`
--

CREATE TABLE `customreciepts` (
  `id` int(11) NOT NULL,
  `admission_no` text,
  `class_section` text,
  `student_name` text,
  `father_name` text,
  `mother_name` text,
  `pay` text,
  `fee_for` text,
  `mobile` text,
  `email` text,
  `dob` text,
  `address` text,
  `gender` text,
  `fee_name1` text,
  `fee_value1` text,
  `fee_name2` text,
  `fee_value2` text,
  `fee_name3` text,
  `fee_value3` text,
  `fee_name4` text,
  `fee_value4` text,
  `fee_name5` text,
  `fee_value5` text,
  `remark` text,
  `parent_sessionid` text,
  `sessionid` text,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customreciepts`
--
ALTER TABLE `customreciepts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customreciepts`
--
ALTER TABLE `customreciepts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

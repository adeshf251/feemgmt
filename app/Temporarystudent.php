<?php

namespace DELLMOND;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class Temporarystudent extends Model
{
    public function getDates()
    {
        return ['created_at', 'updated_at', 'dob'];
    }

    /**
     * @return string
     */
    public function getCreatedAtAttribute()
    {
        return  Carbon::parse($this->attributes['created_at'])->diffForHumans();
    }

    public function getDobAttribute()
    {
        return  Carbon::parse($this->attributes['dob'])->format('d-m-Y');
        // return  Carbon::parse($this->attributes['dob'])->format('l jS \\of F Y h:i:s A');
    }

    /**
     * @return string
     */
    public function getUpdatedAtAttribute()
    {
        return  Carbon::parse($this->attributes['updated_at'])->format('Y-m-d');
    }
}

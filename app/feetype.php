<?php

namespace DELLMOND;

use Illuminate\Database\Eloquent\Model;

class feetype extends Model
{
    public $fillable = ['fee_type','description','once_required','jan','feb','march','april', 'may','june','july','aug','sep','oct','nov','dec'];
}

<?php

namespace DELLMOND;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class feereciept_student extends Model
{
    public function getDates()
    {
        return ['created_at', 'updated_at'];
    }

    /**
     * @return string
     */
    public function getCreatedAtAttribute()
    {
        return  Carbon::parse($this->attributes['created_at'])->diffForHumans();
    }

    /**
     * @return string
     */
    public function getUpdatedAtAttribute()
    {
        return  Carbon::parse($this->attributes['updated_at'])->format('d-m-Y');
    }
}

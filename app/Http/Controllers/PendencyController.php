<?php

namespace DELLMOND\Http\Controllers;
use DELLMOND\Http\Requests;
use DELLMOND\User;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Input;
use DELLMOND\classmgmt;
use DELLMOND\feeamount;
use DELLMOND\feetype;
use DELLMOND\student;
use DELLMOND\CollectionModel;
use DELLMOND\feereciept_student;
use DELLMOND\feereciept_sc;
use DB;
use Excel;


class PendencyController extends Controller
{
    public function monthwise_pendency_index()
    {
    	
        $options = array(array('jan', 'feb', 'march', 'april', 'may', 'june', 'july', 'aug', 'sep', 'oct', 'nov', 'dec'));
        //$options = \DELLMOND\CollectionModel::get_submitted_fee_months();

		return view('pendency.monthwise_pendency_form')->with([
	            'options' => $options,
	            ]);
    }

    public function monthwise_pendency_show()
    {
        $sessionid                  =       Session('valid_id');

        $field = Input::all();
        $month          = isset($field['month'])            ?   $field['month']             : 'april';
        $options_class = \DELLMOND\CollectionModel::get_unique_class_registered();


        $className                  =       Array();
        $paidStudentsCount          =       Array();
        $paidAmpount                =       Array();
        $payableAmount              =       Array();
        $paidTransportAmount        =       Array();
        $payableTransportAmount     =       Array();
        $pendingAtudentCount        =       Array();
        $pendingAmountPerStudent    =       Array();

        foreach ($options_class as $key => $value) {
            foreach ($value as $K => $V) {
                

                $totalPaid = \DELLMOND\feereciept_sc::where('fee_months', $month)->where('sessionid', $sessionid)->where('class_section', $V)->sum('total_paid');
                $totalPayable = \DELLMOND\feereciept_sc::where('fee_months', $month)->where('sessionid', $sessionid)->where('class_section', $V)->sum('total_payable');
                $trans = \DELLMOND\feereciept_sc::where('fee_months', $month)->where('sessionid', $sessionid)->where('class_section', $V)->sum('transportation_amount');
                $paidStuCount = \DELLMOND\feereciept_sc::where('fee_months', $month)->where('sessionid', $sessionid)->where('class_section', $V)->count('total_paid');

                $defaulter_count = \DELLMOND\CollectionModel::defaulter_classwise_count($month, $V);
                $count = '0';
                foreach ($defaulter_count as $def_key => $def_value)
                    {
                        foreach ($def_value as $def_key_sub => $def_sub_value)
                            {
                                $count = $def_sub_value;
                            }
                    }

                $obj = new PendencyController();
                $feeamount = $obj->classwise_fee_amount_calc($month, $V);
                $transport_total = $obj->transport_total_classwise($V);

                array_push($className,                   $V                 );
                array_push($paidStudentsCount,           $paidStuCount      );
                array_push($paidAmpount,                 $totalPaid         );
                array_push($payableAmount,               $totalPayable      );
                array_push($paidTransportAmount,         $trans             );
                array_push($payableTransportAmount,      $transport_total[0]->amt   );
                array_push($pendingAtudentCount,         $count             );
                array_push($pendingAmountPerStudent,     $feeamount         );
            }
        }

        return view('pendency.monthwise_pendency_show')->with([
                'className'                 => $className,
                'paidStudentsCount'         => $paidStudentsCount,
                'paidAmpount'               => $paidAmpount,
                'payableAmount'             => $payableAmount,
                'paidTransportAmount'       => $paidTransportAmount,
                'payableTransportAmount'    => $payableTransportAmount,
                'pendingAtudentCount'       => $pendingAtudentCount,
                'pendingAmountPerStudent'   => $pendingAmountPerStudent,
                ]);

    }




    public function overall_pendency_show()
    {
        $sessionid                  =       Session('valid_id');

        $m1          = array(array('jan', 'feb', 'march', 'april', 'may', 'june', 'july', 'aug', 'sep', 'oct', 'nov', 'dec'));
        $options_class = \DELLMOND\CollectionModel::get_unique_class_registered();

        

        $monthList                  =       Array();
        $className                  =       Array();
        $paidStudentsCount          =       Array();

        $paidAmpount                =       Array();
        $payableAmount              =       Array();

        $paidTransportAmount        =       Array();
        $payableTransportAmount     =       Array();

        $pendingAtudentCount        =       Array();
        $pendingAmountPerStudent    =       Array();
        $mnth                       =       Array();


        foreach ($m1 as $k1 => $v1)
        {
           foreach ($v1 as $k2 => $v2)
           {
                array_push($monthList,  $v2 );
           }
        }
       
        foreach ($monthList as $kkk => $month)
        {
            foreach ($options_class as $key => $value)
            {
                foreach ($value as $K => $V)
                {
                    
                    $totalPaid = \DELLMOND\feereciept_sc::where('fee_months', $month)->where('sessionid', $sessionid)->where('class_section', $V)->sum('total_paid');
                    $totalPayable = \DELLMOND\feereciept_sc::where('fee_months', $month)->where('sessionid', $sessionid)->where('class_section', $V)->sum('total_payable');
                    $trans = \DELLMOND\feereciept_sc::where('fee_months', $month)->where('sessionid', $sessionid)->where('class_section', $V)->sum('transportation_amount');
                    $paidStuCount = \DELLMOND\feereciept_sc::where('fee_months', $month)->where('sessionid', $sessionid)->where('class_section', $V)->count('total_paid');

                    $defaulter_count = \DELLMOND\CollectionModel::defaulter_classwise_count($month, $V);
                    $count = '0';
                    foreach ($defaulter_count as $def_key => $def_value)
                        {
                            foreach ($def_value as $def_key_sub => $def_sub_value)
                                {
                                    $count = $def_sub_value;
                                }
                        }
                    $obj = new PendencyController();
                    $feeamount = $obj->classwise_fee_amount_calc($month, $V);
                    $transport_total = $obj->transport_total_classwise($V);

                    array_push($className,                   $V                 );
                    array_push($paidStudentsCount,           $paidStuCount      );
                    array_push($paidAmpount,                 $totalPaid         );
                    array_push($payableAmount,               $totalPayable      );

                    array_push($paidTransportAmount,         $trans             );
                    array_push($payableTransportAmount,      $transport_total[0]->amt   );

                    array_push($pendingAtudentCount,         $count             );
                    array_push($pendingAmountPerStudent,     $feeamount         );
                    array_push($mnth,     $month         );
                }
            }
        }

        return view('pendency.overall_pendency_show')->with([
                'months'                    => $mnth,
                'className'                 => $className,
                'paidStudentsCount'         => $paidStudentsCount,
                'paidAmpount'               => $paidAmpount,
                'payableAmount'             => $payableAmount,
                'paidTransportAmount'       => $paidTransportAmount,
                'payableTransportAmount'    => $payableTransportAmount,
                'pendingAtudentCount'       => $pendingAtudentCount,
                'pendingAmountPerStudent'   => $pendingAmountPerStudent,
                ]);

    }


    public function transport_total_classwise($cl) {

        $month = 'jan';
        // $cl = 'I-A';
        $sessionid                  =       Session('valid_id');

        $amt = DB::table('students')
            ->join('transportroutes', 'students.transport_route', '=', 'transportroutes.id')
            ->where('students.class_section', $cl)
            ->where('students.sessionid', $sessionid)
            // ->select('users.id', 'users.admission_no','users.student_name',  'transportroutes.amount')
            // ->select('transportroutes.amount' )
            ->select( \DB::raw('SUM(transportroutes.amount) as amt') )
            ->get();

            return ($amt);
    }



    public function classwise_pendency_index()
    {
        
    	$options_class = \DELLMOND\CollectionModel::get_unique_class_registered();
        $options = \DELLMOND\CollectionModel::get_submitted_fee_months();

		return view('pendency.classwise_pendency_form')->with([
	            'options' => $options,
	            'options_class' => $options_class,
	            ]);
    }

    public function classwise_pendency_show()
    {
        $sessionid                  =       Session('valid_id');
    	$field = Input::all();
        $month 			= isset($field['month']) 		 	? 	$field['month'] 			: 'april';
        $class_section  = isset($field['class_section']) 	? 	$field['class_section'] 	: 'I-A';

        $totalPaid = \DELLMOND\feereciept_sc::where('fee_months', $month)->where('sessionid', $sessionid)->where('class_section', $class_section)->sum('total_paid');
        
        $totalPayable = \DELLMOND\feereciept_sc::where('fee_months', $month)->where('sessionid', $sessionid)->where('class_section', $class_section)->sum('total_payable');
        
        $trans = \DELLMOND\feereciept_sc::where('fee_months', $month)->where('sessionid', $sessionid)->where('class_section', $class_section)->sum('transportation_amount');
        
        $paidStuCount = \DELLMOND\feereciept_sc::where('fee_months', $month)->where('sessionid', $sessionid)->where('class_section', $class_section)->count('total_paid');
        
        $defaulter_count = \DELLMOND\CollectionModel::defaulter_classwise_count($month, $class_section);
        $count = '0';
        foreach ($defaulter_count as $k => $v)
	        {
	        	foreach ($v as $key => $value)
			        {
			        	$count = $value;
			        }
	        }
	    $obj = new PendencyController();
        $feeamount = $obj->classwise_fee_amount_calc($month, $class_section);
        $transport_total = $obj->transport_total_classwise($class_section);
	    
        return view('pendency.classwise_pendency_show')->with([
            'defaulter_count' => $count,
            'fee_amount' => $feeamount,
            'month' => $month,
            'class_section' => $class_section,
            'totalPaid' => $totalPaid,
            'totalPayable' => $totalPayable,
            'transportPaid' => $trans,
            'transportPayable' => $transport_total[0]->amt,
            'paidStuCount' => $paidStuCount,
            ]);
    }

    public static function classwise_fee_amount_calc($month, $class_section)
    {
        $sessionid                  =       Session('valid_id');

        $feetypeslist =array(); $feeamt =array();
       
        $feetype = \DB::table('feetypes')->select('fee_type')->where('sessionid', $sessionid)->get();
        foreach ($feetype as $key => $value)
        {
            foreach ($value as $k => $i)
            {
                 $am = \DB::table('feeamounts')->select('amount')->where('sessionid', $sessionid)->where('fee_type', '=', $i)->get();
                 array_push($feetypeslist, $i); array_push($feeamt, $am);          
            }
        }
        //print_r($feeamt);
        
        $subtotal = 0;
        foreach ($feetypeslist as $k => $v) 
        {

            $entrypass = \DB::table('feetypes')->where('fee_type', '=', $v)
                                            ->where('sessionid', $sessionid)
            								    ->where($month, '=', '1') 
            								   ->count('id');
            if($entrypass==1)
            {

            $amt = \DB::table('feeamounts')->where('fee_type', '=', $v)->where('sessionid', $sessionid)->where('class_section', '=', $class_section)->count('amount');
                if($amt == 1)
                {
                    $amtt = \DB::table('feeamounts')->where('sessionid', $sessionid)->where('fee_type', '=', $v)->where('class_section', '=', $class_section)->pluck('amount');
                
                $subtotal = $subtotal + $amtt[0];
                }
            }
                    
        }
            return $subtotal;           
        
    }



    public function studentwise_pendency_index()
    {
        
        return view('pendency.studentwise_pendency_form')->with([ ]);
    }

    public function studentwise_pendency_show()
    {
        $sessionid                  =       Session('valid_id');

        $field = Input::all();
        $admissionNumber          = isset($field['admissionNumber'])   ?   $field['admissionNumber']      : '1';

        $m1          = array(array('jan', 'feb', 'march', 'april', 'may', 'june', 'july', 'aug', 'sep', 'oct', 'nov', 'dec'));
        $options_class = \DELLMOND\CollectionModel::get_class_registered_for_known_admission_no($admissionNumber);

        $monthList                  =       Array();
        $className                  =       Array();
        $paidStudentsCount          =       Array();
        $paidAmpount                =       Array();
        $payableAmount              =       Array();
        $paidTransportAmount        =       Array();
        $pendingAtudentCount        =       Array();
        $pendingAmountPerStudent    =       Array();


        foreach ($m1 as $k1 => $v1)
        {
           foreach ($v1 as $k2 => $v2)
           {
                array_push($monthList,  $v2 );
           }
        }
       
        foreach ($monthList as $kkk => $month)
        {
            foreach ($options_class as $key => $value)
            {
                foreach ($value as $K => $V)
                {

                    $totalPaid = \DELLMOND\feereciept_sc::where('fee_months', $month)->where('sessionid', $sessionid)->where('admission_no', $admissionNumber)->sum('total_paid');
                    $totalPayable = \DELLMOND\feereciept_sc::where('fee_months', $month)->where('sessionid', $sessionid)->where('admission_no', $admissionNumber)->sum('total_payable');
                    $trans = \DELLMOND\feereciept_sc::where('fee_months', $month)->where('sessionid', $sessionid)->where('admission_no', $admissionNumber)->sum('transportation_amount');
                    $paidStuCount = \DELLMOND\feereciept_sc::where('fee_months', $month)->where('sessionid', $sessionid)->where('admission_no', $admissionNumber)->count('total_paid');

                    $defaulter_count = \DELLMOND\CollectionModel::defaulter_classwise_count($month, $V);
                    $count = '0';
                    foreach ($defaulter_count as $def_key => $def_value)
                        {
                            foreach ($def_value as $def_key_sub => $def_sub_value)
                                {
                                    $count = $def_sub_value;
                                }
                        }

                    $obj = new PendencyController();
                    $feeamount = $obj->classwise_fee_amount_calc($month, $V);

                    array_push($className,                   $V                 );
                    array_push($paidStudentsCount,           $paidStuCount      );
                    array_push($paidAmpount,                 $totalPaid         );
                    array_push($payableAmount,               $totalPayable      );
                    array_push($paidTransportAmount,         $trans             );
                    array_push($pendingAtudentCount,         $count             );
                    array_push($pendingAmountPerStudent,     $feeamount         );
                }
            }
        }

        return view('pendency.studentwise_pendency_show')->with([
                'admissionNumber'           => $admissionNumber,
                'monthList'                 => $monthList,
                'className'                 => $className,
                'paidStudentsCount'         => $paidStudentsCount,
                'paidAmpount'               => $paidAmpount,
                'payableAmount'             => $payableAmount,
                'paidTransportAmount'       => $paidTransportAmount,

                'pendingAmountPerStudent'   => $pendingAmountPerStudent,
                ]);


    }


    /*   *** ADDED LATER ***    */

    public function determine_transport_amount($admission_no)
    {
        $sessionid                  =       Session('valid_id');
        $transportation_status ;
        $transportation_amount ;
        $transp = DB::table('students')->where('transport_route', '>', '0')->where('admission_no', '=', $admission_no)->count('admission_no');
           
       if($transp == '0') { $transportation_status = 0 ;  }
       else               { $transportation_status = 1 ;  }

       if($transportation_status == 1)
            { 
                $r = 0 ; 
                $tprroute = DB::table('students')->where('admission_no', '=', $admission_no)->pluck('transport_route');
                foreach ($tprroute as $key => $value) {
                    $r = $value ; 
                }
                // now we got tansport route, we have to calc amount at that route 
                $amttpr = DB::table('transportroutes')->where('id', '=', $r)->pluck('amount');
                foreach ($amttpr as $title) {   $transportation_amount = $title;  }
            }
       else  { $transportation_amount = 0 ;  } 
       return $transportation_amount;
    }




    ######################################## ONCE REQUIRED FEE PENDENCY ######################################
     public function once_req_fee_pendency_index()
    {
        $sessionid                  =       Session('valid_id');
        $data = feetype::where('once_required', '1')->where('sessionid', $sessionid)->get();
        return view('pendency.once_req_fee_pendency_form')->with([ 'fee_type' => $data, ]);
    }

    public function once_req_fee_pendency_show()
    {
        $sessionid                  =       Session('valid_id');
        $field = Input::all();
        $onceRequired = isset($field['onceRequired']) ? $field['onceRequired'] : '0';

        $options_class = \DELLMOND\CollectionModel::get_unique_class_registered();

        $monthList                  =       Array();
        $className                  =       Array();
        $paidStudentsCount          =       Array();
        $paidAmpount                =       Array();
        $payableAmount              =       Array();
        $pendingAtudentCount        =       Array();
        $pendingAmountPerStudent    =       Array();

        foreach ($options_class as $key => $value)
        {
            foreach ($value as $K => $V)
            {
                
                $totalPaid = \DELLMOND\feereciept_sc::where('fee_months', $onceRequired)->where('sessionid', $sessionid)->where('class_section', $V)->sum('total_paid');
                $totalPayable = \DELLMOND\feereciept_sc::where('fee_months', $onceRequired)->where('sessionid', $sessionid)->where('class_section', $V)->sum('total_payable');
                $paidStuCount = \DELLMOND\feereciept_sc::where('fee_months', $onceRequired)->where('sessionid', $sessionid)->where('class_section', $V)->count('total_paid');

                $defaulter_count = \DELLMOND\CollectionModel::defaulter_classwise_count($onceRequired, $V);
                $count = '0';
                foreach ($defaulter_count as $def_key => $def_value)
                    {
                        foreach ($def_value as $def_key_sub => $def_sub_value)
                            {
                                $count = $def_sub_value;
                            }
                    }

                $obj = new PendencyController();
                $feeamount = $obj->calculate_once_requiredfee_amount($onceRequired, $V);

                array_push($monthList,                   $onceRequired      );
                array_push($className,                   $V                 );
                array_push($paidStudentsCount,           $paidStuCount      );
                array_push($paidAmpount,                 $totalPaid         );
                array_push($payableAmount,               $totalPayable      );
                array_push($pendingAtudentCount,         $count             );
                array_push($pendingAmountPerStudent,     $feeamount         );
            }
        }
        

        return view('pendency.once_req_fee_pendency_show')->with([
            'className' => $className,
            'monthList' => $monthList,
            'paidStudentsCount' => $paidStudentsCount,
            'paidAmpount' => $paidAmpount,
            'payableAmount' => $payableAmount,
            'pendingAtudentCount' => $pendingAtudentCount,
            'pendingAmountPerStudent' => $pendingAmountPerStudent,
            ]);
    }


    public function calculate_once_requiredfee_amount($typeoffee, $class_section)
    {
        $sessionid                  =       Session('valid_id');
        $subtotal = 0;
         
            $entrypass = DB::table('feetypes')->where('fee_type', '=', $typeoffee)->where('sessionid', $sessionid)->where('once_required', '=', '1')->count('id');
            if($entrypass==1)
               {
                 $amt = DB::table('feeamounts')->where('fee_type', '=', $typeoffee)->where('sessionid', $sessionid)->where('class_section', '=', $class_section)->count('amount');
                    if($amt == 1)
                    {
                        $amtt = DB::table('feeamounts')->where('fee_type', '=', $typeoffee)->where('sessionid', $sessionid)->where('class_section', '=', $class_section)->pluck('amount');
                    
                    $subtotal = $subtotal + $amtt[0];
                    }
               }
                    
            
            return $subtotal;       
    }



}

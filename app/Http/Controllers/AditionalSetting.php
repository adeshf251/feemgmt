<?php

namespace DELLMOND\Http\Controllers;

use Illuminate\Http\Request;
use DELLMOND\User;
use DELLMOND\Admin;

class AditionalSetting extends Controller
{
    public function ab() {
        return 'ssss';
    }
    public function showusers()
    {
        $userlist = User::all();
        return view('members.showusers')->with(['userlist' => $userlist , ]);
    }
    public function passwordReset()
    {
        return view('members.reptpasswordreset');
    }
    public function passwordResetSubmit(Request $request)
    {
        $password = $request->input('password');
        // $email = $request->input('email');
        \DB::table('administrator')->update(['password' => md5($password)]);
        return view('thanks');
    }

    public function adduserform()
    {
        return view('members.addusers');
    }

    public function adduser(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);


        $name = $request->input('name');
        $email = $request->input('email');
        $password = $request->input('password');

        $x = new User;
        $x->name = $name;
        $x->email = $email;
        $x->password = bcrypt($password) ;
        $x->save();

        return view('members.thanks');
    }


    public function showadmins()
    {
        $userlist = Admin::all();
        
        return view('members.showadmins')->with(['userlist' => $userlist , ]);
    }
    public function addadminform()
    {
        return view('members.addadmins');
    }
    public function addadmin(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:admins',
            'password' => 'required|string|min:6|confirmed',
        ]);


        $name = $request->input('name');
        $email = $request->input('email');
        $password = $request->input('password');
        
        $x = new Admin;
        $x->name = $name;
        $x->role_id = 1;
        $x->email = $email;
        $x->avatar = 'users/default.png';
        $x->password = md5($password) ;
        $x->save();

        return view('members.thanks');
    }
    
    
    public function createNewUserReq() {
        //
        $userlist = User::all();

        return view('members.showusers')->with(['userlist' => $userlist , ]);
        echo 'Work Under Progress';
    }
    public function createNewUserSubmit(Request $req) {
        //
        echo 'Work Under Progress';
    }
    public function createPasswordResetReq() {
        //
        echo 'Work Under Progress';
    }
    public function createPasswordResetSubmit(Request $req) {
        //
        echo 'Work Under Progress';
    }
    public function deleteUser(Request $req) {
        //
        echo 'Work Under Progress';
    }
    public function showAllUser() {
        //
        echo 'Work Under Progress';
    }


    public function getAdminLogin(Request $req) {
        $value = Session('admin');
        if($value == 'yes') {
            return redirect('/admin-controller');
        }
        return view('adminlogin');
    }

    public function adminLogin(Request $request) {
        $email = $request->input('email');
        $password = $request->input('password');
        $enc = md5($password);
        $a = Admin::where('email', $email )->where('password',$enc)->count('id');
        if($a > 0) {
            session(['admin' => 'yes']);
            session(['admin_email' => $email ]);
            session(['admin_password' => $password ]);
            return redirect('/');
        } else {
            return 'Unable to login at the moment! Please try again later.';
        }
        // dd($a, $email, $password, $enc );
        // if (auth()->guard('admin')->attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
        //     return 's';
        // } else {
        //     return 'asa';
        // }
    }



    // ********************************************//
    public function sendSMSreq() {
        //
        echo 'Work Under Progress';
    }
    public function sendSMSSubmit(Request $req) {
        //
        echo 'Work Under Progress';
    }
    public function sendEmailreq() {
        //
        echo 'Work Under Progress';
    }
    public function sendEmailSubmit(Request $req) {
        //
        echo 'Work Under Progress';
    }
    public function syncServer(Request $req) {
        //
        echo 'Work Under Progress';
    }
}

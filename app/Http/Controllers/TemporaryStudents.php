<?php

namespace DELLMOND\Http\Controllers;

use Illuminate\Http\Request;
use DELLMOND\student;
use DELLMOND\classmgmt;
use DELLMOND\Temporarystudent;
use DELLMOND\transportroute;

class TemporaryStudents extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $classopt=\DB::table('classmgmts')->orderBy('class_section', 'asc')->where('sessionid', $sessionid)->select('class_section')->get();
        // $routess=\DB::table('transportroutes')->orderBy('id', 'asc')->where('sessionid', $sessionid)->select('id')->get();

        $sessionid                  =       Session('valid_id');
        $student =  Temporarystudent::where('sessionid', $sessionid)->get();
        return view('temporarystudent.showall')->with(['data' => $student]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sessionid                  =       Session('valid_id');
        $classopt=classmgmt::where('sessionid', $sessionid)->orderBy('class_section', 'asc')->select('class_section')->get();
        $routess=transportroute::where('sessionid', $sessionid)->orderBy('id', 'asc')->select('id')->get();

        return view('temporarystudent.createnew')->with(['classname' => $classopt, 'transport_route' => $routess, ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $sessionid                  =       Session('valid_id');
        // $field = Input::all();
        $sub = new Temporarystudent;
        $sub->sessionid             = $sessionid;
        $sub->Save();


        $sub->admission_no          = 'Dellmond/'.date("d").'/'.$sub->id;
        $sub->session               = Session('valid_period');;
        $sub->student_name          = $req->input('stdname');
        $sub->class_section         = $req->input('clsec');
        $sub->father_name           = $req->input('father_name');
        $sub->mother_name           = $req->input('mother_name');
        $sub->gender                = $req->input('gender');
        $sub->address               = $req->input('address');
        $sub->mobile                = $req->input('mobile');
        $sub->transport_route       = $req->input('transport_route');
        $sub->dob                   = $req->input('dob');
        $sub->status                = 'active';
        $sub->priority              = 'N.A';
        $sub->ref                   = 'N.A';
        $sub->remark                = $req->input('remark');
        $sub->email                = $req->input('email');
        $sub->sessionid             = $sessionid;
        $sub->Save();
$emailid = $req->input('email');
        if( strpos($emailid, '@') !== false ) { 
            $email   =           $emailid;
            $subject =           'Student Registration';
            $body    =           'Hey '.$req->input('father_name').'!
               Thank you for registering your child with DELLMOND INTERNATION SCHOOL!';
    
            $postdata = http_build_query(
                array(
                    'email'     => $email,
                    'subject'   => $subject,
                    'body'      => $body,
                )
            );

            $opts = array('http' =>
                array(
                    'method'  => 'POST',
                    'header'  => 'Content-type: application/x-www-form-urlencoded',
                    'content' => $postdata
                )
            );

            $context  = stream_context_create($opts);

            $result = @file_get_contents('http://central.dellmondinternational.in/sendmail.php', false, $context);

            // if($http_response_header[0]=="HTTP/1.1 404 Not Found"):

            //             return "iam 404";

            // elseif($http_response_header[0]=="HTTP/1.1 200 OK"):

            //             // return view('emails.sended');

            // else:

            //             return "unknown error"; 

            // endif;
        }
        return view('thanks');
    }


    public function sendmailsubmit(Request $request)
    {
        // send mail via hitting to sendmail page at dellmond domain
        $email   =           $request->input('email');
        $subject =           $request->input('title');
        $body    =           $request->input('content');

        $postdata = http_build_query(
            array(
                'email'     => $email,
                'subject'   => $subject,
                'body'      => $body,
            )
        );

        $opts = array('http' =>
            array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => $postdata
            )
        );

        $context  = stream_context_create($opts);

        $result = @file_get_contents('http://central.dellmondinternational.in/sendmail.php', false, $context);

        if($http_response_header[0]=="HTTP/1.1 404 Not Found"):

                    return "iam 404";

        elseif($http_response_header[0]=="HTTP/1.1 200 OK"):

                    return view('emails.sended');

        else:

                    return "unknown error"; 

        endif;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sessionid                  =       Session('valid_id');
        $reptlists =  Temporarystudent::find($id);
        return view('temporarystudent.recieptpreview')->with(['data' => $reptlists]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $x = Temporarystudent::find($id);
        $x->delete();
        
        $request->session()->flash('message', 'Successfully Deleted the Student Details!');
        return view('thanks');
    }

    public function displayCollection() {
        $sessionid                  =       Session('valid_id');
        return view('temporarystudent/collection_datewaise');
    }
    public function displayCollectionSubmit(Request $request) {
        $sessionid                  =       Session('valid_id');
        $start = $request->input('collection_start_date');
        $finish = $request->input('collection_end_date');

        $list = Temporarystudent::where('updated_at', '>=', $start)->where('updated_at', '<=', $finish)->get();
        // dd($list);

        return view('temporarystudent.collection_datewise_display')->with(['list'=>$list,
                'start_date' => $start,
                'end_date' => $finish,
                ]);      
    }
}

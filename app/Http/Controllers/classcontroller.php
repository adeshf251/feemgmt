<?php

namespace DELLMOND\Http\Controllers;
use DELLMOND\Http\Requests;
use DELLMOND\User;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Input;
use DELLMOND\classmgmt;
use DELLMOND\feeamount;
use DELLMOND\feetype;
use DELLMOND\student;
use DB;
use Excel;

 
 /*
 Schema::create('classmgmts', function (Blueprint $table) {
           $table->increments('rept_no');
           $table->text('admission_no');
           $table->text('class_section');
           $table->text('description');
           $table->timestamps();
       }); *
 *
 *
 *
 */
class classcontroller extends Controller
{
    public function Index()
    {
        return view('datatables.classtable');
    }
    public function anyData()
    {
         return Datatables::of(Classmgmt::query('select * from classmgmts'))->make(true);
    }
    public function getPosts() 
    {
    return Datatables::of(Classmgmt::query('select * from classmgmts'))->make(true);
    }
     public function insert()
    {
         $sessionid                  =       Session('valid_id');
    	$field = Input::all();
        $sub = new Classmgmt;
        $sub->class_section = $field['class_section'];
        $sub->description = $field['description'];
        $sub->sessionid = $sessionid ;
        $sub->Save();
        return view('thanks');
    }
    public function addclassrequest()
    {
         $sessionid                  =       Session('valid_id');
    	 $txtlabels = array('Class Section ex - 1-a, 2-c, 8-a', 'description');
        $txtnm = array('class_section','description');
        $txttype = array('text','text');
        
        return view('formgenerator')->with([
            'form_heading' =>'Add New class to Management Section',
            'submit_to' => 'f.regclass',
            'txtlabels'=>$txtlabels,
            'txtnm'=> $txtnm,
            'txttype'=>$txttype,]);
    }
    public function deleteclassrequest()
    {
         $sessionid                  =       Session('valid_id');
        $field = Input::all();
       
        $data=\DB::table('classmgmts')->select('class_section', 'description', 'id')->orderBy('id', 'desc')->where('sessionid', $sessionid)->get();
        return view('coremgmt.classdeletetablegenerate')->with(['data'=>$data,'form_heading' => 'Click on delete button to delete the class',]);
    }
    public function deleteclass($id)
    {
         $sessionid                  =       Session('valid_id');
        DB::table('classmgmts')->where('id','=',$id)->delete();
        return view('thanks');
    }

}

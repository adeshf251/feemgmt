<?php

namespace DELLMOND\Http\Controllers;
use DELLMOND\Http\Requests;
use DELLMOND\User;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Input;
use DELLMOND\classmgmt;
use DELLMOND\feeamount;
use DELLMOND\feetype;
use DELLMOND\student;
use DELLMOND\CollectionModel;
use DELLMOND\feereciept_student;
use DELLMOND\feereciept_sc;
use DB;
use Excel;

class CollectionController extends Controller
{
    public function index()
    {
         $sessionid                  =       Session('valid_id');
    	return view('collection/collection_datewise_form');
    }

    public function collectidatewise()
    {
         $sessionid                  =       Session('valid_id');
    	$field = Input::all();
        $collection_start_date = isset($field['collection_start_date']) ? $field['collection_start_date'] : '2017-03-30';
        $collection_end_date = isset($field['collection_end_date']) ? $field['collection_end_date'] : '2030-12-30';
        $payment_type = isset($field['payment_type']) ? $field['payment_type'] : 'cash';

        if($payment_type == 'cash')
        {
        	$amt = \DELLMOND\CollectionModel::collection_between_dates_cash_amount_only( $collection_start_date, $collection_end_date );
        	$student_details = \DELLMOND\CollectionModel::collection_between_dates_cash_students_details_only( $collection_start_date, $collection_end_date);
        }

        if($payment_type == 'cheque')
        {
        	$amt = \DELLMOND\CollectionModel::collection_between_dates_cheque_amount_only( $collection_start_date, $collection_end_date );
        	$student_details = \DELLMOND\CollectionModel::collection_between_dates_cheque_students_details_only( $collection_start_date, $collection_end_date);
        }

        if($payment_type == 'dd')
        {
        	$amt = \DELLMOND\CollectionModel::collection_between_dates_dd_amount_only( $collection_start_date, $collection_end_date );
        	$student_details = \DELLMOND\CollectionModel::collection_between_dates_dd_students_details_only( $collection_start_date, $collection_end_date);
        }

        $amount = 0;
    	foreach ($amt as $key => $v) { foreach ($v as $k => $value) { $amount = $value; }}

	return view('collection.collection_datewise_display')->with(['amount'=>$amount,
            'student_details' => $student_details,
            'start_date' => $collection_start_date,
            'end_date' => $collection_end_date,
            ]);        
    }


    public function collection_monthwise_index()
    {
         $sessionid                  =       Session('valid_id');
        
        $options = \DELLMOND\CollectionModel::get_submitted_fee_months();

    return view('collection.collection_monthwise_form')->with([
            'options' => $options,
            ]);        
    }

    public function collection_monthwise_submit()
    {
         $sessionid                  =       Session('valid_id');
        $field = Input::all();
        $month = isset($field['month']) ? $field['month'] : 'april';
        
        
            $amt = \DELLMOND\CollectionModel::collection_fee_months_amount_only( $month);
            $student_details = \DELLMOND\CollectionModel::collection_fee_months_students_details_only( $month);
        

        $amount = 0;
        foreach ($amt as $key => $v) { foreach ($v as $k => $value) { $amount = $value; }}

    return view('collection.collection_datewise_display')->with(['amount'=>$amount,
            'student_details' => $student_details,
            'start_date' => '2017-1-1',
            'end_date' => '2017-1-1',
            ]);        
    }




    public function collection_student_index()
    {
         $sessionid                  =       Session('valid_id');

    return view('collection.collection_studentwise_form')->with([  ]);        
    }

    public function collection_student_submit()
    {
         $sessionid                  =       Session('valid_id');
        $field = Input::all();
        $admission_no = isset($field['admission_no']) ? $field['admission_no'] : '0';
        
        
            $amt = \DELLMOND\CollectionModel::collection_admission_no_amount_only( $admission_no);
            $student_details = \DELLMOND\CollectionModel::collection_admission_no_students_details_only( $admission_no);
        

        $amount = 0;
        foreach ($amt as $key => $v) { foreach ($v as $k => $value) { $amount = $value; }}

    return view('collection.collection_datewise_display')->with(['amount'=>$amount,
            'student_details' => $student_details,
            'start_date' => 'Session-Start',
            'end_date' => 'Session-End',
            ]);        
    }


    public function collectidatewise123()
    {
         $sessionid                  =       Session('valid_id');
    	
    	$student_name = \DELLMOND\CollectionModel::collection_between_dates( '2017-07-01', '2017-08-11');
    	$amt = \DELLMOND\CollectionModel::collection_between_dates_dd_amount_only( '2017-07-01', '2017-08-11');
    	print_r($amt);
    }

    //***********************************  Transport Collection Management ***************************************

    public function transport_index()
    {
         $sessionid                  =       Session('valid_id');
        return view('collection/collection_datewise_transport_form');
    }

    public function collectidatewisetransport_submit()
    {
         $sessionid                  =       Session('valid_id');
        $field = Input::all();
        $collection_start_date = isset($field['collection_start_date']) ? $field['collection_start_date'] : '2017-03-30';
        $collection_end_date = isset($field['collection_end_date']) ? $field['collection_end_date'] : '2030-12-30';
        //$payment_type = isset($field['payment_type']) ? $field['payment_type'] : 'cash';

        
        $amt = \DELLMOND\CollectionModel::collection_between_dates_transport_amount_only( $collection_start_date, $collection_end_date );
        $student_details = \DELLMOND\CollectionModel::collection_between_dates_transport_students_details_only( $collection_start_date, $collection_end_date);

        $amount = 0;
        foreach ($amt as $key => $v) { foreach ($v as $k => $value) { $amount = $value; }}

        return view('collection.collection_datewise_display')->with(['amount'=>$amount,
                'student_details' => $student_details,
                'start_date' => $collection_start_date,
                'end_date' => $collection_end_date,
                ]);      
    }

    public function collection_student_transport_index()
    {
         $sessionid                  =       Session('valid_id');

        return view('collection.collection_transport_studentwise_form')->with([  ]);        
    }

    public function collection_student_transport_submit()
    {
         $sessionid                  =       Session('valid_id');
        $field = Input::all();
        $admission_no = isset($field['admission_no']) ? $field['admission_no'] : '0';
        
        
            $amt = \DELLMOND\CollectionModel::collection_admission_no_transport_amount_only( $admission_no);
            $student_details = \DELLMOND\CollectionModel::collection_admission_no_transport_students_details_only( $admission_no);

        $amount = 0;
        foreach ($amt as $key => $v) { foreach ($v as $k => $value) { $amount = $value; }}

        return view('collection.collection_datewise_display')->with(['amount'=>$amount,
                'student_details' => $student_details,
                'start_date' => 'Session-Start',
                'end_date' => 'Session-End',
                ]);  
                
    }



}

/*
Route::get('/collection-datewise-transport', 'CollectionController@transport_index' );
Route::post('/collection-datewise-transport-submit', 'CollectionController@collectidatewisetransport_submit' );
Route::get('/collection-by-student-transport-request', 'CollectionController@collection_student_transport_index' );
Route::post('/collection-student-transport-submit', 'CollectionController@collection_student_transport_submit' );
*/
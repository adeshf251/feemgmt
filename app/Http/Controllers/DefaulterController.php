<?php

namespace DELLMOND\Http\Controllers;
use DELLMOND\Http\Requests;
use DELLMOND\User;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Input;
use DELLMOND\classmgmt;
use DELLMOND\feeamount;
use DELLMOND\feetype;
use DELLMOND\student;
use DELLMOND\CollectionModel;
use DELLMOND\feereciept_student;
use DELLMOND\feereciept_sc;
use DB;
use Excel;

class DefaulterController extends Controller
{
    public function defaulter_monthwise_index()
    {
         $sessionid                  =       Session('valid_id');
    	
        $options = \DELLMOND\CollectionModel::get_submitted_fee_months();

	return view('defaulter.monthwisedefaulter_form')->with([
            'options' => $options,
            ]);        
    }

    public function defaulter_monthwise_show()
    {
         $sessionid                  =       Session('valid_id');
    	
        $field = Input::all();
        $month = isset($field['month']) ? $field['month'] : 'april';
        $student_list = \DELLMOND\CollectionModel::defaulter_monthwise_list($month);
       // $student_count = \DELLMOND\CollectionModel::defaulter_monthwise_list($month);
       // print_r($student_list);

        return view('defaulter.monthwisedefaulter_display')->with([
            'student_list' => $student_list,
            
            ]);        
    }








    // now classwise and monthwise both
    public function defaulter_classwise_index()
    {
         $sessionid                  =       Session('valid_id');
    	
        $options_class = \DELLMOND\CollectionModel::get_unique_class_registered();
        $options = \DELLMOND\CollectionModel::get_submitted_fee_months();

	return view('defaulter.classwisedefaulter_form')->with([
            'options' => $options,
            'options_class' => $options_class,
            ]);
    }

    public function defaulter_classwise_show()
    {
         $sessionid                  =       Session('valid_id');
    	
        $field = Input::all();
        $month 			= isset($field['month']) 		 	? 	$field['month'] 			: 'april';
        $class_section  = isset($field['class_section']) 	? 	$field['class_section'] 	: 'I-A';
        
        $student_list = \DELLMOND\CollectionModel::defaulter_classwise_list($month, $class_section);

        return view('defaulter.monthwisedefaulter_display')->with([
            'student_list' => $student_list,
            
            ]);           
    }

    public function transport_total_studentwise($admission_no) {
        $sessionid                  =       Session('valid_id');
        $month = 'jan';
        // $cl = 'I-A';

        $amt = DB::table('students')
            ->join('transportroutes', 'students.transport_route', '=', 'transportroutes.id')
            ->where('students.sessionid', $sessionid)
            ->where('students.admission_no', $admission_no)
            // ->select('users.id', 'users.admission_no','users.student_name',  'transportroutes.amount')
            // ->select('transportroutes.amount' )
            ->select( \DB::raw('SUM(transportroutes.amount) as amt') )
            ->get();

            return ($amt[0]->amt);
    }

    public function month_fee_amount($class_section, $feetype_individual = 'Tuition fee') {
            $sessionid                  =       Session('valid_id');
            $fee_value = DB::table('feeamounts')
                // ->join('contacts', 'users.id', '=', 'contacts.user_id')
                ->where('sessionid', $sessionid)
                ->where('fee_type', '=', $feetype_individual)
                ->where('class_section', '=', $class_section)
                ->pluck('amount');
        return $fee_value[0];
    }

    public function calculatePendingMonths($class = 'I-A')
    {
        $sessionid                  =       Session('valid_id');
        $students=\DB::table('students')
                ->select('id', 'student_name', 'class_section', 'father_name', 'dob', 'admission_no', 'transport_route', 'mobile')
                ->where('sessionid', $sessionid)
                ->where('class_section','=',$class)
                ->get();

        $transport_totals       =       Array();
        $fee_totals             =       Array();
        $fee_months             =       Array();
                
        foreach ($students as $k1 => $v1) {
                $admission_no = $v1->admission_no ;
                $sum_transport = 0;
                $sum_fee = 0;
                $str1 = '';

                // getting once req fee variable --- start
                $array_once_fee = array();
                $once_req_fee_array = DB::table('feetypes')->where('sessionid', $sessionid)->where('once_required', '=', '1')->pluck('fee_type');

                foreach ($once_req_fee_array as $key => $value) {
                $reqq = DB::table('feereciept_scs')->where('sessionid', $sessionid)->where('month_admno_code', '=', $admission_no.$value)->count('admission_no');
                if($reqq == '0') { array_push($array_once_fee, $value);  }
                }
        
                // getting once req fee variable --- stop

                $jan = DB::table('feereciept_scs')->where('sessionid', $sessionid)->where('month_admno_code', '=', $admission_no."jan")->count('admission_no');
                $feb = DB::table('feereciept_scs')->where('sessionid', $sessionid)->where('month_admno_code', '=', $admission_no."feb")->count('admission_no');
                $march = DB::table('feereciept_scs')->where('sessionid', $sessionid)->where('month_admno_code', '=', $admission_no."march")->count('admission_no');
                $april = DB::table('feereciept_scs')->where('sessionid', $sessionid)->where('month_admno_code', '=', $admission_no."april")->count('admission_no');
                $may = DB::table('feereciept_scs')->where('sessionid', $sessionid)->where('month_admno_code', '=', $admission_no."may")->count('admission_no');
                $june = DB::table('feereciept_scs')->where('sessionid', $sessionid)->where('month_admno_code', '=', $admission_no."june")->count('admission_no');
                $july = DB::table('feereciept_scs')->where('sessionid', $sessionid)->where('month_admno_code', '=', $admission_no."july")->count('admission_no');
                $aug = DB::table('feereciept_scs')->where('sessionid', $sessionid)->where('month_admno_code', '=', $admission_no."aug")->count('admission_no');
                $sept = DB::table('feereciept_scs')->where('sessionid', $sessionid)->where('month_admno_code', '=', $admission_no."sep")->count('admission_no');
                $oct = DB::table('feereciept_scs')->where('sessionid', $sessionid)->where('month_admno_code', '=', $admission_no."oct")->count('admission_no');
                $nov = DB::table('feereciept_scs')->where('sessionid', $sessionid)->where('month_admno_code', '=', $admission_no."nov")->count('admission_no');
                $dec = DB::table('feereciept_scs')->where('sessionid', $sessionid)->where('month_admno_code', '=', $admission_no."dec")->count('admission_no');


                // now generate monthly list

                if($april == 0) { $str1 = $str1.'April ,'; 
                        $sum_transport = $sum_transport + $this->transport_total_studentwise($admission_no) ;
                        $sum_fee = $sum_fee + $this->month_fee_amount($class); }
                if($may == 0) { $str1 = $str1.'May ,'; 
                        $sum_transport = $sum_transport + $this->transport_total_studentwise($admission_no) ;
                        $sum_fee = $sum_fee + $this->month_fee_amount($class); }
                if($june == 0) { $str1 = $str1.'June ,'; 
                        $sum_transport = $sum_transport + $this->transport_total_studentwise($admission_no) ;
                        $sum_fee = $sum_fee + $this->month_fee_amount($class); }
                if($july == 0) { $str1 = $str1.'July ,'; 
                        $sum_transport = $sum_transport + $this->transport_total_studentwise($admission_no) ;
                        $sum_fee = $sum_fee + $this->month_fee_amount($class); }
                if($aug == 0) { $str1 = $str1.'Aug ,'; 
                        $sum_transport = $sum_transport + $this->transport_total_studentwise($admission_no) ;
                        $sum_fee = $sum_fee + $this->month_fee_amount($class); }
                if($sept == 0) { $str1 = $str1.'Sept ,'; 
                        $sum_transport = $sum_transport + $this->transport_total_studentwise($admission_no) ;
                        $sum_fee = $sum_fee + $this->month_fee_amount($class); }
                if($oct == 0) { $str1 = $str1.'Oct ,'; 
                        $sum_transport = $sum_transport + $this->transport_total_studentwise($admission_no) ;
                        $sum_fee = $sum_fee + $this->month_fee_amount($class); }
                if($nov == 0) { $str1 = $str1.'Nov ,'; 
                        $sum_transport = $sum_transport + $this->transport_total_studentwise($admission_no) ;
                        $sum_fee = $sum_fee + $this->month_fee_amount($class); }
                if($dec == 0) { $str1 = $str1.'Dec ,'; 
                        $sum_transport = $sum_transport + $this->transport_total_studentwise($admission_no) ;
                        $sum_fee = $sum_fee + $this->month_fee_amount($class); }
                if($jan == 0) { $str1 = $str1.'Jan ,'; 
                        $sum_transport = $sum_transport + $this->transport_total_studentwise($admission_no) ;
                        $sum_fee = $sum_fee + $this->month_fee_amount($class); }
                if($feb == 0) { $str1 = $str1.'Feb ,'; 
                        $sum_transport = $sum_transport + $this->transport_total_studentwise($admission_no) ;
                        $sum_fee = $sum_fee + $this->month_fee_amount($class); }
                if($march == 0) { $str1 = $str1.'March ,'; 
                        $sum_transport = $sum_transport + $this->transport_total_studentwise($admission_no) ;
                        $sum_fee = $sum_fee + $this->month_fee_amount($class); }

                array_push($transport_totals, $sum_transport);
                array_push($fee_totals,       $sum_fee);
                array_push($fee_months,       $str1);
        }
        return array($students,$fee_totals, $transport_totals, $fee_months);
    }
    public function overall_classwise(Request $req)
    {
            $options_class = \DELLMOND\CollectionModel::get_unique_class_registered();

		return view('defaulter.overalldefaulter_selectclass')->with([
	            'options_class' => $options_class,
	            ]);
            //overalldefaulter_selectclass.blade.php
        //     $this->calculatePendingMonths('I-A');
    }
    public function overallClasswiseSubmit(Request $req)
    {//overalldefaulter_lists.blade.php
        $class_section = $req->input('class_section');
        $data = $this->calculatePendingMonths($class_section);
        return view('defaulter.overalldefaulter_lists')->with([
	            'students' => $data[0],
	            'fee_totals' => $data[1],
                    'transport_totals' => $data[2],
                    'fee_months' => $data[3],
	            ]);
    }
    public function overallDefaulter(Request $req)
    {
        $students               =       Array();
        $transport_totals       =       Array();
        $fee_totals             =       Array();
        $fee_months             =       Array();

        $options_class = \DELLMOND\CollectionModel::get_unique_class_registered();
        foreach ($options_class as $key => $value) {
                $data = $this->calculatePendingMonths($value->class_section);
                array_push($students, $data[0]);
                array_push($fee_totals, $data[1]);
                array_push($transport_totals, $data[2]);
                array_push($fee_months, $data[3]);
        }
        return view('defaulter.overalldefaulter_lists')->with([
	            'students' => $students,
	            'fee_totals' => $fee_totals,
                    'transport_totals' => $transport_totals,
                    'fee_months' => $fee_months,
	            ]);
    }
}

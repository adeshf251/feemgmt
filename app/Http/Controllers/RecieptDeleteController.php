<?php


namespace DELLMOND\Http\Controllers;
use DELLMOND\Http\Requests;
use DELLMOND\User;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Input;
use DELLMOND\classmgmt;
use DELLMOND\feeamount;
use DELLMOND\feetype;
use DELLMOND\student;
use DELLMOND\discountfee;
use DELLMOND\feereciept_student;
use DELLMOND\feereciept_sc;
use DELLMOND\deleted_feereciept_sc;
use DELLMOND\deleted_feereciept_student;
use DB;
use Excel;

class RecieptDeleteController extends Controller
{
    public function verifyadmin()
    { //echo md5('dineshchandra');
        return view('deletereciept.adminpasswordverify');
    }
    public function verifyadminpassword(Request $request)
    { 
        $sessionid                  =       Session('valid_id');

        $field = Input::all();                        //adminreptmanager password
        $password    = isset( $field['password'] )?   $field['password']   : '0' ;
        $encryptedpassword =  md5($password);
        
        
    
      $data=\DB::table('administrator')->where('sessionid', $sessionid)->where('position', '=', 'adminreptmanager')->where('password', '=', $encryptedpassword)->orderBy('id', 'desc')->count('*');
       if($data > 0)
        { 
             $request->session()->put('key', 'permitted');
        return redirect('/delete-paid-fee-list');   
        }
        else
        {
            //echo 'You entered the wrong password OR the user "adminreptmanager" does not exists';
            return view('permissiondeny');
        }
        
      /*    if($data > '0')
        {
            $array_once_fee = array();
        $once_req_fee_array = DB::table('feetypes')->where('once_required', '=', '1')->pluck('fee_type');

        foreach ($once_req_fee_array as $key => $value) {
           $reqq = DB::table('feereciept_scs')->where('month_admno_code', '=', $admission_no.$value)->count('admission_no');
             if($reqq == '0') { array_push($array_once_fee, $value);  }
             }

        $m = array('jan', 'feb', 'march', 'april', 'may', 'june', 'july', 'aug', 'sep', 'oct', 'nov', 'dec',);
        $options = array_merge($m, $array_once_fee);
        }
        print_r($options);
       */ // return view('fee.paidfeedeletetablegenerate')->with(['data'=>$data,'form_heading' => 'Select the Month to proceed',]);
       
    }
    public function deletepaidfeelistgenerate(Request $request)
    {
        $sessionid                  =       Session('valid_id');

        $field = Input::all();
      $value = $request->session()->get('key');
      if($value == 'permitted')
      {

        $data=\DB::table('feereciept_students')->where('sessionid', $sessionid)->select('rept_no', 'admission_no', 'student_name', 'class_section', 'father_name','total_paid','fee_months', 'updated_at')->orderBy('rept_no', 'desc')->get();
        return view('deletereciept.listallreciept')->with(['data'=>$data,'form_heading' => 'Select the Month to proceed',]);
      }
      else
      {
        echo "Something went wrong! You don't have permission to delete the reciept, Please Retry Again";
      }
    }
    public function deletepaidreciept(Request $request, $month, $x)
    {
        $sessionid                  =       Session('valid_id');

        $value = $request->session()->get('key');
        if($value != 'permitted')
        { return view('permissiondeny');  }
    
        $getupdated_at = \DELLMOND\getprintreciept::getstudentinner_reciept_no( $x ); 
        $rept ="";
        foreach ($getupdated_at as $key => $value) 
        {
            foreach ($value as $k => $v) 
            {
            $rept=  $v;
            }
        }
            $rept       = explode(" , ",$rept);




            foreach ($rept as $key => $value)
            {
                // extracting and saving to another table
                $data=\DB::table('feereciept_scs')->select('*')->where('sessionid', $sessionid)->orderBy('rept_no', 'desc')->where('rept_no','=',$value)->get()->toArray();
                
                $sub = new deleted_feereciept_sc;
                foreach ($data as $key => $value) {
                    foreach ($value as $k => $v) {
                    $sub->$k = $v;
                    }
                }
                $sub->sessionid     = $sessionid;
                $sub->Save();
                // extraction completed here
            }






            foreach ($rept as $key => $value)
            {

                \DB::table('feereciept_scs')->where('rept_no','=',$value)->where('sessionid', $sessionid)->delete();
            }

    	// extracting and saving to another table
                $data=\DB::table('feereciept_students')->select('*')->where('sessionid', $sessionid)->where('rept_no','=',$x)->get()->toArray();
               
                $sub = new deleted_feereciept_student;
                foreach ($data as $key => $value) {
                    foreach ($value as $k => $v) {
                    $sub->$k = $v;
                    }
                }
                $sub->sessionid = $sessionid;
                $sub->Save();
        // extraction completed here

            \DB::table('feereciept_students')->where('sessionid', $sessionid)->where('rept_no','=',$x)->delete();

            $request->session()->put('key', 'notpermitted');    

        return view('thanks');
        
    }
}

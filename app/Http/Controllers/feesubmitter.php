<?php

namespace DELLMOND\Http\Controllers;
use DELLMOND\Http\Requests;
use DELLMOND\User;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Input;
use DELLMOND\classmgmt;
use DELLMOND\feeamount;
use DELLMOND\feetype;
use DELLMOND\student;
use DELLMOND\discountfee;
use DELLMOND\feereciept_student;
use DELLMOND\feereciept_sc;
use DB;
use Excel;

class feesubmitter extends Controller
{
    public function payselectedstudent($admission_no)
    {
        $sessionid                  =       Session('valid_id');

        // getting once req fee variable --- start
        $array_once_fee = array();
        $once_req_fee_array = DB::table('feetypes')->where('sessionid', $sessionid)->where('once_required', '=', '1')->pluck('fee_type');

        foreach ($once_req_fee_array as $key => $value) {
           $reqq = DB::table('feereciept_scs')->where('sessionid', $sessionid)->where('month_admno_code', '=', $admission_no.$value)->count('admission_no');
             if($reqq == '0') { array_push($array_once_fee, $value);  }
        }
       
        // getting once req fee variable --- stop

        $jan = DB::table('feereciept_scs')->where('sessionid', $sessionid)->where('month_admno_code', '=', $admission_no."jan")->count('admission_no');
        $feb = DB::table('feereciept_scs')->where('sessionid', $sessionid)->where('month_admno_code', '=', $admission_no."feb")->count('admission_no');
        $march = DB::table('feereciept_scs')->where('sessionid', $sessionid)->where('month_admno_code', '=', $admission_no."march")->count('admission_no');
        $april = DB::table('feereciept_scs')->where('sessionid', $sessionid)->where('month_admno_code', '=', $admission_no."april")->count('admission_no');
        $may = DB::table('feereciept_scs')->where('sessionid', $sessionid)->where('month_admno_code', '=', $admission_no."may")->count('admission_no');
        $june = DB::table('feereciept_scs')->where('sessionid', $sessionid)->where('month_admno_code', '=', $admission_no."june")->count('admission_no');
        $july = DB::table('feereciept_scs')->where('sessionid', $sessionid)->where('month_admno_code', '=', $admission_no."july")->count('admission_no');
        $aug = DB::table('feereciept_scs')->where('sessionid', $sessionid)->where('month_admno_code', '=', $admission_no."aug")->count('admission_no');
        $sept = DB::table('feereciept_scs')->where('sessionid', $sessionid)->where('month_admno_code', '=', $admission_no."sep")->count('admission_no');
        $oct = DB::table('feereciept_scs')->where('sessionid', $sessionid)->where('month_admno_code', '=', $admission_no."oct")->count('admission_no');
        $nov = DB::table('feereciept_scs')->where('sessionid', $sessionid)->where('month_admno_code', '=', $admission_no."nov")->count('admission_no');
        $dec = DB::table('feereciept_scs')->where('sessionid', $sessionid)->where('month_admno_code', '=', $admission_no."dec")->count('admission_no');

        $data=\DB::table('students')->select('student_name', 'class_section', 'father_name', 'dob', 'admission_no', 'transport_route')->where('sessionid', $sessionid)->where('admission_no','=',$admission_no)->get();
         return view('fee.paymentmonthselect')->with(['data'=>$data, 'form_heading' => 'select student',
            'jan' => $jan,
            'feb' => $feb,
            'march' => $march,
            'april' => $april,
            'may' => $may,
            'june' => $june,
            'july' => $july,
            'aug' => $aug,
            'sep' => $sept,
            'oct' => $oct,
            'nov' => $nov,
            'dec' => $dec,
            'once_required' => $array_once_fee,
            ]);
    }

    public function determine_transport_amount($admission_no)
    {
        $sessionid                  =       Session('valid_id');

        $transportation_status ;
        $transportation_amount ;
        $transp = DB::table('students')->where('transport_route', '>', '0')->where('sessionid', $sessionid)->where('admission_no', '=', $admission_no)->count('admission_no');

       if($transp == '0') { $transportation_status = 0 ;  }
       else               { $transportation_status = 1 ;  }

       if($transportation_status == 1)
            {
                $r = 0 ; 
                $tprroute = DB::table('students')->where('sessionid', $sessionid)->where('admission_no', '=', $admission_no)->pluck('transport_route');
                foreach ($tprroute as $key => $value) {
                    $r = $value ; 
                } 
                // now we got tansport route, we have to calc amount at that route 
                $amttpr = DB::table('transportroutes')->where('sessionid', $sessionid)->where('id', '=', $r)->pluck('amount');
                foreach ($amttpr as $title) {   $transportation_amount = $title;  }

            }
       else  { $transportation_amount = 0 ;  }
       return $transportation_amount;
    }


    public function confirmpayment()
    {
        $sessionid                  =       Session('valid_id');

        $field = Input::all();
        $admission_no = isset($field['admission_no']) ? $field['admission_no'] : '0';
        $student_name = isset($field['student_name']) ? $field['student_name'] : '0';
        $class_section = isset($field['class_section']) ? $field['class_section'] : '0';
        $parent_name = isset($field['parent_name']) ? $field['parent_name'] : '0';
        $admissionfee = isset($field['admissionfee']) ? $field['admissionfee'] : '0';
        $latefee           = isset($field['latefee'])? $field['latefee'] :0;
        $once_required_fee_array = unserialize(base64_decode($field['once_required_fee_array'])); 
        
        $x = new feesubmitter();  // creating object of this class
        $transport_amount = $x->determine_transport_amount($admission_no); // calling fn by help of object

//*******************************
        $feetypeslist =array();  $payingmonths = array();   $feeamt =array();
       
       // generate the total types of fee
        $feetype = DB::table('feetypes')->select('fee_type')->where('sessionid', $sessionid)->get();
        foreach ($feetype as $key => $value) {
            foreach ($value as $k => $i) {
                 $am = DB::table('feeamounts')->select('amount')->where('sessionid', $sessionid)->where('fee_type', '=', $i)->get();
                 array_push($feetypeslist, $i); array_push($feeamt, 0);
            }
        }

        // checking the status of checked box for traversing individuals name
       $arr =array();        $month_counter = 0;
       $m = array('jan', 'feb', 'march', 'april', 'may', 'june', 'july', 'aug', 'sep', 'oct', 'nov', 'dec',);
        foreach ($m as $key => $value) 
        {  $temp = isset($field[$value]) ? $field[$value] : '0';
             if( $temp !='0' )
             {
               array_push($payingmonths, $value);   $month_counter += 1;
              }
        }
        $once_required_fee_copy = $once_required_fee_array;
        foreach ($once_required_fee_copy as $key => $value) 
        {  $temp = isset($field[$value]) ? $field[$value] : '0';
             if( $temp =='0' )
             {
               $once_required_fee_array = array_diff($once_required_fee_array, array($value));
              }
        }
       
      
       // calc the payable total amount by traversing each fee type and checking whether that type of fee is applicable in that particular month. if yes than add the amount in the array corresponding to the fee type.
       foreach ($payingmonths as $key => $payingmonth_column) 
       { 
         foreach ($feetypeslist as $k => $feetype_individual) 
         {

            $entrypass = DB::table('feetypes')->where('sessionid', $sessionid)->where('fee_type', '=', $feetype_individual)->where($payingmonth_column, '=', '1')->count('id');
            if($entrypass==1)
            {       // we found that fee payable in the traversing month
                    $amt = DB::table('feeamounts')->where('sessionid', $sessionid)->where('fee_type', '=', $feetype_individual)->where('class_section', '=', $class_section)->count('amount');
                    if($amt == 1)
                    {    // calc fee amount of particular fee type for particular class
                        $amtt = DB::table('feeamounts')->where('sessionid', $sessionid)->where('fee_type', '=', $feetype_individual)->where('class_section', '=', $class_section)->pluck('amount');
                    
                    $feeamt[$k] = $feeamt[$k] + $amtt[0];  
                    }
                    }        
            }
         }


        foreach ($once_required_fee_array as $key => $fee_element) 
        { 
         foreach ($feetypeslist as $k => $feetype_individual) 
         {
            if( $fee_element == $feetype_individual )
            {      
                $feetype_presence_counter = DB::table('feeamounts')->where('sessionid', $sessionid)->where('fee_type', '=', $feetype_individual)->where('class_section', '=', $class_section)->count('amount');
                    if($feetype_presence_counter == 1)
                    {    
                        $fee_value = DB::table('feeamounts')->where('sessionid', $sessionid)->where('fee_type', '=', $feetype_individual)->where('class_section', '=', $class_section)->pluck('amount');
                    
                    $feeamt[$k] = $feeamt[$k] + $fee_value[0];  
                    }
                    }        
            }
         }


          
         $total_transport_amount = $month_counter * $transport_amount ;
         array_push($feetypeslist, 'Transportation Fee');
         array_push($feeamt, $total_transport_amount);

          array_push($feetypeslist, 'Late Fee');
          array_push($feeamt, $latefee);

          // getting discount provider list
         $discountprovider=\DB::table('discountproviders')->where('sessionid', $sessionid)->select('discountprovider_name')->orderBy('id', 'desc')->get();

          // calc overall total payable amount
          $total_payable_amount =0;
          foreach ($feeamt as $key => $value) {
              $total_payable_amount += $value;
          }
          //$total += $latefee;
          //$total += $transportation_fee;
          
 
          return view('fee.laststeppaymentconfirm')->with(['admission_no'=>$admission_no, 'form_heading' => 'Confirm Before Final Payment',
            'student_name' => $student_name,
            'class_section' => $class_section,
            'parent_name' => $parent_name,
            'feetypeslist' => $feetypeslist,
            'payingmonths' => $payingmonths,
            'amount' => $feeamt,
            'latefee' => $latefee,
            'discountprovider' => $discountprovider,
            'total' => $total_payable_amount,
            'once_required_fee_array' => $once_required_fee_array,
            ]);
            
            
    }


    public function confirm_cheque_payment()
    {
        $sessionid                  =       Session('valid_id');

        $field = Input::all();
        $admission_no = isset($field['admission_no']) ? $field['admission_no'] : '0';
        $student_name = isset($field['student_name']) ? $field['student_name'] : '0';
        $class_section = isset($field['class_section']) ? $field['class_section'] : '0';
        $parent_name = isset($field['parent_name']) ? $field['parent_name'] : '0';
        $admissionfee = isset($field['admissionfee']) ? $field['admissionfee'] : '0';
        $latefee           = isset($field['latefee'])? $field['latefee'] :0;
        $once_required_fee_array = unserialize(base64_decode($field['once_required_fee_array'])); 
        
        $x = new feesubmitter();  // creating object of this class
        $transport_amount = $x->determine_transport_amount($admission_no); // calling fn by help of object

//*******************************
        $feetypeslist =array();  $payingmonths = array();   $feeamt =array();
       
       // generate the total types of fee
        $feetype = DB::table('feetypes')->where('sessionid', $sessionid)->select('fee_type')->get();
        foreach ($feetype as $key => $value) {
            foreach ($value as $k => $i) {
                 $am = DB::table('feeamounts')->select('amount')->where('sessionid', $sessionid)->where('fee_type', '=', $i)->get();
                 array_push($feetypeslist, $i); array_push($feeamt, 0);          
            }
        }

        // checking the status of checked box for traversing individuals name
       $arr =array();        $month_counter = 0;
       $m = array('jan', 'feb', 'march', 'april', 'may', 'june', 'july', 'aug', 'sep', 'oct', 'nov', 'dec',);
        foreach ($m as $key => $value) 
        {  $temp = isset($field[$value]) ? $field[$value] : '0';
             if( $temp !='0' )
             {
               array_push($payingmonths, $value);   $month_counter += 1;
              }
        }
        $once_required_fee_copy = $once_required_fee_array;
        foreach ($once_required_fee_copy as $key => $value) 
        {  $temp = isset($field[$value]) ? $field[$value] : '0';
             if( $temp =='0' )
             {
               $once_required_fee_array = array_diff($once_required_fee_array, array($value));
              }
        }
       
      
       // calc the payable total amount by traversing each fee type and checking whether that type of fee is applicable in that particular month. if yes than add the amount in the array corresponding to the fee type.
       foreach ($payingmonths as $key => $payingmonth_column) 
       { 
         foreach ($feetypeslist as $k => $feetype_individual) 
         {
            $entrypass = DB::table('feetypes')->where('sessionid', $sessionid)->where('fee_type', '=', $feetype_individual)->where($payingmonth_column, '=', '1')->count('id');
            if($entrypass==1)
            {       // we found that fee payable in the traversing month
                    $amt = DB::table('feeamounts')->where('sessionid', $sessionid)->where('fee_type', '=', $feetype_individual)->where('class_section', '=', $class_section)->count('amount');
                    if($amt == 1)
                    {    // calc fee amount of particular fee type for particular class
                        $amtt = DB::table('feeamounts')->where('sessionid', $sessionid)->where('fee_type', '=', $feetype_individual)->where('class_section', '=', $class_section)->pluck('amount');
                    
                    $feeamt[$k] = $feeamt[$k] + $amtt[0];  
                    }
                    }        
            }
         }


        foreach ($once_required_fee_array as $key => $fee_element) 
        { 
         foreach ($feetypeslist as $k => $feetype_individual) 
         {
            if( $fee_element == $feetype_individual )
            {      
                    $feetype_presence_counter = DB::table('feeamounts')->where('sessionid', $sessionid)->where('fee_type', '=', $feetype_individual)->where('class_section', '=', $class_section)->count('amount');
                    if($feetype_presence_counter == 1)
                    {    
                        $fee_value = DB::table('feeamounts')->where('sessionid', $sessionid)->where('fee_type', '=', $feetype_individual)->where('class_section', '=', $class_section)->pluck('amount');
                    
                    $feeamt[$k] = $feeamt[$k] + $fee_value[0];  
                    }
                    }        
            }
         }


          
         $total_transport_amount = $month_counter * $transport_amount ;
         array_push($feetypeslist, 'Transportation Fee');
         array_push($feeamt, $total_transport_amount);

          array_push($feetypeslist, 'Late Fee');
          array_push($feeamt, $latefee);

          // getting discount provider list
         $discountprovider=\DB::table('discountproviders')->where('sessionid', $sessionid)->select('discountprovider_name')->orderBy('id', 'desc')->get();

          // calc overall total payable amount
          $total_payable_amount =0;
          foreach ($feeamt as $key => $value) {
              $total_payable_amount += $value;
          }
          //$total += $latefee;
          //$total += $transportation_fee;
          
 
          return view('fee.cheque_payment_form')->with(['admission_no'=>$admission_no, 'form_heading' => 'Confirm Before Final Payment',
            'student_name' => $student_name,
            'class_section' => $class_section,
            'parent_name' => $parent_name,
            'feetypeslist' => $feetypeslist,
            'payingmonths' => $payingmonths,
            'amount' => $feeamt,
            'latefee' => $latefee,
            'discountprovider' => $discountprovider,
            'total' => $total_payable_amount,
            'once_required_fee_array' => $once_required_fee_array,
            ]);
            
            
    }

    public function confirm_dd_payment()
    {
        $sessionid                  =       Session('valid_id');

        $field = Input::all();
        $admission_no = isset($field['admission_no']) ? $field['admission_no'] : '0';
        $student_name = isset($field['student_name']) ? $field['student_name'] : '0';
        $class_section = isset($field['class_section']) ? $field['class_section'] : '0';
        $parent_name = isset($field['parent_name']) ? $field['parent_name'] : '0';
        $admissionfee = isset($field['admissionfee']) ? $field['admissionfee'] : '0';
        $latefee           = isset($field['latefee'])? $field['latefee'] :0;
        $once_required_fee_array = unserialize(base64_decode($field['once_required_fee_array'])); 
        
        $x = new feesubmitter();  // creating object of this class
        $transport_amount = $x->determine_transport_amount($admission_no); // calling fn by help of object

//*******************************
        $feetypeslist =array();  $payingmonths = array();   $feeamt =array();
       
       // generate the total types of fee

        $feetype = DB::table('feetypes')->where('sessionid', $sessionid)->select('fee_type')->get();
        foreach ($feetype as $key => $value) {
            foreach ($value as $k => $i) {
                 $am = DB::table('feeamounts')->where('sessionid', $sessionid)->select('amount')->where('fee_type', '=', $i)->get();
                 array_push($feetypeslist, $i); array_push($feeamt, 0);          
            }
        }

        // checking the status of checked box for traversing individuals name
       $arr =array();        $month_counter = 0;
       $m = array('jan', 'feb', 'march', 'april', 'may', 'june', 'july', 'aug', 'sep', 'oct', 'nov', 'dec',);
        foreach ($m as $key => $value) 
        {  $temp = isset($field[$value]) ? $field[$value] : '0';
             if( $temp !='0' )
             {
               array_push($payingmonths, $value);   $month_counter += 1;
              }
        }
        $once_required_fee_copy = $once_required_fee_array;
        foreach ($once_required_fee_copy as $key => $value) 
        {  $temp = isset($field[$value]) ? $field[$value] : '0';
             if( $temp =='0' )
             {
               $once_required_fee_array = array_diff($once_required_fee_array, array($value));
              }
        }
       
      
       // calc the payable total amount by traversing each fee type and checking whether that type of fee is applicable in that particular month. if yes than add the amount in the array corresponding to the fee type.
       foreach ($payingmonths as $key => $payingmonth_column) 
       { 
         foreach ($feetypeslist as $k => $feetype_individual) 
         {
            $entrypass = DB::table('feetypes')->where('sessionid', $sessionid)->where('fee_type', '=', $feetype_individual)->where($payingmonth_column, '=', '1')->count('id');
            if($entrypass==1)
            {       // we found that fee payable in the traversing month
                    $amt = DB::table('feeamounts')->where('sessionid', $sessionid)->where('fee_type', '=', $feetype_individual)->where('class_section', '=', $class_section)->count('amount');
                    if($amt == 1)
                    {    // calc fee amount of particular fee type for particular class
                        $amtt = DB::table('feeamounts')->where('sessionid', $sessionid)->where('fee_type', '=', $feetype_individual)->where('class_section', '=', $class_section)->pluck('amount');
                    
                    $feeamt[$k] = $feeamt[$k] + $amtt[0];  
                    }
                    }        
            }
         }


        foreach ($once_required_fee_array as $key => $fee_element) 
        { 
         foreach ($feetypeslist as $k => $feetype_individual) 
         {
            if( $fee_element == $feetype_individual )
            {      
                    $feetype_presence_counter = DB::table('feeamounts')->where('sessionid', $sessionid)->where('fee_type', '=', $feetype_individual)->where('class_section', '=', $class_section)->count('amount');
                    if($feetype_presence_counter == 1)
                    {    
                        $fee_value = DB::table('feeamounts')->where('sessionid', $sessionid)->where('fee_type', '=', $feetype_individual)->where('class_section', '=', $class_section)->pluck('amount');
                    
                    $feeamt[$k] = $feeamt[$k] + $fee_value[0];  
                    }
                    }        
            }
         }


          
         $total_transport_amount = $month_counter * $transport_amount ;
         array_push($feetypeslist, 'Transportation Fee');
         array_push($feeamt, $total_transport_amount);

          array_push($feetypeslist, 'Late Fee');
          array_push($feeamt, $latefee);

          // getting discount provider list
         $discountprovider=\DB::table('discountproviders')->where('sessionid', $sessionid)->select('discountprovider_name')->orderBy('id', 'desc')->get();

          // calc overall total payable amount
          $total_payable_amount =0;
          foreach ($feeamt as $key => $value) {
              $total_payable_amount += $value;
          }
          //$total += $latefee;
          //$total += $transportation_fee;
          
 
          return view('fee.dd_payment_form')->with(['admission_no'=>$admission_no, 'form_heading' => 'Confirm Before Final Payment',
            'student_name' => $student_name,
            'class_section' => $class_section,
            'parent_name' => $parent_name,
            'feetypeslist' => $feetypeslist,
            'payingmonths' => $payingmonths,
            'amount' => $feeamt,
            'latefee' => $latefee,
            'discountprovider' => $discountprovider,
            'total' => $total_payable_amount,
            'once_required_fee_array' => $once_required_fee_array,
            ]);
            
            
    }

    public function calculateamount($typeoffee, $month, $class_section)
    {
        $sessionid                  =       Session('valid_id');

        $subtotal = 0;
         foreach ($typeoffee as $k => $v) 
         {
            $entrypass = DB::table('feetypes')->where('sessionid', $sessionid)->where('fee_type', '=', $v)->where($month, '=', '1')->count('id');
            if($entrypass==1)
               {
                 $amt = DB::table('feeamounts')->where('fee_type', '=', $v)->where('sessionid', $sessionid)->where('class_section', '=', $class_section)->count('amount');
                    if($amt == 1)
                    {
                        $amtt = DB::table('feeamounts')->where('sessionid', $sessionid)->where('fee_type', '=', $v)->where('class_section', '=', $class_section)->pluck('amount');
                    
                    $subtotal = $subtotal + $amtt[0];
                    }
               }
                    
            }
            return $subtotal;       
    }


    public function calculate_once_requiredfee_amount($typeoffee, $class_section)
    {
        $sessionid                  =       Session('valid_id');

        $subtotal = 0;
         
            $entrypass = DB::table('feetypes')->where('sessionid', $sessionid)->where('fee_type', '=', $typeoffee)->where('once_required', '=', '1')->count('id');
            if($entrypass==1)
               {
                 $amt = DB::table('feeamounts')->where('sessionid', $sessionid)->where('fee_type', '=', $typeoffee)->where('class_section', '=', $class_section)->count('amount');
                    if($amt == 1)
                    {
                        $amtt = DB::table('feeamounts')->where('sessionid', $sessionid)->where('fee_type', '=', $typeoffee)->where('class_section', '=', $class_section)->pluck('amount');
                    
                    $subtotal = $subtotal + $amtt[0];
                    }
               }
                    
            
            return $subtotal;       
    }



    public function confirmpaymentcommit()
    {
        $sessionid                  =       Session('valid_id');

        $field = Input::all();
        $admission_no    = $field['admission_no'];
        $student_name    = $field['student_name'];
        $class_section   = $field['class_section'];
        $parent_name     = isset($field['parent_name'])?$field['parent_name']:'N.A.';
        $total           = $field['total'];
        $discountprovider= isset($field['discountprovider'])? $field['discountprovider'] :'0';
        $discountreason  = isset($field['discountreason'])? $field['discountreason'] :'0';
        $discountamount  = isset($field['discountamount'])? $field['discountamount'] :0;
        $latefee           = isset($field['latefee'])? $field['latefee'] :0;

        $reciept           = array();
        $extradetails    = "N.A.";
        $totalpayable = $total;   // copy of total
        $payablethismonth = 0; 
        $discountgiventhismonth = 0; 
        $datetime = 0;
        $latefeetoken =0;
        $transportationfeetoken =0;
        $latefeecopy  = $latefee;
        

        $pm = unserialize(base64_decode($field['pm']));  // paying month
        $ftl = unserialize(base64_decode($field['ftl']));  // fee type list
        $amt = unserialize(base64_decode($field['amt']));  // amount corresponding to fee type
        $once_required_fee_array = unserialize(base64_decode($field['once_required_fee_array']));  // array corresponding to one time fee type

        

        $m = array('jan', 'feb', 'march', 'april', 'may', 'june', 'july', 'aug', 'sep', 'oct', 'nov', 'dec', 'once_required');
        foreach ($pm as $key => $value) {
            foreach ($m as $mkey => $mvalue) {
                
            
            if($value==$mvalue)
            {  // $subtotal_transport = 0;
                $x = new feesubmitter();
                $subtotal_fee       = $x->calculateamount($ftl, $mvalue, $class_section);
                $subtotal_transport = $x->determine_transport_amount($admission_no);
                $subtotal = $subtotal_fee + $subtotal_transport;
                if($discountamount >= 0)
                  {
                    if($discountamount > $subtotal ) 
                        {   
                        // if discount is more than individual month sum
                            $payablethismonth = 0;
                            $totalpayable = $totalpayable - $subtotal;
                            $discountamount = $discountamount - $subtotal; 
                            $extradetails = "Total payable amount was".$subtotal."and".$subtotal." amount is given as discount in month ". $mvalue;
                            $discountgiventhismonth = $subtotal;
                            //$latefeetoken =1;
                        }
                    else{ if($discountamount >= 0) {  
                        $payablethismonth = $subtotal - $discountamount; 
                        $totalpayable = $totalpayable - $subtotal;   
                        $extradetails = "Total payable amount was".$subtotal."and".$discountamount." amount is given as discount in month ". $mvalue;  
                        $discountgiventhismonth = $discountamount; $discountamount = 0; }  
                         }
                    }

                    if( $latefeetoken == 0) { if( $latefee >= 0) { $subtotal += $latefee; $latefeetoken = 1; 
                                                $payablethismonth += $latefee; $latefeetoken = 1; } }
                $notif_transport = 'No'; if($subtotal_transport > 0 ) { $notif_transport = 'Yes'; }
                $sub = new feereciept_sc;   // fee reciept of school of each month
                $sub->admission_no          = $admission_no;
                $sub->student_name          = $student_name;
                $sub->class_section         = $class_section;
                $sub->father_name           = $parent_name;
                $sub->fee_months            = $mvalue;
                $sub->month_admno_code      = $admission_no."".$mvalue;
                $sub->requireonce_included  = $class_section;
                $sub->requireonce_amount    = 'N.A.';
                $sub->discount_included     = isset($discountgiventhismonth)? 'Yes' : 'No';
                $sub->discount_given        = $discountgiventhismonth;
                $sub->discount_total_offered = isset($field['discountamount'])? $field['discountamount'] :0;
                $sub->discountprovider     = $discountprovider;
                $sub->discountreason       = $discountreason;
                $sub->transportation_included = $notif_transport;
                $sub->transportation_amount = $subtotal_transport;
                $sub->total_payable         = $subtotal;
                $sub->total_paid            = $payablethismonth;
                $sub->payment_mode          = 'cash';
                $sub->feetype_array         = implode(" , ",$ftl);
                $sub->feeamountarray        = implode(" , ",$amt);
                $sub->associated_reciept_array = implode(" , ",$reciept);
                $sub->extradetails          = $extradetails ;
                $sub->sessionid = $sessionid;
                $sub->Save();


                array_push($reciept, $sub->id);
                $datetime = $sub->updated_at;                             

                // now re-iniatializing temporary variable Inportant 
                $payablethismonth = 0;
                $discountgiventhismonth = 0;
                $transportationfeecopy =0;

               
            }
            // adding value month by month in school payment internal table over here,
          }
        }



        $m = $once_required_fee_array;
       
            foreach ($m as $mkey => $mvalue) {
                
            
            
             $subtotal_transport = 0;
                $x = new feesubmitter();
                $subtotal_fee       = $x->calculate_once_requiredfee_amount($mvalue, $class_section);
                //$subtotal_transport = $x->determine_transport_amount($admission_no); because transport is only on months
                $subtotal = $subtotal_fee + $subtotal_transport;
                if($discountamount >= 0)
                  {
                    if($discountamount > $subtotal ) 
                        {   
                        // if discount is more than individual month sum
                            $payablethismonth = 0;
                            $totalpayable = $totalpayable - $subtotal;
                            $discountamount = $discountamount - $subtotal; 
                            $extradetails = "Total payable amount was".$subtotal."and".$subtotal." amount is given as discount in month ". $mvalue;
                            $discountgiventhismonth = $subtotal;
                            //$latefeetoken =1;
                        }
                    else{ if($discountamount >= 0) {  
                        $payablethismonth = $subtotal - $discountamount; 
                        $totalpayable = $totalpayable - $subtotal;   
                        $extradetails = "Total payable amount was".$subtotal."and".$discountamount." amount is given as discount in month ". $mvalue;  
                        $discountgiventhismonth = $discountamount; $discountamount = 0; }  
                         }
                    }

                    if( $latefeetoken == 0) { if( $latefee >= 0) { $subtotal += $latefee; $latefeetoken = 1; 
                                                $payablethismonth += $latefee; $latefeetoken = 1; } }
                array_push($pm, $mvalue);
                $notif_transport = 'No'; if($subtotal_transport > 0 ) { $notif_transport = 'Yes'; }
                $sub = new feereciept_sc;   // fee reciept of school of each month
                $sub->admission_no          = $admission_no;
                $sub->student_name          = $student_name;
                $sub->class_section         = $class_section;
                $sub->father_name           = $parent_name;
                $sub->fee_months            = $mvalue;
                $sub->month_admno_code      = $admission_no."".$mvalue;
                $sub->requireonce_included  = $class_section;
                $sub->requireonce_amount    = 'N.A.';
                $sub->discount_included     = isset($discountgiventhismonth)? 'Yes' : 'No';
                $sub->discount_given        = $discountgiventhismonth;
                $sub->discount_total_offered = isset($field['discountamount'])? $field['discountamount'] :0;
                $sub->discountprovider      = $discountprovider;
                $sub->discountreason        = $discountreason;
                $sub->transportation_included = $notif_transport;
                $sub->transportation_amount = $subtotal_transport;
                $sub->total_payable         = $subtotal;
                $sub->total_paid            = $payablethismonth;
                $sub->payment_mode          = 'cash';
                $sub->feetype_array         = implode(" , ",$ftl);
                $sub->feeamountarray        = implode(" , ",$amt);
                $sub->associated_reciept_array = implode(" , ",$reciept);
                $sub->extradetails          = $extradetails ;
                $sub->sessionid             = $sessionid;
                $sub->Save();


                array_push($reciept, $sub->id);
                $datetime = $sub->updated_at;


                // now re-iniatializing temporary variable Inportant 
                $payablethismonth = 0;
                $discountgiventhismonth = 0;
                $transportationfeecopy =0;

               
            }
            // adding value month by month in school payment internal table over here,
          
       




        // loop ends here
           

            $y = isset($field['discountamount'])? $field['discountamount'] :0;
            $latefee = isset($field['latefee'])? $field['latefee'] :0;
            $xx = $field['total'] -  $y;           // this is original payment table
                $originalrecipt = new feereciept_student;  
                $originalrecipt->admission_no          = $admission_no;
                $originalrecipt->student_name          = $student_name;
                $originalrecipt->class_section         = $class_section;
                $originalrecipt->father_name           = $parent_name;
                $originalrecipt->fee_months            = implode(" , ",$pm);
                $originalrecipt->feetypearray          = implode(" , ",$ftl);
                $originalrecipt->feeamountarray        = implode(" , ",$amt);
                $originalrecipt->discount_provider     = $discountprovider ;
                $originalrecipt->discount_amount       = isset($field['discountamount'])? $field['discountamount'] :0;
                $originalrecipt->total_payable         = $field['total'];
                $originalrecipt->total_paid            = $xx;
                $originalrecipt->extradetails          = implode(" , ",$reciept);
                $originalrecipt->sessionid             = $sessionid;
                $originalrecipt->Save();


               return redirect('/Reprint-fee-Reciept/'.$originalrecipt->id);
    }



    public function confirm_cheque_paymentcommit()
    {
        $sessionid                  =       Session('valid_id');

        $field = Input::all();
        $admission_no    = $field['admission_no'];
        $student_name    = $field['student_name'];
        $class_section   = $field['class_section'];
        $parent_name     = isset($field['parent_name'])?$field['parent_name']:'N.A.';
        $total           = $field['total'];
        $discountprovider= isset($field['discountprovider'])? $field['discountprovider'] :'0';
        $discountreason  = isset($field['discountreason'])? $field['discountreason'] :'0';
        $discountamount  = isset($field['discountamount'])? $field['discountamount'] :0;
        $latefee           = isset($field['latefee'])? $field['latefee'] :0;

        $cheque_no           = isset($field['cheque_no'])? $field['cheque_no'] :0;
        $cheque_date           = isset($field['cheque_date'])? $field['cheque_date'] :0;
        $cheque_bank           = isset($field['cheque_bank'])? $field['cheque_bank'] :0;
        $cheque_branch           = isset($field['cheque_branch'])? $field['cheque_branch'] :0;
        $cheque_amount           = isset($field['cheque_amount'])? $field['cheque_amount'] :0;
        

        $reciept           = array();
        $extradetails    = "N.A.";
        $totalpayable = $total;   // copy of total
        $payablethismonth = 0; 
        $discountgiventhismonth = 0; 
        $datetime = 0;
        $latefeetoken =0;
        $transportationfeetoken =0;
        $latefeecopy  = $latefee;
        

        $pm = unserialize(base64_decode($field['pm']));  // paying month
        $ftl = unserialize(base64_decode($field['ftl']));  // fee type list
        $amt = unserialize(base64_decode($field['amt']));  // amount corresponding to fee type
        $once_required_fee_array = unserialize(base64_decode($field['once_required_fee_array']));  // array corresponding to one time fee type

        

        $m = array('jan', 'feb', 'march', 'april', 'may', 'june', 'july', 'aug', 'sep', 'oct', 'nov', 'dec', 'once_required');
        foreach ($pm as $key => $value) {
            foreach ($m as $mkey => $mvalue) {
                
            
            if($value==$mvalue)
            {  // $subtotal_transport = 0;
                $x = new feesubmitter();
                $subtotal_fee       = $x->calculateamount($ftl, $mvalue, $class_section);
                $subtotal_transport = $x->determine_transport_amount($admission_no);
                $subtotal = $subtotal_fee + $subtotal_transport;
                if($discountamount >= 0)
                  {
                    if($discountamount > $subtotal ) 
                        {   
                        // if discount is more than individual month sum
                            $payablethismonth = 0;
                            $totalpayable = $totalpayable - $subtotal;
                            $discountamount = $discountamount - $subtotal; 
                            $extradetails = "Total payable amount was".$subtotal."and".$subtotal." amount is given as discount in month ". $mvalue;
                            $discountgiventhismonth = $subtotal;
                            //$latefeetoken =1;
                        }
                    else{ if($discountamount >= 0) {  
                        $payablethismonth = $subtotal - $discountamount; 
                        $totalpayable = $totalpayable - $subtotal;   
                        $extradetails = "Total payable amount was".$subtotal."and".$discountamount." amount is given as discount in month ". $mvalue;  
                        $discountgiventhismonth = $discountamount; $discountamount = 0; }  
                         }
                    }

                    if( $latefeetoken == 0) { if( $latefee >= 0) { $subtotal += $latefee; $latefeetoken = 1; 
                                                $payablethismonth += $latefee; $latefeetoken = 1; } }
                $notif_transport = 'No'; if($subtotal_transport > 0 ) { $notif_transport = 'Yes'; }
                $sub = new feereciept_sc;   // fee reciept of school of each month
                $sub->admission_no          = $admission_no;
                $sub->student_name          = $student_name;
                $sub->class_section         = $class_section;
                $sub->father_name           = $parent_name;
                $sub->fee_months            = $mvalue;
                $sub->month_admno_code      = $admission_no."".$mvalue;
                $sub->requireonce_included  = $class_section;
                $sub->requireonce_amount    = 'N.A.';
                $sub->discount_included     = 'No';
                $sub->discount_given        = $discountgiventhismonth;
                $sub->discount_total_offered = isset($field['discountamount'])? $field['discountamount'] :0;
                $sub->discountprovider     = $discountprovider;
                $sub->discountreason       = $discountreason;
                $sub->transportation_included = $notif_transport;
                $sub->transportation_amount = $subtotal_transport;
                $sub->total_payable         = $subtotal;
                $sub->total_paid            = $payablethismonth;
                $sub->payment_mode          = 'cheque';
                $sub->feetype_array         = implode(" , ",$ftl);
                $sub->feeamountarray        = implode(" , ",$amt);
                $sub->associated_reciept_array = implode(" , ",$reciept);
                $sub->extradetails          = $extradetails ;
                $sub->cheque_no                = $cheque_no;
                $sub->cheque_date              = $cheque_date;
                $sub->cheque_bank              = $cheque_bank;
                $sub->cheque_branch            = $cheque_branch;
                $sub->cheque_amount            = $cheque_amount;
                $sub->cheque_verify            = 'Pending';
                $sub->sessionid                = $sessionid;
                $sub->Save();


                array_push($reciept, $sub->id);
                $datetime = $sub->updated_at;                             

                // now re-iniatializing temporary variable Inportant 
                $payablethismonth = 0;
                $discountgiventhismonth = 0;
                $transportationfeecopy =0;

               
            }
            // ading value month by month in school payment internal table over here,
          }
        }



        $m = $once_required_fee_array;
       
            foreach ($m as $mkey => $mvalue) {
                
            
            
             $subtotal_transport = 0;
                $x = new feesubmitter();
                $subtotal_fee       = $x->calculate_once_requiredfee_amount($mvalue, $class_section);
                //$subtotal_transport = $x->determine_transport_amount($admission_no);  because transport is only on months
                $subtotal = $subtotal_fee + $subtotal_transport;
                if($discountamount >= 0)
                  {
                    if($discountamount > $subtotal ) 
                        {   
                        // if discount is more than individual month sum
                            $payablethismonth = 0;
                            $totalpayable = $totalpayable - $subtotal;
                            $discountamount = $discountamount - $subtotal; 
                            $extradetails = "Total payable amount was".$subtotal."and".$subtotal." amount is given as discount in month ". $mvalue;
                            $discountgiventhismonth = $subtotal;
                            //$latefeetoken =1;
                        }
                    else{ if($discountamount >= 0) {  
                        $payablethismonth = $subtotal - $discountamount; 
                        $totalpayable = $totalpayable - $subtotal;   
                        $extradetails = "Total payable amount was".$subtotal."and".$discountamount." amount is given as discount in month ". $mvalue;  
                        $discountgiventhismonth = $discountamount; $discountamount = 0; }  
                         }
                    }

                    if( $latefeetoken == 0) { if( $latefee >= 0) { $subtotal += $latefee; $latefeetoken = 1; 
                                                $payablethismonth += $latefee; $latefeetoken = 1; } }
                array_push($pm, $mvalue);
                $notif_transport = 'No'; if($subtotal_transport > 0 ) { $notif_transport = 'Yes'; }
                $sub = new feereciept_sc;   // fee reciept of school of each month
                $sub->admission_no          = $admission_no;
                $sub->student_name          = $student_name;
                $sub->class_section         = $class_section;
                $sub->father_name           = $parent_name;
                $sub->fee_months            = $mvalue;
                $sub->month_admno_code      = $admission_no."".$mvalue;
                $sub->requireonce_included  = $class_section;
                $sub->requireonce_amount    = 'N.A.';
                $sub->discount_included     = 'No';
                $sub->discount_given        = $discountgiventhismonth;
                $sub->discount_total_offered = isset($field['discountamount'])? $field['discountamount'] :0;
                $sub->discountprovider     = $discountprovider;
                $sub->discountreason       = $discountreason;
                $sub->transportation_included = $notif_transport;
                $sub->transportation_amount = $subtotal_transport;
                $sub->total_payable         = $subtotal;
                $sub->total_paid            = $payablethismonth;
                $sub->payment_mode          = 'cheque';
                $sub->feetype_array         = implode(" , ",$ftl);
                $sub->feeamountarray        = implode(" , ",$amt);
                $sub->associated_reciept_array = implode(" , ",$reciept);
                $sub->extradetails          = $extradetails ;
                $sub->cheque_no                = $cheque_no;
                $sub->cheque_date              = $cheque_date;
                $sub->cheque_bank              = $cheque_bank;
                $sub->cheque_branch            = $cheque_branch;
                $sub->cheque_amount            = $cheque_amount;
                $sub->cheque_verify            = 'Pending';
                $sub->sessionid                = $sessionid;
                $sub->Save();


                array_push($reciept, $sub->id);
                $datetime = $sub->updated_at;


                // now re-iniatializing temporary variable Inportant 
                $payablethismonth = 0;
                $discountgiventhismonth = 0;
                $transportationfeecopy =0;

               
            }
            // ading value month by month in school payment internal table over here,
          
       




        // loop ends here
           

            $y = isset($field['discountamount'])? $field['discountamount'] :0;
            $latefee = isset($field['latefee'])? $field['latefee'] :0;
            $xx = $field['total'] -  $y;           // this is original payment table
                $originalrecipt = new feereciept_student;  
                $originalrecipt->admission_no          = $admission_no;
                $originalrecipt->student_name          = $student_name;
                $originalrecipt->class_section         = $class_section;
                $originalrecipt->father_name           = $parent_name;
                $originalrecipt->payment_mode           = 'cheque ('.$cheque_no .')';
                $originalrecipt->fee_months            = implode(" , ",$pm);
                $originalrecipt->feetypearray          = implode(" , ",$ftl);
                $originalrecipt->feeamountarray        = implode(" , ",$amt);
                $originalrecipt->discount_provider     = $discountprovider ;
                $originalrecipt->discount_amount       = isset($field['discountamount'])? $field['discountamount'] :0;
                $originalrecipt->total_payable         = $field['total'];
                $originalrecipt->total_paid            = $xx;
                $originalrecipt->extradetails          = implode(" , ",$reciept);
                $originalrecipt->sessionid            = $sessionid;
                $originalrecipt->Save();


               return redirect('/Reprint-fee-Reciept/'.$originalrecipt->id);
    }



    public function confirm_dd_paymentcommit()
    {
        $sessionid                  =       Session('valid_id');

        $field = Input::all();
        $admission_no    = $field['admission_no'];
        $student_name    = $field['student_name'];
        $class_section   = $field['class_section'];
        $parent_name     = isset($field['parent_name'])?$field['parent_name']:'N.A.';
        $total           = $field['total'];
        $discountprovider= isset($field['discountprovider'])? $field['discountprovider'] :'0';
        $discountreason  = isset($field['discountreason'])? $field['discountreason'] :'0';
        $discountamount  = isset($field['discountamount'])? $field['discountamount'] :0;
        $latefee           = isset($field['latefee'])? $field['latefee'] :0;

        $dd_no           = isset($field['dd_no'])? $field['dd_no'] :0;
        $dd_date           = isset($field['dd_date'])? $field['dd_date'] :0;
        $dd_bank           = isset($field['dd_bank'])? $field['dd_bank'] :0;
        $dd_branch           = isset($field['dd_branch'])? $field['dd_branch'] :0;
        $dd_amount           = isset($field['dd_amount'])? $field['dd_amount'] :0;

        $reciept           = array();
        $extradetails    = "N.A.";
        $totalpayable = $total;   // copy of total
        $payablethismonth = 0; 
        $discountgiventhismonth = 0; 
        $datetime = 0;
        $latefeetoken =0;
        $transportationfeetoken =0;
        $latefeecopy  = $latefee;
        

        $pm = unserialize(base64_decode($field['pm']));  // paying month
        $ftl = unserialize(base64_decode($field['ftl']));  // fee type list
        $amt = unserialize(base64_decode($field['amt']));  // amount corresponding to fee type
        $once_required_fee_array = unserialize(base64_decode($field['once_required_fee_array']));  // array corresponding to one time fee type

        

        $m = array('jan', 'feb', 'march', 'april', 'may', 'june', 'july', 'aug', 'sep', 'oct', 'nov', 'dec', 'once_required');
        foreach ($pm as $key => $value) {
            foreach ($m as $mkey => $mvalue) {
                
            
            if($value==$mvalue)
            {  // $subtotal_transport = 0;
                $x = new feesubmitter();
                $subtotal_fee       = $x->calculateamount($ftl, $mvalue, $class_section);
                $subtotal_transport = $x->determine_transport_amount($admission_no);
                $subtotal = $subtotal_fee + $subtotal_transport;
                if($discountamount >= 0)
                  {
                    if($discountamount > $subtotal ) 
                        {   
                        // if discount is more than individual month sum
                            $payablethismonth = 0;
                            $totalpayable = $totalpayable - $subtotal;
                            $discountamount = $discountamount - $subtotal; 
                            $extradetails = "Total payable amount was".$subtotal."and".$subtotal." amount is given as discount in month ". $mvalue;
                            $discountgiventhismonth = $subtotal;
                            //$latefeetoken =1;
                        }
                    else{ if($discountamount >= 0) {  
                        $payablethismonth = $subtotal - $discountamount; 
                        $totalpayable = $totalpayable - $subtotal;   
                        $extradetails = "Total payable amount was".$subtotal."and".$discountamount." amount is given as discount in month ". $mvalue;  
                        $discountgiventhismonth = $discountamount; $discountamount = 0; }  
                         }
                    }

                    if( $latefeetoken == 0) { if( $latefee >= 0) { $subtotal += $latefee; $latefeetoken = 1; 
                                                $payablethismonth += $latefee; $latefeetoken = 1; } }
                $notif_transport = 'No'; if($subtotal_transport > 0 ) { $notif_transport = 'Yes'; }
                $sub = new feereciept_sc;   // fee reciept of school of each month
                $sub->admission_no          = $admission_no;
                $sub->student_name          = $student_name;
                $sub->class_section         = $class_section;
                $sub->father_name           = $parent_name;
                $sub->fee_months            = $mvalue;
                $sub->month_admno_code      = $admission_no."".$mvalue;
                $sub->requireonce_included  = $class_section;
                $sub->requireonce_amount    = 'N.A.';
                $sub->discount_included     = 'No';
                $sub->discount_given        = $discountgiventhismonth;
                $sub->discount_total_offered = isset($field['discountamount'])? $field['discountamount'] :0;
                $sub->discountprovider     = $discountprovider;
                $sub->discountreason       = $discountreason;
                $sub->transportation_included = $notif_transport;
                $sub->transportation_amount = $subtotal_transport;
                $sub->total_payable         = $subtotal;
                $sub->total_paid            = $payablethismonth;
                $sub->payment_mode          = 'dd';
                $sub->feetype_array         = implode(" , ",$ftl);
                $sub->feeamountarray        = implode(" , ",$amt);
                $sub->associated_reciept_array = implode(" , ",$reciept);
                $sub->extradetails          = $extradetails ;
                $sub->dd_no                = $dd_no;
                $sub->dd_date              = $dd_date;
                $sub->dd_bank              = $dd_bank;
                $sub->dd_branch            = $dd_branch;
                $sub->dd_amount            = $dd_amount;
                $sub->dd_verify            = 'Pending';
                $sub->sessionid            = $sessionid;
                $sub->Save();


                array_push($reciept, $sub->id);
                $datetime = $sub->updated_at;                             

                // now re-iniatializing temporary variable Inportant 
                $payablethismonth = 0;
                $discountgiventhismonth = 0;
                $transportationfeecopy =0;

               
            }
            // adding value month by month in school payment internal table over here,
          }
        }



        $m = $once_required_fee_array;
       
            foreach ($m as $mkey => $mvalue) {
                
            
            
             $subtotal_transport = 0;
                $x = new feesubmitter();
                $subtotal_fee       = $x->calculate_once_requiredfee_amount($mvalue, $class_section);
                //$subtotal_transport = $x->determine_transport_amount($admission_no);  because transport is only on months
                $subtotal = $subtotal_fee + $subtotal_transport;
                if($discountamount >= 0)
                  {
                    if($discountamount > $subtotal ) 
                        {   
                        // if discount is more than individual month sum
                            $payablethismonth = 0;
                            $totalpayable = $totalpayable - $subtotal;
                            $discountamount = $discountamount - $subtotal; 
                            $extradetails = "Total payable amount was".$subtotal."and".$subtotal." amount is given as discount in month ". $mvalue;
                            $discountgiventhismonth = $subtotal;
                            //$latefeetoken =1;
                        }
                    else{ if($discountamount >= 0) {  
                        $payablethismonth = $subtotal - $discountamount; 
                        $totalpayable = $totalpayable - $subtotal;   
                        $extradetails = "Total payable amount was".$subtotal."and".$discountamount." amount is given as discount in month ". $mvalue;  
                        $discountgiventhismonth = $discountamount; $discountamount = 0; }  
                         }
                    }

                    if( $latefeetoken == 0) { if( $latefee >= 0) { $subtotal += $latefee; $latefeetoken = 1; 
                                                $payablethismonth += $latefee; $latefeetoken = 1; } }
                array_push($pm, $mvalue);
                $notif_transport = 'No'; if($subtotal_transport > 0 ) { $notif_transport = 'Yes'; }
                $sub = new feereciept_sc;   // fee reciept of school of each month
                $sub->admission_no          = $admission_no;
                $sub->student_name          = $student_name;
                $sub->class_section         = $class_section;
                $sub->father_name           = $parent_name;
                $sub->fee_months            = $mvalue;
                $sub->month_admno_code      = $admission_no."".$mvalue;
                $sub->requireonce_included  = $class_section;
                $sub->requireonce_amount    = 'N.A.';
                $sub->discount_included     = 'No';
                $sub->discount_given        = $discountgiventhismonth;
                $sub->discount_total_offered = isset($field['discountamount'])? $field['discountamount'] :0;
                $sub->discountprovider     = $discountprovider;
                $sub->discountreason       = $discountreason;
                $sub->transportation_included = $notif_transport;
                $sub->transportation_amount = $subtotal_transport;
                $sub->total_payable         = $subtotal;
                $sub->total_paid            = $payablethismonth;
                $sub->payment_mode          = 'dd';
                $sub->feetype_array         = implode(" , ",$ftl);
                $sub->feeamountarray        = implode(" , ",$amt);
                $sub->associated_reciept_array = implode(" , ",$reciept);
                $sub->extradetails          = $extradetails ;
                $sub->dd_no                = $dd_no;
                $sub->dd_date              = $dd_date;
                $sub->dd_bank              = $dd_bank;
                $sub->dd_branch            = $dd_branch;
                $sub->dd_amount            = $dd_amount;
                $sub->dd_verify            = 'Pending';
                $sub->sessionid             = $sessionid;
                $sub->Save();


                array_push($reciept, $sub->id);
                $datetime = $sub->updated_at;


                // now re-iniatializing temporary variable Inportant 
                $payablethismonth = 0;
                $discountgiventhismonth = 0;
                $transportationfeecopy =0;

               
            }
            // adding value month by month in school payment internal table over here,
          
       




        // loop ends here
           

            $y = isset($field['discountamount'])? $field['discountamount'] :0;
            $latefee = isset($field['latefee'])? $field['latefee'] :0;
            $xx = $field['total'] -  $y;           // this is original payment table
                $originalrecipt = new feereciept_student;  
                $originalrecipt->admission_no          = $admission_no;
                $originalrecipt->student_name          = $student_name;
                $originalrecipt->class_section         = $class_section;
                $originalrecipt->father_name           = $parent_name;
                $originalrecipt->payment_mode           = 'dd ('.$dd_no .')';
                $originalrecipt->fee_months            = implode(" , ",$pm);
                $originalrecipt->feetypearray          = implode(" , ",$ftl);
                $originalrecipt->feeamountarray        = implode(" , ",$amt);
                $originalrecipt->discount_provider     = $discountprovider ;
                $originalrecipt->discount_amount       = isset($field['discountamount'])? $field['discountamount'] :0;
                $originalrecipt->total_payable         = $field['total'];
                $originalrecipt->total_paid            = $xx;
                $originalrecipt->extradetails          = implode(" , ",$reciept);
                $originalrecipt->sessionid             = $sessionid;
                $originalrecipt->Save();


               return redirect('/Reprint-fee-Reciept/'.$originalrecipt->id);
    }
}

/*

*/
<?php

namespace DELLMOND\Http\Controllers;
use DELLMOND\Http\Requests;
use DELLMOND\User;
use Illuminate\Http\Request;
use Input;
use \DELLMOND\Sessionmanager;
use DELLMOND\feeamount;
use DELLMOND\feetype;
use DELLMOND\student;
use DELLMOND\discountfee;
use DELLMOND\feereciept_student;
use DELLMOND\feereciept_sc;
use DB;
use Excel;

class SessionManagerController extends Controller
{
	###########################  Create Control ##############################
    public function createIndex()
    {
    	return view('admin.sessionmanager.sessionmanager_create');
    }
	public function createSubmit()
	{
		
		$field = Input::all();
        $cr_session       = isset($field['cr_session'])         ?   $field['cr_session']          : '';
        $comment          = isset($field['comment'])            ?   $field['comment']             : '';

        
        //$object->Save();

       $tokens = Sessionmanager::where('session_period', $cr_session)->count('session_period');
 
        if( $tokens != 0) 
            { 
                return view('thanks_already'); 
            }
        else
            {
				$object = new Sessionmanager;
				$object->session_period         = $cr_session;
				$object->status          = '-';
				$object->description     = $comment;
            	$object->Save();
           		return view('thanks');
	        }
	}





	################################### READ CONTROL ##################################
	public function readIndex()
	{
		$data = Sessionmanager::all();
    	return view('admin.sessionmanager.sessionmanager_read')->with(['data' => $data, ]);
	}


	public function updateIndex()
	{

	}
	public function updateSubmit()
	{

	}




	################################# DELETE AND ACTIVATE CONTROL #############################
	public function delete($id)
	{
		Sessionmanager::destroy($id);
		return view('thanks_deleted');
	}
	public function activate($id)
	{
		Sessionmanager::where('status', 'active')
          ->update(['status' => '-']);

        Sessionmanager::where('id', $id)
          ->update(['status' => 'active']);

        return view('thanks');
	}
}

<?php

namespace DELLMOND\Http\Controllers;
use DELLMOND\Http\Requests;
use DELLMOND\User;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Input;
use DELLMOND\classmgmt;
use DELLMOND\feeamount;
use DELLMOND\feetype;
use DELLMOND\student;
use DELLMOND\discountfee;
use DELLMOND\discountprovider;
use DB;
use Excel;

class feecontroller extends Controller
{
	public function getIndex()
    {       
        return view('datatables.searchdatatable');
    }




    
	public function searchstudentreq()
    {
        $txtlabels = array('Search by Admission No', 'Search by Name');
        $txtnm = array('admission_no','stu_name');
        $txttype = array('text','text');
        
        return view('fee.searchstudenttopayfee')->with([
            'form_heading' =>'&#128269; Serch Student to submit fee',
            'submit_to' => 'f.searchstudent',
            'txtlabels'=>$txtlabels,
            'txtnm'=> $txtnm,
            'txttype'=>$txttype,]);
    }





    public function searchstudentreqsubmit()
    {
        $sessionid                  =       Session('valid_id');

        $field = Input::all();
        $sub = new Student;
        
        if($field['admission_no'])
        { $adm_no = $field['admission_no']; 
         $data=\DB::table('students')->orderBy('class_section', 'asc')->select('student_name', 'class_section', 'father_name', 'dob', 'admission_no')->where('sessionid', $sessionid)->where('admission_no','=',$adm_no)->get();
         return view('table.showpayfeetable')->with(['data'=>$data, 'form_heading' => 'select student' ]);
        }

        if($field['stu_name'])
        { 
        $stu_name = $field['stu_name']; 
         $data=\DB::table('students')->orderBy('class_section', 'asc')->select('student_name', 'class_section', 'father_name', 'dob', 'admission_no')->where('sessionid', $sessionid)->where('student_name','like','%'.$stu_name.'%')->get();
         return view('table.showpayfeetable')->with(['data'=>$data, 'form_heading' => 'select student' ]);
        }
        
    }

    public function deletefeetyperequest()
    {
        $sessionid                  =       Session('valid_id');

        $data=\DB::table('feetypes')->select('*')->where('sessionid', $sessionid)->orderBy('id', 'desc')->get();
        return view('coremgmt.deletefeetypetablegenerate')->with(['data'=>$data,'form_heading' => 'Delete the Fee Type']);
    }
    public function deletefeetyperequestsubmit($id)
    {
        $sessionid                  =       Session('valid_id');
        DB::table('feetypes')->where('sessionid', $sessionid)->where('id','=',$id)->delete();
        return view('thanks');
    }
    public function deletepaidfeereq()
    {
        $data = array('jan', 'feb', 'march', 'april', 'may', 'june', 'july', 'aug', 'sep', 'oct', 'nov', 'dec');
        //paidfeeselectmonth

        return view('fee.paidfeeselectmonth')->with(['data'=>$data,'form_heading' => 'Select the Month to proceed',]);
    }
    public function deletepaidfeelistgenerate()
    {
        $sessionid                  =       Session('valid_id');

        $field = Input::all();
        $mnth    = $field['month'];
        $tablename = $mnth."fees";

        $data=\DB::table($tablename)->select('rept_no', 'admission_no', 'student_name', 'class_section', 'parent_name','total_fee','extradetails')->where('sessionid', $sessionid)->orderBy('rept_no', 'desc')->get();
        return view('fee.paidfeedeletetablegenerate')->with(['data'=>$data,'form_heading' => 'Select the Month to proceed','month'=>$mnth,]);
    }
    public function deletepaidreciept($month, $x)
    {
      /*  $tablename = $month."fees";
        DB::table($tablename)->where('rept_no','=',$x)->delete();
        return view('thanks');
        */
        return 'wait while we are working, as of now you can not delete';
    }
    public function addfeetype()
    {
        return view('coremgmt.addfeetypeform');
    }
    public function addfeetypesubmit()
    {
        $sessionid                  =       Session('valid_id');

        $field = Input::all();
        $fee_type       = $field['fee_type'];
        $description    = isset($field['description'])?$field['description']:'N.A.';
        $reqonce        = isset($field['reqonce'])? '1' :'0';
        $jan            = isset($field['jan'])? '1' :'0';
        $feb            = isset($field['feb'])? '1' :'0';
        $march          = isset($field['march'])? '1' :'0';
        $april          = isset($field['april'])? '1' :'0';
        $may            = isset($field['may'])? '1' :'0';
        $june           = isset($field['june'])? '1' :'0';
        $july           = isset($field['july'])? '1' :'0';
        $aug            = isset($field['aug'])? '1' :'0';
        $sep            = isset($field['sep'])? '1' :'0';
        $oct            = isset($field['oct'])? '1' :'0';
        $nov            = isset($field['nov'])? '1' :'0';
        $dec            = isset($field['dec'])? '1' :'0';

        $tokens = \DB::table('feetypes')->where('fee_type', '=', $fee_type)->count('*');

        if( $tokens != 0) 
            { 
                return 'amount already exists !'; 
            }
        else
            {
           

            if( $reqonce == 0)
            {
                $disc = new Feetype;
                $disc->fee_type = $fee_type;
                $disc->description = $description;
                $disc->once_required = $reqonce;
                $disc->jan = $jan;
                $disc->feb = $feb;
                $disc->march = $march;
                $disc->april = $april;
                $disc->april = $april;
                $disc->may = $may;
                $disc->june = $june;
                $disc->july = $july;
                $disc->aug = $aug;
                $disc->sep = $sep;
                $disc->oct = $oct;
                $disc->nov = $nov;
                $disc->dec = $dec;
                $dics->sessionid = $sessionid;
                $disc->Save();
                return view('thanks');
            }
            else
            {
                $disc = new Feetype;
                $disc->fee_type = $fee_type;
                $disc->description = $description;
                $disc->once_required = $reqonce;
                $disc->jan = '0';
                $disc->feb = '0';
                $disc->march = '0';
                $disc->april = '0';
                $disc->april = '0';
                $disc->may = '0';
                $disc->june = '0';
                $disc->july = '0';
                $disc->aug = '0';
                $disc->sep = '0';
                $disc->oct = '0';
                $disc->nov = '0';
                $disc->dec = '0';
                $disc->sessionid = $sessionid;
                $disc->Save();
                return view('thanks');
            }
        }
       
    }

    

    public function updatefeerequest()
    {
        $sessionid                  =       Session('valid_id');
        $data=\DB::table('feeamounts')->select('*')->where('sessionid', $sessionid)->orderBy('class_section', 'desc')->get();
       return view('fee.updatefeevalue_form')->with(['data'=>$data,'form_heading' => 'Update fee value',]);
    }
    public function updatefeerequestsubmit($id)
    {
        $sessionid                  =       Session('valid_id');

        $field = Input::all();
        $amount       = $field['amount'];
        
        \DB::table('feeamounts')->where('id', $id)->where('sessionid', $sessionid)->update(['amount' => $amount]);
        return view('thanks');
    }


    public function removefeerequest()
    {
        $sessionid                  =       Session('valid_id');

        $data=\DB::table('feeamounts')->select('*')->where('sessionid', $sessionid)->orderBy('class_section', 'desc')->get();
       return view('fee.removefeevalue_form')->with(['data'=>$data,'form_heading' => 'Update fee value',]);
    }
    public function removefeerequestsubmit($id)
    {
       $sessionid                  =       Session('valid_id');

        \DB::table('feeamounts')->where('sessionid', $sessionid)->where('id', $id)->delete();
        return view('thanks');
    }





    // closing class bracket here
}

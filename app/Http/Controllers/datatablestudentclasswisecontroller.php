<?php

namespace DELLMOND\Http\Controllers;
use DELLMOND\Http\Requests;
use DELLMOND\User;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Input;
use DELLMOND\classmgmt;
use DELLMOND\feeamount;
use DELLMOND\feetype;
use DELLMOND\student;
use DB;
use Excel;

class datatablestudentclasswisecontroller extends Controller
{
   public function classwiselistreq()
    { 
         $sessionid                  =       Session('valid_id');
       
        $data=\DB::table('classmgmts')->orderBy('class_section', 'asc')->select('class_section')->where('sessionid', $sessionid)->get();
       // $products = Classmgmt::pluck('class_section', 'class_section');
        $optionslabels = array('Select Class');
        $optionsnm = array('classname');
    return view('datatables.searchstudent_classwise_form', compact('products'))->with([
            'form_heading' =>'Add New Student',
            'submit_to' => 'f.classwiselist',
            'optionslabels'=>$optionslabels,
            'optionsnm'=> $optionsnm,
            'optionsvalue'=>$data,]);
    }

    public function submitform()
    {
         $sessionid                  =       Session('valid_id');
        $field = Input::all();
        $sub = new Student;
        $search = $field['classname'];
        return redirect('/classwise-student-list/'.$search);
    }
    public function Index($id)
    {
         $sessionid                  =       Session('valid_id');
        
        $data=\DB::table('students')->select('admission_no', 'student_name', 'class_section', 'father_name', 'mobile', 'dob')->where('class_section','=',$id)->orderBy('student_name', 'asc')->where('sessionid', $sessionid)->get();
        
        return view('datatables.classwisestudentlist')->with(['data'=>$data, 'form_heading' => 'Detail About Student with given Admission Number' ]);
    }
    public function deletestudent($id) 
    { 
         $sessionid                  =       Session('valid_id');
    \DB::table('students')->where('admission_no', $id)->where('sessionid', $sessionid)->delete();
        return view('thanks');
    }
    public function ajaxresponse($id) 
    { 
         $sessionid                  =       Session('valid_id');
   // return Datatables::of(Student::query(DB::table('students')->where('class_section','=','2')->select('*')->get()))->make(true);
    return Datatables::of(Student::query("SELECT * from [Select * from student where admission_no<25]"))->make(true);
    //return Datatables::of(Student::query('select admission_no,student_name,class_section from students'))->make(true);
    }
	public function exportexcel_xlsx()
    {
         $sessionid                  =       Session('valid_id');
        Excel::create('Filename', function($excel) {
        $excel->setTitle('Students');
        $excel->setCreator('Adesh')->setCompany('DELLMOND');
        $excel->setDescription('A complete student list, automatically generated');
        $excel->sheet('Sheetname', function($sheet) {
            $prices = \DELLMOND\feetype::all();
            $sheet->fromModel($prices);
        });
    })->download('xlsx');
    }
    public function exportexcel_csv()
    {
         $sessionid                  =       Session('valid_id');
        Excel::create('Filename', function($excel) {
        $excel->setTitle('Students');
        $excel->setCreator('Adesh')->setCompany('DELLMOND');
        $excel->setDescription('A complete student list, automatically generated');
        $excel->sheet('Sheetname', function($sheet) {
            $prices = \DELLMOND\feetype::all();
            $sheet->fromModel($prices);
        });
    })->download('csv');
    }
}


<?php

namespace DELLMOND\Http\Controllers;
use DELLMOND\Http\Requests;
use DELLMOND\User;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Input;
use DELLMOND\classmgmt;
use DELLMOND\feeamount;
use DELLMOND\feetype;
use DELLMOND\student;
use DB;
use Excel;

class studentcontroller extends Controller
{
    public function Index()
    {   
        return view('datatables.studenttable');
    }

    public function CompleteStudentList()
    {   
        $sessionid                  =       Session('valid_id');

        $student =  \DB::table('students')->where('sessionid', $sessionid)->select('admission_no', 'student_name', 'class_section', 'father_name', 'gender', 'mobile', 'dob')->get();
        return view('student.completestudentlist')->with(['data' => $student]);
    }


    public function anyData()
    {
        $field = Input::all();
        if (Input::has('classname'))
            {
                //
            }
        $sub = new Student;
        $sub->student_name = $field['classname'];
        dd($sub);
    }
    public function getPosts() 
    {    
     return Datatables::of(Student::query('select * from students'))->make(true);
           
    }

    public function exportUserList(UserListExport $export)
    {
        // work on the export
        return $export->sheet('sheetName', function($sheet)
        {

        })->export('xls');
    }
    public function insert()
    {
        $sessionid                  =       Session('valid_id');

        $field = Input::all();
        $sub = new Student;
        $sub->admission_no          = $field['admission_no'];
        $sub->session               = '1';
        $sub->student_name          = $field['stdname'];
        $sub->class_section         = $field['clsec'];
        $sub->father_name           = $field['father_name'];
        $sub->mother_name           = $field['mother_name'];
        $sub->gender                = $field['gender'];
        $sub->address               = $field['address'];
        $sub->mobile                = $field['mobile'];
        $sub->transport_route       = $field['transport_route'];
        $sub->dob                   = $field['dob'];
        $sub->status                = 'active';
        $sub->priority              = 'N.A';
        $sub->ref                   = 'N.A';
        $sub->remark                = $field['remark'];
        $sub->sessionid             = $sessionid;
        $sub->Save();
        return view('thanks');
    }
    public function addstudentrequest()
    {
        $sessionid                  =       Session('valid_id');

        $data=\DB::table('classmgmts')->where('sessionid', $sessionid)->orderBy('class_section', 'asc')->select('class_section')->get();
        $optionslabels = array('Select Class');
        $optionsnm = array('classname');
      
        return view('student.newregistration')->with(['classname'=>$data,]);
            
    }
    public function addstudentrequestexcel()
    {
        return view('student.newregistration_excel');
            
    }
    public function insertexcel()
    {
                $sessionid                  =       Session('valid_id');

        $field = Input::all();
        
        $student = $field['student'];
        $student = Input::file('student');
        $file_name = $student->getClientOriginalName();
        $student->move('/public/exceluploads', $file_name);
       
        $results = Excel::load('/public/exceluploads/'.$file_name, function($reader){
            //$reader->all();
            //$reader->limitRows(15)->limitColumns(6);
            $reader->limitColumns(10)->limitRows(1618);
        })->get();

        $counter_sucess = 0;
        $counter_failure = 0;
        $failure_array = array() ; 
        $r= array();
        foreach ($results as $key => $value) {
           foreach ($value as $k => $vv) {
           array_push($r, $vv);
        }
        }

       //print_r($r);

        foreach ($r as $key => $value) 
            {   $admission_no = 0; $name = "N.A"; $parent = "N.A"; $class = ""; $dob = "N.A"; $gender="";
            foreach ($value as $k => $v) { 
           if(($k == 'admission')  && ($v !='0' ) )     {  $admission_no    = $v;           }
           if(($k == 'student')  && ($v !='0' ) )       {  $name            = $v;           }
            if(($k == 'father')  && ($v !='0' ) )      {  $father_name          =$v;            }
            if(($k == 'mother') && ($v !='0' ) )        {  $mother_name             =  $v ;         }
            if(($k == 'class')  && ($v !='0' ) )        {  $class           = $v;           }
            if(($k == 'section') && ($v !='0' ) )       {  $class          .= "-".$v ;      }
            if(($k == 'gn') && ($v !='0' ) )        {   $gender             =  '' ;         }
            if(($k == 'address') && ($v !='0' ) )       {  $address             =  $v ;         }
            if(($k == 'mobile') && ($v !='0' ) )        {  $mobile             =  $v ;         }
            if(($k == 'dob') && ($v !='0' ) )           {  $dob             =  $v ;         }
            } 
            
            $name            = isset($name        )?   $name        :     'N.A'      ;
            $admission_no    = isset($admission_no)? $admission_no  :      '0'       ;
            $class           = isset($class       )?   $class       :     'N.A.'     ;
            $father_name     = isset($father_name      )?   $father_name      :     'N.A.'     ;
            $mother_name     = isset($mother_name         )?   $mother_name         :      'N.A.'    ;
            $gender          = isset($gender         )?   $gender         :      'N.A.'    ;
            $address         = isset($address         )?   $address         :      'N.A.'    ;
            $mobile          = isset($mobile         )?   $mobile         :      'N.A.'    ;
            $dob            = isset($dob         )?   $dob        :      '01-jan-1990'    ;

        
            $tokens = \DB::table('students')->where('admission_no', '=', $admission_no)->count('*');
            echo $tokens;
                if($admission_no != '0')
                {           echo "<br>";
                if( $tokens != 0) 
                    { 
                        echo $admission_no. 'student already exists !'; $counter_failure += 1;
                    }
                else
                    {
                    $sub = new Student;
                    $sub->admission_no   =  $admission_no   ;
                    $sub->session        =  '2017-2018'            ;
                    $sub->student_name   =  $name           ;
                    $sub->class_section  =  $class          ;
                    $sub->father_name    =  $father_name         ;
                    $sub->mother_name    =  $mother_name            ;
                    $sub->gender         =  $gender            ;
                    $sub->address        =  $address            ;
                    $sub->mobile         =  $mobile            ;
                    $sub->transport_route=  '0'     ;
                    $sub->dob            =  $dob            ;
                    $sub->status         =  'active'            ;
                    $sub->priority       =  '1'            ;
                    $sub->ref            =  '0'            ;
                    $sub->remark         =  '0'           ;
                    $sub->sessionid      =  $sessionid;

                    if ( $sub->Save() ) { echo $admission_no .'Added to database.' ; $counter_sucess += 1; }
                    else {  echo 'Unknown Problem encountered with'.$admission_no .' please add manually.'; }
                    }
                }
            }
      //return view('thanks');
    }
    public function classwiselistreq()
    {
                $sessionid                  =       Session('valid_id');

        $products = Classmgmt::where('sessionid', $sessionid)->pluck('class_section', 'class_section');
        $optionslabels = array('Select Class');
        $optionsnm = array('classname');
    return view('classchoose', compact('products'))->with([
            'form_heading' =>'Add New Student',
            'submit_to' => 'f.classwiselist',
            'optionslabels'=>$optionslabels,
            'optionsnm'=> $optionsnm,
            'optionsvalue'=>$products,]);
    }

     public function classwiselist()
    {
        $field = Input::all();
        $sub = new Student;
        $search = $field['classname'];
        return Datatables::of(Student::query('select * from students'))->make(true);
    }
    public function searchstudentreq()
    {
        $txtlabels = array('Search by Admission No', 'Search by Name');
        $txtnm = array('admission_no','stu_name');
        $txttype = array('text','text');
        
        return view('student.searchstudent')->with([
            'form_heading' =>'Serch Student to submit fee',
            'submit_to' => 'f.studetails',
            'txtlabels'=>$txtlabels,
            'txtnm'=> $txtnm,
            'txttype'=>$txttype,]);
    }
    public function searchstudentreqsubmit()
    {
                $sessionid                  =       Session('valid_id');

        $field = Input::all();
        $sub = new Student;
        
       if($field['admission_no'])
        { $adm_no = $field['admission_no'];
         $data=\DB::table('students')->orderBy('class_section', 'asc')->where('sessionid', $sessionid)->select('id','admission_no', 'student_name', 'class_section', 'father_name', 'mother_name','gender', 'address', 'mobile', 'transport_route', 'dob', 'remark', 'updated_at')->where('admission_no','=',$adm_no)->get();
         $data_name = array('Unique ID', 'Admission No', 'Student Name', 'Class-Section', 'Father Name', 'Mother Name', 'Gender', 'Address', 'Mobile No', 'Transport Route ID', 'Date of Birth', 'Any Remark', 'Last Updated At');
         return view('student.showstudentdetails')->with(['data'=>$data, 'data_name' => $data_name, 'form_heading' => 'Detail About Student with given Admission Number' ]);
        }

        if($field['stu_name'])
        { $stu_name = $field['stu_name']; 
         $data=\DB::table('students')->orderBy('class_section', 'asc')->where('sessionid', $sessionid)->select('id','admission_no', 'student_name', 'class_section', 'father_name', 'mother_name','gender', 'address', 'mobile', 'transport_route', 'dob', 'remark', 'updated_at')->where('student_name','=',$stu_name)->get();
         $data_name = array('Unique ID', 'Admission No', 'Student Name', 'Class-Section', 'Father Name', 'Mother Name', 'Gender', 'Address', 'Mobile No', 'Transport Route ID', 'Date of Birth', 'Any Remark', 'Last Updated At');
         return view('student.showstudentdetails')->with(['data'=>$data, 'data_name' => $data_name, 'form_heading' => 'Detail About Student with given Admission Number' ]);
        }
        
    }


    public function searchstudentreqsubmit_for_update()
    {
        $sessionid                  =       Session('valid_id');

        $field = Input::all();
        $sub = new Student;
        
        $classopt=\DB::table('classmgmts')->orderBy('class_section', 'asc')->where('sessionid', $sessionid)->select('class_section')->get();
        $routess=\DB::table('transportroutes')->orderBy('id', 'asc')->where('sessionid', $sessionid)->select('id')->get();
        if($field['admission_no'])
        { $adm_no = $field['admission_no']; 
         $data=\DB::table('students')->where('sessionid', $sessionid)->select('id', 'admission_no', 'student_name', 'class_section', 'father_name', 'mother_name', 'gender', 'address', 'mobile', 'transport_route', 'dob', 'remark')->where('admission_no','=',$adm_no)->get();
         return view('student.updatestudent_information_selection_display')->with(['data'=>$data, 'classopt' => $classopt, 'routess' => $routess, 'form_heading' => 'Detail About Student with given Admission Number' ]);
        }

        if($field['stu_name'])
        { $stu_name = $field['stu_name']; 
         $data=\DB::table('students')->where('sessionid', $sessionid)->select('id', 'admission_no', 'student_name', 'class_section', 'father_name', 'mother_name', 'gender', 'address', 'mobile', 'transport_route', 'dob', 'remark')->where('student_name','=',$stu_name)->get();
         return view('student.updatestudent_information_selection_display')->with(['data'=>$data, 'classopt' => $classopt, 'routess' => $routess, 'form_heading' => 'Detail About Student with given Admission Number' ]);
        }
        
    }

    public function updatestudentdetails_req( $limit )
    { 
                $sessionid                  =       Session('valid_id');

        $routess=\DB::table('transportroutes')->where('sessionid', $sessionid)->orderBy('id', 'asc')->select('route_name')->get();
        $studentdetails=\DB::table('students')->where('sessionid', $sessionid)->skip($limit)->limit(100)->select('admission_no','student_name','class_section','father_name', 'dob', 'id')->orderBy('id', 'asc')->get();
        $prevlimit = 0; $nextlimit = $limit + 100;
        if($prevlimit > 100) { $prevlimit = $limit - 100; }
       return view('student.updatestudent')->with(['data'=>$studentdetails,'classopt'=>$classopt,'prevlimit'=>$classopt,'nextlimit'=>$nextlimit,'routess' => $routess ,'form_heading' => 'Discount Beneficiary Provider',]);
        
    }
    public function updatestudentdetails_req_submit()
    {
        $sessionid                  =       Session('valid_id');

    //  'id', 'admission_no', 'student_name', 'class_section', 'father_name', 'mother_name', 'gender', 'address', 'mobile', 'transport_route', 'dob', 'remark'
        $field = Input::all();
        $id              = $field['id'];
        $admission_no            = isset($field['admission_no'])   ?$field['admission_no']                 : 0 ;
        $student_name            = isset($field['student_name'])   ?$field['student_name']                 : 0 ;
        $class_section            = isset($field['class_section'])   ?$field['class_section']                 : 0 ;
        $father_name            = isset($field['father_name'])   ?$field['father_name']                 : 0 ;
        $mother_name            = isset($field['mother_name'])   ?$field['mother_name']                 : 0 ;
        $gender            = isset($field['gender'])   ?$field['gender']                 : 0 ;
        $address            = isset($field['address'])   ?$field['address']                 : 0 ;
        $mobile            = isset($field['mobile'])   ?$field['mobile']                 : 0 ;
        $transport_route            = isset($field['transport_route'])   ?$field['transport_route']                 : 0 ;
        $dob            = isset($field['dob'])   ?$field['dob']                 : 0 ;
        $remark            = isset($field['remark'])   ?$field['remark']                 : 0 ;

        
        if($admission_no == 0){ return 'please specify correct admission number! Empty & Zero can not be stored'; }
        //return $name;
        //if($clsec == 0){ return 'please specify correct Class-Section in new Class-Section column given as select box! Empty & Zero can not be stored'; }
        //if($name == 0){ return 'please specify correct Name of student! Empty & Zero can not be stored'; }

         $tokens = \DB::table('students')->where('sessionid', $sessionid)->where('admission_no', '=', $admission_no)->where('id', '!=', $id)->count('*');

        if( $tokens != 0) 
            { 
                
                return 'Same Admission no exist at some other place, please delete that one OR choose some other admission number.'; 
            }
        else
            { //return $tokens;
               // $update = [['stdname' => $name],['admission_no' => $admission_no],['clsec' => $clsec],['ptname' => $parent], ['dob' => $dob]];
            \DB::table('students')->where('id', '=', $id)->update(['student_name' => $student_name ]);
            \DB::table('students')->where('id', '=', $id)->update(['admission_no' => $admission_no]);
            \DB::table('students')->where('id', '=', $id)->update(['class_section' => $class_section]);
            \DB::table('students')->where('id', '=', $id)->update(['father_name' => $father_name]);
            \DB::table('students')->where('id', '=', $id)->update([ 'mother_name' => $mother_name ]);
            \DB::table('students')->where('id', '=', $id)->update([ 'gender' => $gender ]);
            \DB::table('students')->where('id', '=', $id)->update([ 'address' => $address ]);
            \DB::table('students')->where('id', '=', $id)->update([ 'mobile' => $mobile ]);
            \DB::table('students')->where('id', '=', $id)->update([ 'transport_route' => $transport_route ]);
            \DB::table('students')->where('id', '=', $id)->update([ 'dob' => $dob ]);
            \DB::table('students')->where('id', '=', $id)->update([ 'remark' => $remark ]);
                return view('thanks');
            }
    }
}

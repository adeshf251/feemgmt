<?php

namespace DELLMOND\Http\Controllers\Auth;

use DELLMOND\User;
use DELLMOND\Admin;
use DELLMOND\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class AdminLogin extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('guest', ['except' => 'logout']);
    // }

    public function getAdminLogin()
    {

        //if (auth()->guard('admin')->user()) return redirect()->route('admin.dashboard');
        return view('adminlogin');
    }

    public function adminAuth(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);


        if (auth()->guard('admin')->attempt(['email' => $request->input('email'), 'password' => $request->input('password')]))
        { dd();
            return redirect()->route('admin.dashboard');
        }else{
            dd('your username and password are wrong.');
        }
    }

    public function aa() {
        //
    }
    public function dashboard()
    {
        //return view('admin.dashboard');
        //admin-controller
    }
}
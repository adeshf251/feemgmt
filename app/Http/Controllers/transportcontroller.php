<?php

namespace DELLMOND\Http\Controllers;
use DELLMOND\Http\Requests;
use DELLMOND\User;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Input;
use DELLMOND\classmgmt;
use DELLMOND\feeamount;
use DELLMOND\feetype;
use DELLMOND\student;
use DELLMOND\transportroute;
use DB;
use Excel;

class transportcontroller extends Controller
{
    public function insert_route_request()
    {
    	return view('transport.setroutes');
    }
    public function insert_route_request_submit()
    {
                $sessionid                  =       Session('valid_id');

    	$field = Input::all();
        $destination         = isset($field['destination']    )?   $field['destination']          :'N.A.';
        $amount              = isset($field['amount']         )?   $field['amount']               :  'N.A'  ;
       
         $tokens = \DB::table('transportroutes')->where('sessionid', $sessionid)->where('destination', '=', $destination)->count('*');
        // echo $tokens;
        if( $tokens != 0) 
            { 
            	\DB::table('transportroutes')->where('sessionid', $sessionid)->where('destination', '=', $destination)->update(['amount' => $amount]);
                return 'amount for this destination already exists ! It has been modified Now.'; 
            }
        else
            {
            $sub = new transportroute;
	        $sub->route_name      = isset($field['route_name']   )?   $field['route_name']    :  0  ;
	        $sub->source          = isset($field['source']       )?   $field['source']        :  'N.A'  ;
	        $sub->destination     = isset($field['destination']  )?   $field['destination']   :  'N.A'  ;
	        $sub->amount          = isset($field['amount']       )?   $field['amount']        :  'N.A'  ;
            $sub->remark          = isset($field['remark']       )?   $field['remark']        :  'N.A'  ;
            $sub->sessionid       = $sessionid;
            if ( $sub->Save() ) {      }
            else { return 'Problem encountered, data might exits, please check it.'; }
            
            }
            return view('thanks');
    }
    public function update_route_request()
    {
        $sessionid                  =       Session('valid_id');

    	$data = $tokens = \DB::table('transportroutes')->where('sessionid', $sessionid)->select('route_name', 'source', 'destination', 'amount', 'remark','id')->get();
    	//print_r($data);
    	return view('transport.transport_route_list')->with(['data' => $data]);
    }
    public function update_route_submit()
    {
    	//return view('thanks');
    }
    public function delete($id)
    {
        $sessionid                  =       Session('valid_id');

         DB::table('transportroutes')->where('sessionid', $sessionid)->where('id','=',$id)->delete();
        return view('thanks');
    }

    public function transportuserlist()
    {
        $sessionid                  =       Session('valid_id');

        //$student =  student::where('transport_route', '!=' , 0);
        $student =  \DB::table('students')->where('sessionid', $sessionid)->select('admission_no', 'student_name', 'class_section', 'father_name', 'gender', 'dob', 'transport_route')->where('transport_route','>','0')->get();
        //print_r($student);
        return view('transport.transport_user_list')->with(['data' => $student]);
    }
}



<?php

namespace DELLMOND\Http\Controllers;

use Illuminate\Http\Request;
use DELLMOND\CustomReciept;

class CustomRecieptController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sessionid                  =       Session('valid_id');
        $reptlists =  CustomReciept::where('sessionid', $sessionid)->orderby('id', 'desc')->get();
        return view('customreciept.index')->with(['data' => $reptlists]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sessionid                  =       Session('valid_id');

        $data=\DB::table('classmgmts')->where('sessionid', $sessionid)->orderBy('class_section', 'asc')->select('class_section')->get();

        return view('customreciept.create')->with(['classname'=>$data,]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sessionid                  =       Session('valid_id');

        $sub = new CustomReciept;
        $sub->admission_no          = $request->input('admission_no');
        $sub->class_section          = $request->input('clsec');
        $sub->student_name          = $request->input('stdname');
        $sub->father_name          = $request->input('father_name');
        $sub->mother_name          = $request->input('mother_name');
        $sub->address          = $request->input('address');
        $sub->gender          = $request->input('gender');
        $sub->pay          = $request->input('pay');
        $sub->fee_for          = $request->input('fee_for');
        $sub->mobile          = $request->input('mobile');
        $sub->email          = $request->input('email');
        $sub->dob          = $request->input('dob');

        $sub->fee_name1          = $request->input('fee_name1');
        $sub->fee_value1          = $request->input('fee_value1');
        $sub->fee_name2          = $request->input('fee_name2');
        $sub->fee_value2          = $request->input('fee_value2');
        $sub->fee_name3          = $request->input('fee_name3');
        $sub->fee_value3          = $request->input('fee_value3');
        $sub->fee_name4          = $request->input('fee_name4');
        $sub->fee_value4          = $request->input('fee_value4');
        $sub->fee_name5          = $request->input('fee_name5');
        $sub->fee_value5          = $request->input('fee_value5');

        $sub->remark          = $request->input('remark');
        $sub->parent_sessionid          = '0';
        $sub->sessionid          = $sessionid;
        $sub->save();

        return redirect('custom-reciept');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sessionid                  =       Session('valid_id');
        $reptlists =  CustomReciept::find($id);
        return view('customreciept.recieptpreview')->with(['data' => $reptlists]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

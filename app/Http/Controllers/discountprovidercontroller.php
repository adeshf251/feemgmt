<?php

namespace DELLMOND\Http\Controllers;
use DELLMOND\Http\Requests;
use DELLMOND\User;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Input;
use DELLMOND\classmgmt;
use DELLMOND\feeamount;
use DELLMOND\feetype;
use DELLMOND\student;
use DELLMOND\discountprovider;
use DB;
use Excel;


class discountprovidercontroller extends Controller
{
    public function addreq_form()
    {
    	return view ('discount.discountproviderregistration');
    }
    public function addreq_form_submit()
    {
        $sessionid                  =       Session('valid_id');

    	$field = Input::all();
        $sub = new discountprovider;
        $sub->discountprovider_name 		= isset($field['name'])       ?$field['name']        :'N.A.';
        $sub->discountprovider_position 	= isset($field['position'])   ?$field['position']    :'N.A.';
        $sub->discountprovider_department 	= isset($field['department']) ?$field['department']  :'N.A.';
        $sub->discountprovider_email		= isset($field['email'])      ?$field['email']       :'N.A.';
        $sub->discountprovider_gender 		= isset($field['gender'])     ?$field['gender']      :'N.A.';
        $sub->discountprovider_mb 			= isset($field['mobile'])     ?$field['mobile']      :'N.A.';
        $sub->extra1 						= isset($field['extra1'])     ?$field['extra1']      :'N.A.';
        $sub->sessionid = $sessionid;
        $sub->Save();
        return view('thanks');
    }
    public function discountbeneficiaryprovider()
    {
        $sessionid                  =       Session('valid_id');

        $data=\DB::table('discountproviders')->select('discountprovider_name', 'discountprovider_position', 'discountprovider_department', 'discountprovider_email', 'discountprovider_gender','discountprovider_mb','extra1','updated_at', 'id')->orderBy('id', 'desc')->where('sessionid', $sessionid)->get();
       return view('discount.discountbeneficiaryprovider')->with(['data'=>$data,'form_heading' => 'Discount Beneficiary Provider',]);
    }
    public function discountbeneficiarystudent()
    {
        $sessionid                  =       Session('valid_id');

        $data=\DB::table('feereciept_scs')->select('admission_no','student_name','class_section','father_name','fee_months', 'discount_given', 'discountprovider','discountreason')->orderBy('admission_no', 'desc')->where('sessionid', $sessionid)->where('discount_given','!=','0')->get();
       return view('discount.discountbeneficiarystudent')->with(['data'=>$data,'form_heading' => 'Discount Beneficiary Provider',]);
    }
    public function deleteprovider($id)
    {
        $sessionid                  =       Session('valid_id');

         \DB::table('discountproviders')->where('sessionid', $sessionid)->where('id', $id)->delete();
        return view('thanks');
    }
    public function requestprovideramountdetails()
    {
        $sessionid                  =       Session('valid_id');

        $data=\DB::table('discountproviders')->select('discountprovider_name')->where('sessionid', $sessionid)->get();
        return view ('discount.discountproviderselect_togeneratereport')->with(['data'=>$data, 'form_heading' => 'select student' ]);
    }
    public function requestprovideramountdetailssubmit()
    {
        $sessionid                  =       Session('valid_id');

        //*********** Array of once required fee   *****************//
        $array_once_fee = array(); $array_once_fee_discount = array();
        $once_req_fee_array = DB::table('feetypes')->where('sessionid', $sessionid)->where('once_required', '=', '1')->pluck('fee_type');

        foreach ($once_req_fee_array as $key => $value) {
                array_push($array_once_fee_discount, '0'); 
              array_push($array_once_fee, $value); 
        }
        //------------------Array of once req fee -------------------------//

        //print_r($array_once_fee);

        $field = Input::all(); 
        $discountprovider    = $field['discountprovider'];
        $months = array('jan', 'feb', 'march', 'april', 'may', 'june', 'july', 'aug', 'sep', 'oct', 'nov', 'dec');
        $values_disc = array('0','0','0','0','0','0','0','0','0','0','0','0');

        $m = array_merge($months, $array_once_fee);
        $n = array_merge($values_disc, $array_once_fee_discount);

        foreach ($m as $key => $value) {
           $data=\DB::table('feereciept_scs')->select('discount_given')->where('sessionid', $sessionid)->where('discountprovider','=',$discountprovider)->where('fee_months','=',$value)->get();
           foreach ($data as $k => $v) {
              foreach ($v as $a => $b) {
                     if($b > 0)
                     {
                        $n[$key] += $b;
                     }
                 }
           }
        }

        $providerdetail=\DB::table('discountproviders')->select('discountprovider_name', 'discountprovider_position', 'discountprovider_department', 'discountprovider_email', 'discountprovider_gender', 'discountprovider_mb')->where('sessionid', $sessionid)->where('discountprovider_name','=',$discountprovider)->get();

        return view ('discount.discountreportmemberwise')->with(['month'=>$m,'amount'=>$n, 'providerdetail'=>$providerdetail, 'form_heading' => 'select student' ]);
        
    }
}

<?php

namespace DELLMOND\Http\Controllers;
use DELLMOND\Http\Requests;
use DELLMOND\User;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Input;
use DELLMOND\classmgmt;
use DELLMOND\feeamount;
use DELLMOND\feetype;
use DELLMOND\student;
use DELLMOND\discountfee;
use DELLMOND\feereciept_student;
use DELLMOND\feereciept_sc;
use DB;
use Excel;

class deleterecieptcontroller extends Controller
{
    public function deletepaidfeereq()
    {
        return redirect('/delete-paid-fee-list');
    }
    public function deletepaidfeelistgenerate()
    {
        $sessionid                  =       Session('valid_id');
        $field = Input::all();
      //  $mnth    = $field['month'];
       // $tablename = $mnth."fees";

        $data=\DB::table('feereciept_students')->select('rept_no', 'admission_no', 'student_name', 'class_section', 'father_name','total_paid','fee_months')->where('sessionid', $sessionid)->orderBy('rept_no', 'desc')->get();
        return view('fee.paidfeedeletetablegenerate')->with(['data'=>$data,'form_heading' => 'Select the Month to proceed',]);
        
        //return 'welcome';
    }
    public function deletepaidreciept($month, $x)
    {
        $sessionid                  =       Session('valid_id');
    	$getupdated_at = \DELLMOND\getprintreciept::getstudentinner_reciept_no( $x ); 
    	$rept ="";
    	foreach ($getupdated_at as $key => $value) {
    		foreach ($value as $k => $v) {
    		$rept=  $v;
    		}
    	}
    	
    			
                $rept       = explode(" , ",$rept);
            foreach ($rept as $key => $value) {
            	DB::table('feereciept_scs')->where('sessionid', $sessionid)->where('rept_no','=',$value)->delete();
            }

       // $tablename = $month."fees";
        DB::table('feereciept_students')->where('sessionid', $sessionid)->where('rept_no','=',$x)->delete();
        return view('thanks');
    }
}

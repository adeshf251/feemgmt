<?php


namespace DELLMOND\Http\Controllers;
use DELLMOND\Http\Requests;
use DELLMOND\User;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Input;
use DELLMOND\classmgmt;
use DELLMOND\feeamount;
use DELLMOND\feetype;
use DELLMOND\student;
use DELLMOND\discountfee;
use DELLMOND\feereciept_student;
use DELLMOND\feereciept_sc;
use DB;
use Excel;

class reprintrecieptcontroller extends Controller
{
    public function reprintrequest()
    {
		$sessionid                  =       Session('valid_id');

         $data=\DB::table('feereciept_students')->where('sessionid', $sessionid)->select('rept_no', 'admission_no', 'student_name', 'class_section', 'father_name', 'fee_months','total_paid', 'updated_at')->orderBy('rept_no', 'desc')->get();
         return view('fee.reprintrequest')->with(['data'=>$data, 'form_heading' => 'Select the reciept to reprint' ]);
    }

    public function reprint($id)
    {
    	$rept_ID = $id; 
    	$student_name = \DELLMOND\getprintreciept::getstudentname( $rept_ID ); 
    	$std_name ="";
    	foreach ($student_name as $key => $value) {
    		foreach ($value as $k => $v) {
    		$std_name =  $v;
    		}
    	}

    	$class_section = \DELLMOND\getprintreciept::getstudentclass( $rept_ID ); 
    	$std_class ="";
    	foreach ($class_section as $key => $value) {
    		foreach ($value as $k => $v) {
    		$std_class=  $v;
    		}
    	}

    	$getstudentparent = \DELLMOND\getprintreciept::getstudentparent( $rept_ID ); 
    	$parent ="";
    	foreach ($getstudentparent as $key => $value) {
    		foreach ($value as $k => $v) {
    		$parent=  $v;
    		}
    	}

    	$feemonths = \DELLMOND\getprintreciept::getstudentfeemonths( $rept_ID ); 
    	$fm ="";
    	foreach ($feemonths as $key => $value) {
    		foreach ($value as $k => $v) {
    		$fm=  $v;
    		}
    	}

    	$feetype = \DELLMOND\getprintreciept::getstudentfeetypearray( $rept_ID ); 
    	$ft ="";
    	foreach ($feetype as $key => $value) {
    		foreach ($value as $k => $v) {
    		$ft=  $v;
    		}
    	}

    	$feeamount = \DELLMOND\getprintreciept::getstudentfeeamountarray( $rept_ID ); 
    	$fa ="";
    	foreach ($feeamount as $key => $value) {
    		foreach ($value as $k => $v) {
    		$fa=  $v;
    		}
    	}

    	$discountprovider = \DELLMOND\getprintreciept::getstudentdiscount_provider( $rept_ID ); 
    	$dp ="";
    	foreach ($discountprovider as $key => $value) {
    		foreach ($value as $k => $v) {
    		$dp=  $v;
    		}
    	}

    	$discountamount = \DELLMOND\getprintreciept::getstudentdiscount_amount( $rept_ID ); 
    	$da ="";
    	foreach ($discountamount as $key => $value) {
    		foreach ($value as $k => $v) {
    		$da=  $v;
    		}
    	}

    	$totalpayable = \DELLMOND\getprintreciept::getstudenttotal_payable( $rept_ID ); 
    	$tpayable ="";
    	foreach ($totalpayable as $key => $value) {
    		foreach ($value as $k => $v) {
    		$tpayable=  $v;
    		}
    	}

    	$totalpaid = \DELLMOND\getprintreciept::getstudenttotal_paid( $rept_ID ); 
    	$tpaid ="";
    	foreach ($totalpaid as $key => $value) {
    		foreach ($value as $k => $v) {
    		$tpaid=  $v;
    		}
    	}

    	$admission_no = \DELLMOND\getprintreciept::getstudentextradetails( $rept_ID ); 
    	$admno ="";
    	foreach ($admission_no as $key => $value) {
    		foreach ($value as $k => $v) {
    		$admno=  $v;
    		}
    	}

    	$getrept_no = \DELLMOND\getprintreciept::getrept_no( $rept_ID ); 
        $rept ="";
        foreach ($getrept_no as $key => $value) {
            foreach ($value as $k => $v) {
            $rept=  $v;
            }
        }


        $getrept_no = \DELLMOND\getprintreciept::getpayment_mode( $rept_ID ); 
        $payment_mode ="";
        foreach ($getrept_no as $key => $value) {
            foreach ($value as $k => $v) {
            $payment_mode=  $v;
            }
        }

    	$getupdated_at = \DELLMOND\getprintreciept::getupdated_at( $rept_ID ); 
    	$updated ="";
    	foreach ($getupdated_at as $key => $value) {
    		foreach ($value as $k => $v) {
    		$updated=  $v;
    		}
    	}
    	
    			$fm            = explode(" , ",$fm);
                $ft          = explode(" , ",$ft);
                $fa       = explode(" , ",$fa);
                $rept = array($rept);



    	return view('fee.reptpreview')->with(['admission_no'=>$admno,
            'student_name' => $std_name,
            'class_section' => $std_class,
            'parent_name' => $parent,
            'feetypeslist' => $ft,
            'payingmonths' => $fm,
            'amount' => $fa,
            'discount' => $da,
            'total' => $tpaid,
            'datetime' => $updated,
            'reciept' => $rept,
            'payment_mode' => $payment_mode,
            ]);

    	/*return view('fee.reptpreview')->with(['admission_no'=>$admno,
            'student_name' => $std_name,
            'class_section' => $std_class,
            'parent_name' => $parent,
            'feetypeslist' => $ft,
            'payingmonths' => $fm,
            'amount' => $fa,
            'discount' => $da,
            'total' => $tpaid,
            'datetime' => $updated,
            'reciept' => $rept,
            ]);
            */
    }
}

<?php

namespace DELLMOND\Http\Controllers;
use DELLMOND\Http\Requests;
use DELLMOND\User;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Input;
use DELLMOND\classmgmt;
use DELLMOND\feeamount;
use DELLMOND\feetype;
use DELLMOND\student;
use DB;
use Excel;

class errorcontroller extends Controller
{
    public function rendering()
    {
    	return view('errorpage');
    }

    public function tester()
    {
		$sessionid                  =       Session('valid_id');

    	$data=\DB::table('sc_student')->where('sessionid', $sessionid)->select('admission_no', 'student_name', 'student_father_name', 'student_class', 'student_section', 'student_dob')->get();
    	foreach ($data as $key => $v) 
    	{
    		$admission_no = "";
    		$student_name= "";
    		$student_father_name= "";
    		$student_class= "";
    		$student_section= "";
    		$student_dob= "";
    		foreach ($v as $k => $ele) 
    		{
    			if($k == 'admission_no')
    			{
    				$admission_no = $ele;
    			}
    			if($k == 'student_name')
    			{
    				$student_name= $ele;
    			}
    			if($k == 'student_father_name')
    			{
    				$student_father_name= $ele;
    			}
    			if($k == 'student_class')
    			{
    				if($ele==24 ){$student_class= '1';}
    				if($ele==25 ){$student_class= '2';}
    				if($ele==26 ){$student_class= '3';}
    				if($ele==27 ){$student_class= '4';}
    				if($ele==28 ){$student_class= '5';}
    				if($ele==29 ){$student_class= '6';}
    				if($ele==30 ){$student_class= '7';}
    				if($ele==31 ){$student_class= '8';}
    				if($ele==32 ){$student_class= '9';}
    				if($ele==33 ){$student_class= '10';}
    				if($ele==34 ){$student_class= '11 COM';}
    				if($ele==35 ){$student_class= '11 SCI';}
    				if($ele==36 ){$student_class= '12 COM';}
    				if($ele==37 ){$student_class= '12 SCI';}
    				if($ele==38 ){$student_class= 'LKG';}
    				if($ele==39 ){$student_class= 'UKG';}
    				if($ele==40 ){$student_class= 'NUR';}
    				if($ele==41 ){$student_class= 'PRE NUR';}
    				
    			}
    			if($k == 'student_section')
    			{
    				if($ele==12 ){$student_section= 'A';}
    				if($ele==13 ){$student_section= 'B';}
    				if($ele==14 ){$student_section= 'C';}
    			}
    			if($k == 'student_dob')
    			{
    				$student_dob= $ele;
    			}
    		
    		}
    		// here work starts
			//echo $student_class."-".$student_section."<br>";
			 $field = Input::all();
			 $sessionid                  =       Session('valid_id');
        $sub = new Student;
        $sub->admission_no = $admission_no;
        $sub->student_name = $student_name;
        $sub->class_section = $student_class."-".$student_section;
        $sub->parent_name = $student_father_name;
		$sub->dob = $student_dob;
		$sub->sessionid = $sessionid;
        $sub->Save();


        $x = $student_class."-".$student_section; $entrypass=0;
        
        $entrypass = DB::table('classmgmts')->where('sessionid', $sessionid)->where('class_section', '=', $x)->count('id');
            if($entrypass ==0)
            {      
                $sub = new Classmgmt;
        $sub->class_section = $student_class."-".$student_section;
		$sub->description = 'N.A.';
		$sub->sessionid = $sessionid;
        if($sub->Save()){ echo 'done';}    
            }
        
        
    	}
    	//print_r($data);
    	return 'done';
    	//return view('errorpage');
    }

}


/*
admission_no
student_name
student_father_name
student_class
student_section
student_dob
*/
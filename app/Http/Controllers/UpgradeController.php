<?php

namespace DELLMOND\Http\Controllers;
use DELLMOND\Http\Requests;
use DELLMOND\User;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Input;
use DELLMOND\classmgmt;
use DELLMOND\feeamount;
use DELLMOND\feetype;
use DELLMOND\student;
use DELLMOND\discountfee;
use DELLMOND\feereciept_student;
use DELLMOND\feereciept_sc;
use DELLMOND\Sessionmanager;
use DELLMOND\discountprovider;
use DELLMOND\transportroute;


class UpgradeController extends Controller
{
    public function index() {

        $session = Sessionmanager::where('id','>', Session('valid_id') )->get();
        return view('upgrade.chooseupcomingsession')->with([
            'session' => $session,
        ]);
    }

    public function setupcomingsession(Request $req) {
        $nextsession = $req->input('nextsession');
        session(['nextsession' => $nextsession ]);
        $pd = Sessionmanager::where('id', $nextsession )->pluck('session_period');
        session(['nextsession_session_period' => $pd[0] ]);
        // dd($pd[0]);
        if($nextsession == null) {
            dd('Your Session was not selected so, terminating the program');
        }
        $session = Sessionmanager::find($nextsession);

        $students = student::where('sessionid', Session('valid_id') )->limit(5)->get();

        $classlist = classmgmt::where('sessionid', Session('valid_id') )->pluck('class_section');

        // dd($students, $classlist, $session);
        return view('upgrade.upgradeclassselect')->with([
            'session' => $session,
            'students' => $students,
            'classlist' => $classlist,
        ]);
    }

    public function showclasslist() {
        $classlist = classmgmt::where('sessionid', Session('valid_id') )->pluck('class_section');

        return view('upgrade.upgradeclasslist')->with([
            'classlist' => $classlist,
        ]);
    }

    public function showclasslistgenerate(Request $req) {
        $nextsession = Session('nextsession');
        $session = Sessionmanager::find($nextsession);

        $class     = $req->input('class_section');

        $students = student::where('sessionid', Session('valid_id') )
                            ->where('class_section', $class)->get();
       $classlist = classmgmt::where('sessionid', Session('valid_id') )->pluck('class_section');

        return view('upgrade.upgradehomepage')->with([
            'session' => $session,
            'students' => $students,
            'classlist' => $classlist,
        ]);
    }
    public function upgradestudents(Request $req) {
        $nextsession = Session('nextsession');
        $admission_no               = $req->input('admission_no');
        $next_class_section      = $req->input('student_class_section');
        $added = 0; $skipped = 0;

        foreach ($admission_no as $k1 => $v1) {
            $students = student::where('admission_no', $v1 )->where('sessionid', Session('valid_id') )->get();
            $count = student::where('admission_no', $v1 )->where('sessionid', $nextsession )->count('id');

            if($count < 1 ) {
                $x = new student;
                $x = $students[0]->replicate();
                
                $transportRouteCount = transportroute::where('parent_sessionid', $x->transport_route )->where('sessionid', $nextsession )->count('id');
                if($transportRouteCount > 0) {
                    $transportRoute = transportroute::where('parent_sessionid', $x->transport_route )->where('sessionid', $nextsession )->get();

                    $x->transport_route = $transportRoute[0]->id;
                }
                $x->class_section = $next_class_section[$k1];
                $x->session = Session('nextsession_session_period');
                $x->parent_sessionid = $v1;
                $x->sessionid = $nextsession;
                $x->save();
                $added = $added + 1;
            } else {
                $skipped = $skipped + 1;
            }

        }
        return view('upgrade.thankyou')->with([
            'added' => $added,
            'skipped' => $skipped,
        ]);
    }

    public function upgradeClassStructure(Request $req) {
        $nextsession = Session('nextsession');
        if(!$nextsession) {
            dd('Error! You have not selected the next session');
        }
        $added = 0; $skipped = 0;
        $classlist = classmgmt::where('sessionid', Session('valid_id') )->pluck('id'); 
        foreach ($classlist as $k1 => $v1) {
            $classdetails = classmgmt::where('id', $v1 )->where('sessionid', Session('valid_id') )->get();
            $count = classmgmt::where('parent_sessionid', $v1 )->where('sessionid', $nextsession )->count('id');
 
            if($count < 1 ) { 
                $x = new classmgmt; 
                $x = $classdetails[0]->replicate();
                $x->parent_sessionid = $v1;
                $x->sessionid = $nextsession;
                $x->save();
                $added = $added + 1;
            } else {
                $skipped = $skipped + 1;
            }
        }
        return view('upgrade.thankyou')->with([
            'added' => $added,
            'skipped' => $skipped,
        ]);
    }

    public function upgradeDiscountProvider(Request $req) {
        $nextsession = Session('nextsession');
        if(!$nextsession) {
            dd('Error! You have not selected the next session');
        }
        
        $added = 0; $skipped = 0;
        $discountProviderList = discountprovider::where('sessionid', Session('valid_id') )->pluck('id');
        foreach ($discountProviderList as $k1 => $v1) {
            $discountProviderDetails = discountprovider::where('id', $v1 )->where('sessionid', Session('valid_id') )->get();
            $count = discountprovider::where('parent_sessionid', $v1 )->where('sessionid', $nextsession )->count('id');
 
            if($count < 1 ) {
                $x = new discountprovider;
                $x = $discountProviderDetails[0]->replicate();
                $x->parent_sessionid = $v1;
                $x->sessionid = $nextsession;
                $x->save();
                $added = $added + 1;
            } else {
                $skipped = $skipped + 1;
            }
        }
        return view('upgrade.thankyou')->with([
            'added' => $added,
            'skipped' => $skipped,
        ]);
    }

    public function upgradeFeeAmounts(Request $req) {
        $nextsession = Session('nextsession');
        if(!$nextsession) {
            dd('Error! You have not selected the next session');
        }
        
        $added = 0; $skipped = 0;
        $feeAmountsList = feeamount::where('sessionid', Session('valid_id') )->pluck('id');
        foreach ($feeAmountsList as $k1 => $v1) {
            $feeAmountDetails = feeamount::where('id', $v1 )->where('sessionid', Session('valid_id') )->get();
            $count = feeamount::where('parent_sessionid', $v1 )->where('sessionid', $nextsession )->count('id');
 
            if($count < 1 ) {
                $x = new feeamount;
                $x = $feeAmountDetails[0]->replicate();
                $x->parent_sessionid = $v1;
                $x->sessionid = $nextsession;
                $x->save();
                $added = $added + 1;
            } else {
                $skipped = $skipped + 1;
            }
        }
        return view('upgrade.thankyou')->with([
            'added' => $added,
            'skipped' => $skipped,
        ]);
    }

    public function upgradeFeeTypes(Request $req) {
        $nextsession = Session('nextsession');
        if(!$nextsession) {
            dd('Error! You have not selected the next session');
        }

        $added = 0; $skipped = 0;
        $feeTypesList = feetype::where('sessionid', Session('valid_id') )->pluck('id');
        foreach ($feeTypesList as $k1 => $v1) {
            $feeTypesDetails = feetype::where('id', $v1 )->where('sessionid', Session('valid_id') )->get();
            $count = feetype::where('parent_sessionid', $v1 )->where('sessionid', $nextsession )->count('id');

            if($count < 1 ) {
                $x = new feetype;
                $x = $feeTypesDetails[0]->replicate();
                $x->parent_sessionid = $v1;
                $x->sessionid = $nextsession;
                $x->save();
                $added = $added + 1;
            } else {
                $skipped = $skipped + 1;
            }
        }
        return view('upgrade.thankyou')->with([
            'added' => $added,
            'skipped' => $skipped,
        ]);
    }

    public function upgradeTransports(Request $req) {
        $nextsession = Session('nextsession');
        if(!$nextsession) {
            dd('Error! You have not selected the next session');
        }

        $added = 0; $skipped = 0;
        $transportRoutesList = transportroute::where('sessionid', Session('valid_id') )->pluck('id');
        foreach ($transportRoutesList as $k1 => $v1) {
            $transportRouteDetails = transportroute::where('id', $v1 )->where('sessionid', Session('valid_id') )->get();
            $count = transportroute::where('parent_sessionid', $v1 )->where('sessionid', $nextsession )->count('id');

            if($count < 1 ) {
                $x = new transportroute;
                $x = $transportRouteDetails[0]->replicate();
                $x->parent_sessionid = $v1;
                $x->sessionid = $nextsession;
                $x->save();
                $added = $added + 1;
            } else {
                $skipped = $skipped + 1;
            }
        }
        return view('upgrade.thankyou')->with([
            'added' => $added,
            'skipped' => $skipped,
        ]);
    }


}

<?php


namespace DELLMOND\Http\Controllers;
use DELLMOND\Http\Requests;
use DELLMOND\User;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Input;
use DELLMOND\classmgmt;
use DELLMOND\feeamount;
use DELLMOND\transportstudents;
use DELLMOND\student;
use DB;
use Excel;

class transportlistparse extends Controller
{
    public function insertexcel()
    {
                $sessionid                  =       Session('valid_id');

        $results = Excel::load('/public/files/tr.xls', function($reader){
            //$reader->all();
            //$reader->limitRows(15)->limitColumns(6);
            $reader->limitColumns(10)->limitRows(1618);
        })->get();

        $counter_sucess = 0;
        $counter_failure = 0;
        $failure_array = array() ; 
        $r= array();
        foreach ($results as $key => $value) {
          
           array_push($r, $value);
           //print_r($value);
          // echo '<br>';
        }

      // print_r($r);

        foreach ($r as $key => $value) 
            {   $admission_no = 0; $name = "N.A"; $parent = "N.A"; $class = ""; $dob = "N.A"; $gender="";
            foreach ($value as $k => $v) { 
           if(($k == 'admno')  && ($v !='0' ) )     {  $admno    = $v;           }
           if(($k == 'studentname')  && ($v !='0' ) )       {  $name            = $v;           }
            if(($k == 'sex')  && ($v !='0' ) )      {  $sex          =$v;            }
            if(($k == 'fathername') && ($v !='0' ) )        {  $fathername             =  $v ;         }
            if(($k == 'amount')  && ($v !='0' ) )        {  $amount           = $v;           }
           
            } 
            
            $studentname      = isset($studentname        )?   $studentname        :     'N.A'      ;
            $admno            = isset($admno)? $admno  :      '0'       ;
            $amount           = isset($amount       )?   $amount       :     'N.A.'     ;
            $sex              = isset($sex      )?   $sex      :     'N.A.'     ;
            $fathername       = isset($fathername         )?   $fathername         :      'N.A.'    ;
            
     $route_code = 0;
     if($amount == '400') 	{  $route_code = 1; } 
     if($amount == '500') 	{  $route_code = 2; } 
     if($amount == '600') 	{  $route_code = 3; } 
     if($amount == '700') 	{  $route_code = 4; } 
     if($amount == '800') 	{  $route_code = 5; } 
     if($amount == '1000') 	{  $route_code = 6; } 

     \DB::table('students')->where('sessionid', $sessionid)->where('admission_no', '=', $admno)->update(['transport_route' => $route_code ]);
     \DB::table('students')->where('sessionid', $sessionid)->where('admission_no', '=', $admno)->update(['gender' => $sex ]);
    // echo $route_code."<br>".$admno;

    $tokens = \DB::table('transportstudents')->where('sessionid', $sessionid)->where('admno', '=', $admno)->count('*');
           // echo $tokens;
                if($admno != '0')
                {           echo "<br>";
                if( $tokens != 0) 
                    { 
                        echo $admno. 'student already exists !'; $counter_failure += 1;
                    }
                else
                    {
                    $sub = new transportstudents;
                    $sub->admno   =  $admno   ;
                    $sub->studentname        =  $studentname            ;
                    $sub->sex   =  $sex           ;
                    $sub->fathername  =  $fathername          ;
                    $sub->amount    =  $amount         ;
                    $sub->sessionid   = $sessionid;
                  

                    if ( $sub->Save() ) { echo $admno .'Added to database.' ; $counter_sucess += 1; }
                    else {  echo 'Unknown Problem encountered with'.$admno .' please add manually.'; }
                    }
                }
               
            }
     return view('thanks');
    }
}

<?php

namespace DELLMOND\Http\Controllers;
use DELLMOND\Http\Requests;
use DELLMOND\User;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Input;
use DELLMOND\classmgmt;
use DELLMOND\feeamount;
use DELLMOND\feetype;
use DELLMOND\student;
use DB;
use Excel;


class feeamountcontroller extends Controller
{
    public function addreq_form()
    {
        $sessionid                  =       Session('valid_id');

        $ft = array(); $cm = array();
    	$feetypes=\DB::table('feetypes')->where('sessionid', $sessionid)->select('fee_type')->get();
    	$classmgmts=\DB::table('classmgmts')->where('sessionid', $sessionid)->select('class_section')->get();
        $feeamountsmatrix=\DB::table('feeamounts')->where('sessionid', $sessionid)->select('fee_type','class_section', 'amount')->get();
        
        // return view('student.showstudentdetails')->with(['data'=>$data, 'form_heading' => 'Detail About Students with given Names' ]);
        foreach ($feetypes as $key => $value) {
           foreach ($value as $k => $v) {
             array_push($ft, $v);
            }
        }

        foreach ($classmgmts as $key => $value) {
           foreach ($value as $k => $v) {
             array_push($cm, $v);
            }
        }
    	return view ('fee.feevalueadd_req_form')->with(['feetypes'=>$feetypes, 'classmgmts' => $classmgmts, 'feeamountsmatrix' => $feeamountsmatrix ]);
    }
    public function addreq_form_submit()
    {
        $sessionid                  =       Session('valid_id');

    	$field = Input::all();
        $feetype        = isset($field['feetype'])       ?$field['feetype']         :'N.A.';
        $class_section  = isset($field['class_section']) ?$field['class_section']   :'N.A.';
        $amount         = isset($field['amount'])        ?$field['amount']          :'N.A.';
       
         $tokens = \DB::table('feeamounts')->where('sessionid', $sessionid)->where('fee_type', '=', $feetype)->where('class_section', '=', $class_section)->count('*');

        if( $tokens != 0) 
            { 
                \DB::table('feeamounts')->where('fee_type', '=', $feetype)->where('sessionid', $sessionid)->where('class_section', '=', $class_section)->update(['amount' => $amount]);
                return 'amount already exists ! It has been modified Now.'; 
            }
        else
            {
            $sub = new feeamount;
            $sub->fee_type 		= $feetype;
            $sub->class_section = $class_section;
            $sub->amount 	    = $amount;
            $sub->sessionid = $sessionid;
            if ( $sub->Save() ) { return 'Amount has been added.' ; }
            else { return 'Problem encountered, data might exits, please check it.'; }
            return $field['class_section'];
            }
    }
}

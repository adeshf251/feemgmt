<?php

namespace DELLMOND\Http\Controllers;

use Illuminate\Http\Request;
use DELLMOND\Sessionmanager;
use DELLMOND\Http\Requests;
use DELLMOND\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if( Session('valid_id') and Session('valid_period')  )
            {
               return view('home');
            }
        return redirect('sessionchoose');
    }

    public function selectSession()
    {
        $data = Sessionmanager::all();
        return View('sessionmgmt.choosesession')
            ->with('data', $data);
    }

    public function submitSession()
    {
        //
    }

    public function switchSession(Request $request)
    {
        $this->validate($request, [
            'activate_session' => 'required',
        ]);

        $val        =       $request->input('activate_session');
        $x = Sessionmanager::find($val);
        
        session(['valid_id' => $x->id ]);
        session(['valid_period' => $x->session_period]);
                    // echo session('valid_period', 'not-present');
        return redirect('home');
    }
}

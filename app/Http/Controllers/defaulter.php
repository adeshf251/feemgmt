<?php

namespace DELLMOND\Http\Controllers;
use DELLMOND\Http\Requests;
use DELLMOND\User;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Input;
use DELLMOND\classmgmt;
use DELLMOND\feeamount;
use DELLMOND\feetype;
use DELLMOND\student;
use DELLMOND\DefaulterModel;
use DB;
use Excel;

class defaulter extends Controller
{
    public function selectmonths()
    {
        
        $array_once_fee = array();
        $once_req_fee_array = DB::table('feetypes')->where('once_required', '=', '1')->where('sessionid', $sessionid)->pluck('fee_type');
        foreach ($once_req_fee_array as $key => $value) {  array_push($array_once_fee, $value);  }

        return view('defaulter.monthwisedefalter_selectmonths')->with(['once_required'=>$array_once_fee,'form_heading' => 'Select the Month to proceed',]);
    }
    public function monthwisegeneratelist()
    {
      $field = Input::all();
     $once_required_fee_array = unserialize(base64_decode($field['once_required_fee_array'])); 
     
    	$arr =array();        $month_counter = 0; $selectedmonths = Array();
       $m = array('jan', 'feb', 'march', 'april', 'may', 'june', 'july', 'aug', 'sep', 'oct', 'nov', 'dec',);
        foreach ($m as $key => $value) 
        {  $temp = isset($field[$value]) ? $field[$value] : '0';
             if( $temp !='0' )
             {
               array_push($selectedmonths, $value);   $month_counter += 1;
              }
        }

        $once_required_fee_copy = $once_required_fee_array;
        foreach ($once_required_fee_copy as $key => $value) 
        {  $temp = isset($field[$value]) ? $field[$value] : '0';
             if( $temp =='0' )
             {
               $once_required_fee_array = array_diff($once_required_fee_array, array($value));
              }
        }
        $result = array_merge($selectedmonths, $once_required_fee_array); 
       
        $data = \DELLMOND\DefaulterModel::getmonthwisedefaulter( $result ); 
        
      
      return view('defaulter.monthwisedefalterlist')->with(['data'=>$data,'month'=>'april','form_heading' => 'Month wise defaulter',]);
    }


    public function selectclass()
    {
        
         $data=\DB::table('classmgmts')->select('class_section')->where('sessionid', $sessionid)->get();
         $data_month = array('jan', 'feb', 'march', 'april', 'may', 'june', 'july', 'aug', 'sep', 'oct', 'nov', 'dec');

        return view('defaulter.selectclass')->with(['data'=>$data_month, 'data_cl'=>$data,'form_heading' => 'Select the Class to proceed',]);
    }
    public function generatedefaulterlist($month, $class)
    {
        $sessionid                  =       Session('valid_id');
      $students_total=\DB::select(" SELECT * FROM  students WHERE sessionid = $sessionid and  class_section = $class"); 
      $students_paid=\DB::select(" SELECT * FROM  feereciept_scs WHERE sessionid = $sessionid and  class_section = $class and fee_months = $month"); 
    }
    public function classwisegeneratelist()
    {
        $sessionid                  =       Session('valid_id');
        $field = Input::all(); // $sub = "janfees";
        $month    = $field['month'];
        $cl    = $field['cl'];

      $students_total=\DB::select(" SELECT admission_no,student_name,class_section,father_name,gender,address FROM  students WHERE  sessionid = $sessionid and  class_section = '$cl' "); 
      $students_paid=\DB::select(" SELECT * FROM  feereciept_scs WHERE  sessionid = $sessionid and  class_section = '$cl'  and fee_months = '$month'"); 
      
      $result = array(); $d2='';

        foreach ($students_total as $key => $v) { $x = array();
          foreach ($v as $k => $value) {
            array_push($x, $value);
          } 
          array_push($result, $x); $y = implode(" , ",$x); $d2 .= ' /  '.$y;
        }


      $data = $result;
       return view('defaulter.monthandclasswisedefaulterlist')->with(['data'=>$data,'d2'=>$d2, 'cl'=>$cl,'month'=>$month, 'form_heading' => 'Month and class wise defaulter',]);
    }

    public function overall()
    {
        $sessionid                  =       Session('valid_id');
    	$x = array();
    	$data_month = array('jan', 'feb', 'march', 'april', 'may', 'june', 'july', 'aug', 'sept', 'oct', 'nov', 'dec');
         foreach ($data_month as $key => $value) 
         {
         	$sub = $value."fees";
         
         

              $data=\DB::table('students')->join($sub, 'students.admission_no', '!=', $sub.'.admission_no')
             ->select('students.student_name', 'students.class_section', 'students.parent_name', 'students.dob', 'students.admission_no')
             ->where('students.sessionid', $sessionid)
             ->get();

             array_push($x, $data);
      		}
       return view('defaulter.overalldefaulter')->with(['data'=>$x,'form_heading' => 'Month and class wise defaulter',]);
    }

    public function exportexcel_monthwise_xlsx($month)
    {

       $orders = \DELLMOND\getcsv::getCSVReport( $month ); 

      foreach ($orders as $order) 
      {
        $data[] = array(
            "student_name"=> $order->student_name,
            "class_section" => $order->class_section,
            "parent_name"=> $order->parent_name,
            "admission_no"=> $order->admission_no,
        );
      }

     
      Excel::create('Event', function($excel) use($data) 
        {
            $excel->sheet('Sheetname', function($sheet) use($data) 
            {
                $sheet->fromArray($data);
            });
        })->export('xlsx');
    }

    public function exportexcel_monthwise_csv($month)
    {
       $orders = \DELLMOND\getcsv::getCSVReport( $month ); 

      foreach ($orders as $order) 
      {
        $data[] = array(
            "student_name"=> $order->student_name,
            "class_section" => $order->class_section,
            "parent_name"=> $order->parent_name,
            "admission_no"=> $order->admission_no,
        );
      }

     
      Excel::create('Event', function($excel) use($data) 
        {
            $excel->sheet('Sheetname', function($sheet) use($data) 
            {
                $sheet->fromArray($data);
            });
        })->export('csv');
    }


    public function exportexcel_monthwise_classwise_xlsx($month, $cl)
    {

       $orders = \DELLMOND\getcsv::getCSVRclasseport( $month , $cl ); 

      foreach ($orders as $order) 
      {
        $data[] = array(
            "student_name"=> $order->student_name,
            "class_section" => $order->class_section,
            "parent_name"=> $order->parent_name,
            "admission_no"=> $order->admission_no,
        );
      }

     
      Excel::create('Event', function($excel) use($data) 
        {
            $excel->sheet('Sheetname', function($sheet) use($data) 
            {
                $sheet->fromArray($data);
            });
        })->export('xlsx');
    }

    public function exportexcel_monthwise_classwise_csv($month, $cl)
    {
       $orders = \DELLMOND\getcsv::getCSVRclasseport( $month , $cl ); 

      foreach ($orders as $order) 
      {
        $data[] = array(
            "student_name"=> $order->student_name,
            "class_section" => $order->class_section,
            "parent_name"=> $order->parent_name,
            "admission_no"=> $order->admission_no,
        );
      }

     
      Excel::create('Event', function($excel) use($data) 
        {
            $excel->sheet('Sheetname', function($sheet) use($data) 
            {
                $sheet->fromArray($data);
            });
        })->export('csv');
    }

}

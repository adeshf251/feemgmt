<?php
namespace DELLMOND\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\Auth;
class RedirectIfNotAdmin
{
/**
 * Handle an incoming request.
 *
 * @param  \Illuminate\Http\Request  $request
 * @param  \Closure  $next
 * @param  string|null  $guard
 * @return mixed
 */
    public function handle($request, Closure $next, $guard = 'admin')
    {
        $value = Session('admin');
        if($value != 'yes') {
            return redirect('/admin/login');
        }
        // if (!Auth::guard($guard)->check()) {
        //     return redirect('admin/login');
        // }
        return $next($request);
        
        // if(!($request->session()->has('admin')) )
        //     {
        //         return redirect('/admin/login');
        //     }

        //     return $next($request);
    }
}
<?php

namespace DELLMOND;

use Illuminate\Database\Eloquent\Model;

class CollectionModel extends Model
{
    public static function getstudentname( $id )
    {
        $sessionid                  =       Session('valid_id');
               
    	return \DB::select(" SELECT student_name FROM  feereciept_students WHERE sessionid = $sessionid AND rept_no = $id ");
    }

    public static function collection_between_dates( $start_date, $end_date )
    {
        $sessionid                  =       Session('valid_id');
               
    	return \DB::select(" SELECT rept_no, admission_no, student_name, class_section, fee_months, total_paid  FROM  feereciept_students WHERE sessionid = $sessionid AND created_at BETWEEN '".$start_date."  00:00:00' AND '".$end_date." 23:59:59'");
    	//return 'hello';
    }

    public static function collection_between_dates_students_details_only( $start_date, $end_date )
    {
        $sessionid                  =       Session('valid_id');
              
    	return \DB::select(" SELECT rept_no, admission_no, student_name, class_section, fee_months, total_paid  FROM  feereciept_students WHERE sessionid = $sessionid AND created_at BETWEEN '".$start_date."  00:00:00' AND '".$end_date." 23:59:59'");
    	//return 'hello';
    }

    public static function collection_between_dates_amount_only( $start_date, $end_date )
    {
        $sessionid                  =       Session('valid_id');
              
    	return \DB::select(" SELECT SUM(total_paid) FROM  feereciept_students WHERE sessionid = $sessionid AND created_at BETWEEN '".$start_date."  00:00:00' AND '".$end_date." 23:59:59'");
    }

    public static function collection_between_dates_cash_students_details_only( $start_date, $end_date )
    {
        $sessionid                  =       Session('valid_id');
              
    	/*return \DB::select(" SELECT rept_no, admission_no, student_name, class_section, fee_months, total_paid  FROM  feereciept_students WHERE  payment_mode = 'cash' AND ( created_at >= '".$start_date."' OR  created_at <=  '".$end_date."' ) ");
        */
        return \DB::select(" SELECT rept_no, admission_no, student_name, class_section, fee_months, total_paid  FROM  feereciept_students WHERE  sessionid = $sessionid AND payment_mode = 'cash' AND created_at BETWEEN '".$start_date."  00:00:00' AND '".$end_date." 23:59:59'");
        
    }

    public static function collection_between_dates_cash_amount_only( $start_date, $end_date )
    {
        $sessionid                  =       Session('valid_id');
              
    	return \DB::select(" SELECT SUM(total_paid) FROM feereciept_students WHERE sessionid = $sessionid AND payment_mode = 'cash' AND  created_at BETWEEN '".$start_date."  00:00:00' AND '".$end_date." 23:59:59'");
    }


    public static function collection_between_dates_cheque_students_details_only( $start_date, $end_date )
    {
        $sessionid                  =       Session('valid_id');
              
    	return \DB::select(" SELECT rept_no, admission_no, student_name, class_section, fee_months, total_paid  FROM  feereciept_students WHERE  sessionid = $sessionid AND payment_mode LIKE 'cheque%' AND  created_at BETWEEN '".$start_date."  00:00:00' AND '".$end_date." 23:59:59'");
    }

    public static function collection_between_dates_cheque_amount_only( $start_date, $end_date )
    {
        $sessionid                  =       Session('valid_id');
              
    	return \DB::select(" SELECT SUM(total_paid) FROM feereciept_students WHERE sessionid = $sessionid AND payment_mode LIKE 'cheque%' AND  created_at BETWEEN '".$start_date."  00:00:00' AND '".$end_date." 23:59:59'");
    }


    public static function collection_between_dates_dd_students_details_only( $start_date, $end_date )
    {
        $sessionid                  =       Session('valid_id');
              
    	return \DB::select(" SELECT rept_no, admission_no, student_name, class_section, fee_months, total_paid  FROM  feereciept_students WHERE  sessionid = $sessionid AND payment_mode = 'dd%' AND  created_at BETWEEN '".$start_date."  00:00:00' AND '".$end_date." 23:59:59'");
    }

    public static function collection_between_dates_dd_amount_only( $start_date, $end_date )
    {
        $sessionid                  =       Session('valid_id');
              
    	return \DB::select(" SELECT SUM(total_paid) FROM feereciept_students WHERE sessionid = $sessionid AND payment_mode = 'dd%' AND  created_at BETWEEN '".$start_date."  00:00:00' AND '".$end_date." 23:59:59'");
    }


    public static function get_submitted_fee_months()
    {
        $sessionid                  =       Session('valid_id');
               
    	return \DB::select(" SELECT DISTINCT fee_months FROM feereciept_scs WHERE sessionid = $sessionid ");
    }
    public static function get_unique_class_registered()
    {
        $sessionid                  =       Session('valid_id');
              
        return \DB::select(" SELECT DISTINCT class_section FROM students WHERE sessionid = $sessionid ORDER BY class_section ASC ");
    }

    public static function get_class_registered_for_known_admission_no($admission_no)
    {
        $sessionid                  =       Session('valid_id');
              
        return \DB::select(" SELECT DISTINCT class_section FROM students WHERE sessionid = $sessionid AND admission_no='".$admission_no."' ORDER BY class_section ASC ");
    }

    public static function collection_fee_months_students_details_only( $m )
    {
        $sessionid                  =       Session('valid_id');
              
        return \DB::select(" SELECT rept_no, admission_no, student_name, class_section, fee_months, total_paid  FROM  feereciept_students WHERE  sessionid = $sessionid AND fee_months = '".$m."'");
    }

    public static function collection_fee_months_amount_only( $m )
    {
        $sessionid                  =       Session('valid_id');
              
        return \DB::select(" SELECT SUM(total_paid) FROM feereciept_students WHERE  sessionid = $sessionid AND fee_months = '".$m."'" );
    }


    public static function collection_admission_no_students_details_only( $admission_no )
    {
        $sessionid                  =       Session('valid_id');
              
        return \DB::select(" SELECT rept_no, admission_no, student_name, class_section, fee_months, total_paid  FROM  feereciept_students WHERE  sessionid = $sessionid AND admission_no = '".$admission_no."' ");
    }

    public static function collection_admission_no_amount_only( $admission_no )
    {
        $sessionid                  =       Session('valid_id');
              
        return \DB::select(" SELECT SUM(total_paid) FROM feereciept_students WHERE sessionid = $sessionid AND admission_no = '".$admission_no."'  ");
    }

    //******************************************** Transport Collection Mgmt ********************************
    public static function collection_between_dates_transport_students_details_only( $start_date, $end_date )
    {
        $sessionid                  =       Session('valid_id');
              
        return \DB::select(" SELECT rept_no, admission_no, student_name, class_section, fee_months, transportation_amount as total_paid FROM feereciept_scs WHERE  sessionid = $sessionid AND transportation_amount != '0' AND  created_at BETWEEN '".$start_date."  00:00:00' AND '".$end_date." 23:59:59'");
        return 0;
    }

    public static function collection_between_dates_transport_amount_only( $start_date, $end_date )
    {
        $sessionid                  =       Session('valid_id');
              
        return \DB::select(" SELECT SUM(transportation_amount) FROM feereciept_scs WHERE sessionid = $sessionid AND transportation_amount != '0' AND  created_at BETWEEN '".$start_date."  00:00:00' AND '".$end_date." 23:59:59'");
    }

     public static function collection_admission_no_transport_students_details_only( $admission_no )
    {
        $sessionid                  =       Session('valid_id');
              
        return \DB::select(" SELECT rept_no, admission_no, student_name, class_section, fee_months, transportation_amount as total_paid  FROM  feereciept_scs WHERE  sessionid = $sessionid AND admission_no = '".$admission_no."' ");
    }

    public static function collection_admission_no_transport_amount_only( $admission_no )
    {
        $sessionid                  =       Session('valid_id');
              
        return \DB::select(" SELECT SUM(transportation_amount) FROM feereciept_scs WHERE sessionid = $sessionid AND admission_no = '".$admission_no."'  ");
    }





    // now making defaulter healper functions

    public static function defaulter_monthwise_list( $m )
    {
        $sessionid                  =       Session('valid_id');
              
        return \DB::select(" SELECT admission_no,student_name,class_section,father_name ,gender FROM students WHERE students.sessionid = $sessionid AND students.admission_no NOT IN (SELECT feereciept_scs.admission_no FROM feereciept_scs WHERE feereciept_scs.sessionid = $sessionid AND feereciept_scs.fee_months='".$m."') ORDER BY students.admission_no ASC ");
    }

    public static function defaulter_monthwise_count( $m )
    {
        $sessionid                  =       Session('valid_id');
              
        return \DB::select(" SELECT COUNT(students.admission_no) FROM students WHERE students.sessionid = $sessionid AND students.admission_no NOT IN (SELECT feereciept_scs.admission_no FROM feereciept_scs WHERE feereciept_scs.sessionid = $sessionid AND feereciept_scs.fee_months='" .$m. "')  ");
    }

    public static function defaulter_classwise_list( $m, $c )
    {
        $sessionid                  =       Session('valid_id');
              
        return \DB::select(" SELECT admission_no,student_name,class_section,father_name ,gender FROM students WHERE students.sessionid = $sessionid AND students.admission_no NOT IN (SELECT feereciept_scs.admission_no FROM feereciept_scs WHERE feereciept_scs.sessionid = $sessionid AND feereciept_scs.fee_months='".$m."') AND students.class_section = '".$c."' ORDER BY students.admission_no ASC ");
    }

    public static function defaulter_classwise_count( $m, $c )
    {
        $sessionid                  =       Session('valid_id');
              
        return \DB::select(" SELECT COUNT(students.admission_no) FROM students WHERE students.sessionid = $sessionid AND students.admission_no NOT IN (SELECT feereciept_scs.admission_no FROM feereciept_scs WHERE feereciept_scs.sessionid = $sessionid AND feereciept_scs.fee_months='".$m."') AND students.class_section = '".$c."'");
    }

    public static function transport_defaulter_classwise_count( $m, $c )
    {
        $sessionid                  =       Session('valid_id');

        return \DB::select(" SELECT COUNT(students.admission_no) FROM students WHERE students.sessionid = $sessionid AND students.transport_route != '0' AND students.admission_no NOT IN (SELECT feereciept_scs.admission_no FROM feereciept_scs WHERE feereciept_scs.sessionid = $sessionid AND feereciept_scs.fee_months='".$m."') AND students.class_section = '".$c."'");
    }


    

        
    // where('sessionid', Session('valid_id') )
    //SELECT * FROM Orders WHERE OrderDate BETWEEN #07/04/1996# AND #07/09/1996#;

}

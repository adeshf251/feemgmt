<?php

namespace DELLMOND;

use Illuminate\Database\Eloquent\Model;

class feeamount extends Model
{
    public $fillable = ['fee_type','class_section' ,'amount'];
}
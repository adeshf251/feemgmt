<?php

namespace DELLMOND;

use Illuminate\Database\Eloquent\Model;

class classmgmt extends Model
{
    public $fillable = ['class_section','description'];
}

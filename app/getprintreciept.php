<?php

namespace DELLMOND;

use Illuminate\Database\Eloquent\Model;

class getprintreciept extends Model
{
    public static function getstudentname( $id )
    {
        $sessionid                  =       Session('valid_id');
              
    	return \DB::select(" SELECT student_name FROM  feereciept_students WHERE  sessionid = $sessionid AND rept_no = $id ");
    }

    public static function getstudentclass( $id )
    {
        $sessionid                  =       Session('valid_id');
              
    	return \DB::select(" SELECT class_section FROM  feereciept_students WHERE  sessionid = $sessionid AND rept_no = $id ");
    }

    public static function getstudentparent( $id )
    {
        $sessionid                  =       Session('valid_id');
              
    	return \DB::select(" SELECT father_name FROM  feereciept_students WHERE  sessionid = $sessionid AND rept_no = $id ");
    }

    public static function getstudentfeemonths( $id )
    {
        $sessionid                  =       Session('valid_id');
              
    	return \DB::select(" SELECT fee_months FROM  feereciept_students WHERE  sessionid = $sessionid AND rept_no = $id ");
    }

    public static function getstudentfeetypearray( $id )
    {
        $sessionid                  =       Session('valid_id');
              
    	return \DB::select(" SELECT feetypearray FROM  feereciept_students WHERE  sessionid = $sessionid AND rept_no = $id ");
    }

    public static function getstudentfeeamountarray( $id )
    {
        $sessionid                  =       Session('valid_id');
              
    	return \DB::select(" SELECT feeamountarray FROM  feereciept_students WHERE  sessionid = $sessionid AND rept_no = $id ");
    }

    public static function getstudentdiscount_provider( $id )
    {
        $sessionid                  =       Session('valid_id');
              
    	return \DB::select(" SELECT discount_provider FROM  feereciept_students WHERE  sessionid = $sessionid AND rept_no = $id ");
    }

    public static function getstudentdiscount_amount( $id )
    {
        $sessionid                  =       Session('valid_id');
              
    	return \DB::select(" SELECT discount_amount FROM  feereciept_students WHERE  sessionid = $sessionid AND rept_no = $id ");
    }

    public static function getstudenttotal_payable( $id )
    {
        $sessionid                  =       Session('valid_id');
              
    	return \DB::select(" SELECT total_payable FROM  feereciept_students WHERE  sessionid = $sessionid AND rept_no = $id ");
    }

    public static function getstudenttotal_paid( $id )
    {
        $sessionid                  =       Session('valid_id');
              
    	return \DB::select(" SELECT total_paid FROM  feereciept_students WHERE  sessionid = $sessionid AND rept_no = $id ");
    }

    public static function getstudentextradetails( $id )
    {
        $sessionid                  =       Session('valid_id');
              
    	return \DB::select(" SELECT admission_no FROM  feereciept_students WHERE  sessionid = $sessionid AND rept_no = $id ");
    }

    public static function getstudentinner_reciept_no( $id )
    {   // this corresponds to month wise reciepts in the feerecirpe_scs table
        $sessionid                  =       Session('valid_id');
              
    	return \DB::select(" SELECT extradetails FROM  feereciept_students WHERE  sessionid = $sessionid AND rept_no = $id ");
    }

    public static function getrept_no( $id )
    {
        $sessionid                  =       Session('valid_id');
              
        return \DB::select(" SELECT rept_no FROM  feereciept_students WHERE  sessionid = $sessionid AND rept_no = $id ");
    }

    public static function getpayment_mode( $id )
    {
        $sessionid                  =       Session('valid_id');
              
        return \DB::select(" SELECT payment_mode FROM  feereciept_students WHERE  sessionid = $sessionid AND rept_no = $id ");
    }

    public static function getupdated_at( $id )
    {
        $sessionid                  =       Session('valid_id');
              
    	return \DB::select(" SELECT updated_at FROM  feereciept_students WHERE  sessionid = $sessionid AND rept_no = $id ");
    }

    public static function getCSVRclasseport( $sub , $cl)
    {
        $sessionid                  =       Session('valid_id');
              
        //return \DB::table('students')->leftJoin($sub, 'students.admission_no', '=', $sub.'.admission_no')->select('students.student_name', 'students.class_section', 'students.parent_name', 'students.dob', 'students.admission_no');

    	//return \DB::table('students')->join($sub)->select('students.student_name', 'students.class_section', 'students.parent_name', 'students.dob', 'students.admission_no')->where('students.admission_no', '!=', $sub.'.admission_no');

    	return \DB::select(" SELECT * FROM  students WHERE  NOT EXISTS (SELECT *  FROM   $sub  WHERE students.class_section = '$cl' AND students.sessionid = $sessionid AND students.admission_no = $sub.admission_no ) ");
    }
}

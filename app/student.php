<?php

namespace DELLMOND;

use Illuminate\Database\Eloquent\Model;

class student extends Model
{
    public $fillable = ['student_name','class_section','parent_name','dob'];
}

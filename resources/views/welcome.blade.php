<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Easy Fee Management Tool</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-image: url('{{ url('/') }}/images/sc.jpg');
                background-size: 100%;
                background-position:left center;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 90vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 18px;
                font-weight: 600;
                letter-spacing: .15rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            a:link, a:visited {
                background-color: #990000;
                color: white;
                text-align: center;
                text-decoration: none;
                display: inline-block;
            }
            a:hover, a:active {
                background-color: red;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}"> <strong> Home </strong></a>
                    @else
                        <a href="{{ url('/login') }}"> <strong>  Login </strong></a>
                        <!-- <a href="{{ url('/register') }}"> <strong> Register </strong> </a> -->
                    @endif
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                
                </div>

            </div>
        </div>

        <div class="content">
                <div class="title m-b-md">
                
                </div>
<footer>
            <div style="width: 100%; height: 4vh; background-color: skyblue;">
                <p><strong> <b> Designed by : Vikas Tyagi @ Risinginnovators.com </b> </strong> </p>
            </div>
        </footer>
            </div>
        </div>
        
    </body>
</html>

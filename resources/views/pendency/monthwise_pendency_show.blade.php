
@extends('layouts.app2')

@section('content')

<?php $count = 1; $sum=0; ?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading">Defaulters List Monthwise</div>

                <div class="panel-body">
                <div id="colvis"></div>
                <table  class="table table-bordered" id="users-table">
                    <thead>
                        <th>S. No.</th>
                        <th>Class Name</th>
                        <th>No of Paid Students</th>
                        
                        <th>Expected Amount</th>
                        <th>Paid Amount</th>
                        <th>Transport Expected</th>
                        <th>Transport Paid</th>
                        <th>No of Pending Students</th>
                        <th>Monthly Fee Per Student</th>
                        <th>Pending Amount (monthly + Transport)</th>
                    </thead>
                    <tbody>
                    	@foreach ($className as $key => $element)
                        	<tr>
                        		<td> {{ $count++ }} </td>
                        		<td> {{ $className[$key] }} </td>
                        		<td> {{ $paidStudentsCount[$key] }} </td>
                        		
                        		<td> {{ $payableAmount[$key] }} </td>
                        		<td> {{ $paidAmpount[$key] }} </td>
                        		<td> {{ $payableTransportAmount[$key] }} </td>
                        		<td> {{ $paidTransportAmount[$key] }} </td>
                        		<td> {{ $pendingAtudentCount[$key] }} </td>
                        		<td> {{ $pendingAmountPerStudent[$key] }} </td>
                        		<td> {{ $a = ( $pendingAtudentCount[$key] * $pendingAmountPerStudent[$key] ) + ( $payableTransportAmount[$key] - $paidTransportAmount[$key] ) }} </td>
                        			<?php $sum += $a; ?>
                        	</tr>
                    	@endforeach
                    </tbody>

                    <tfoot>
                        <td colspan="9"><i> This amount is only expected, Actual might differ. </i> </td>
                        <td> {{ $sum }}</td>
                    </tfoot>
                </table>
       

                </div>
            </div>         
        </div>
    </div>
</div>


@endsection
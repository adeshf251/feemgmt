
@extends('layouts.app2')
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
@section('content')



<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading"> Pendency Amount Estimation For Individual Class for Individual Months</div>

                <div class="panel-body">
                <div id="colvis"></div>
                <table  class="table table-bordered" id="users-table">
                    <thead>
                        <th>Class-Section</th>
                        <th>Month</th>
                        <th>Paid Student Count</th>
                        
                        <th>Amount Payable</th>
                        <th>Amount Paid</th>
                        <th>Transport Payable</th>
                        <th>Transport Paid</th>
                        <th>Defaulter Count</th>
                        <th>Fee Amount Per Head</th>
                        <th>Total Pending (including Transport) </th>
                    </thead>
                    <tbody>
                        <tr>
                        	<td> {{ $class_section }} </td>
                        	<td> {{ $month }} </td>
                        	<td> {{ $paidStuCount }} </td>
                        	
                            <td> {{ $totalPayable }} </td>
                            <td> {{ $totalPaid }} </td>
                        	
                            <td> {{ $transportPayable }} </td>
                            <td> {{ $transportPaid }} </td>

                        	<td> {{ $defaulter_count }} </td>
                        	<td> {{ $fee_amount }} </td>
                        	<td> {{ ( $defaulter_count * $fee_amount )  + ( $transportPayable - $transportPaid )  }} </td>
                        </tr>
                    </tbody>

                    <tfoot>
                        <td colspan="10"><i> Original Pending Amount May be Less than the shown, due to discount. </i> </td>
                    </tfoot>
                </table>
       

                </div>
            </div>         
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function()
    {
          $("tr:even").css("background-color","#e5f9f9");

    });
</script>

@endsection
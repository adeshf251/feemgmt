
@extends('layouts.app2')

@section('content')

<?php $count = 1; $sum=0; ?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading">Overview of Student, Admission Number : {{ $admissionNumber }}</div>

                <div class="panel-body">
                <div id="colvis"></div>
                <table  class="table table-bordered" id="users-table">
                    <thead>
                        <th>S. No.</th>
                        <th>Class Name</th>
                        <th>Month</th>
                        
                        <th>Expected Amount</th>
                        <th>Paid Amount</th>
                        <th>Transport Amount</th>
                        <th>Status (1=Pending)</th>
                        <th>Monthly Fee Per Student</th>
                        <th>Pending Amount (Excluding Transport)</th>
                    </thead>
                    <tbody>
                    	@foreach ($className as $key => $element)
                        	<tr>
                        		<td> {{ $count++ }} </td>
                        		<td> {{ $className[$key] }} </td>
                        		<td> {{ $monthList[$key] }} </td>
                        		
                        		<td> {{ $payableAmount[$key] }} </td>
                        		<td> {{ $paidAmpount[$key] }} </td>
                        		<td> {{ $paidTransportAmount[$key] }} </td>
                        		<td> {{ $toggle = 1 - $paidStudentsCount[$key] }} </td>
                        		<td> {{ $pendingAmountPerStudent[$key] }} </td>
                        		<td> {{ $a = ( $toggle * $pendingAmountPerStudent[$key] ) }} </td>
                        			<?php $sum += $a; ?>
                        	</tr>
                    	@endforeach
                    </tbody>

                    <tfoot>
                        <td colspan="8"><i> This is only the Monthwise Fee (Excluding Transport). </i> </td>
                        <td> {{ $sum }}</td>
                    </tfoot>
                </table>
       

                </div>
            </div>         
        </div>
    </div>
</div>


@endsection
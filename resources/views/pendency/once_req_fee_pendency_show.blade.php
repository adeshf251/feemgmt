

@extends('layouts.app2')

@section('content')

<?php $count = 1; $tAmount=0; $tPayable=0; $sum=0; ?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading"> Overview of FEE </div>

                <div class="panel-body">
     
                <table  class="table table-bordered" id="users-table">
                    <thead>
                        <th>S. No.</th>
                        <th>Class Name</th>
                        <th>Month</th>
                        
                        <th> Paid Student Count </th>
                        <th> Paid Amount </th>
                        <th> Payable Amount </th>
                        <th> Pending Student Count </th>
                        <th> Pending Amount Per Student </th>
                        <th> Total Amount </th>
                    </thead>
                    <tbody>
                    	@foreach ($className as $key => $element)
                        	<tr>
                        		<td> {{ $count++ }} </td>
                        		<td> {{ $className[$key] }} </td>
                        		<td> {{ $monthList[$key] }} </td>
                        		
                        		<td> {{ $paidStudentsCount[$key] }} </td>
                        		<td> {{ $b = $paidAmpount[$key] }} </td>
                        		<td> {{ $c = $payableAmount[$key] }} </td>

                        		<td> {{ $pendingAtudentCount[$key] }} </td>
                                <td> {{ $pendingAmountPerStudent[$key] }} </td>
                                <td> {{ $a = ( $pendingAmountPerStudent[$key] * $pendingAtudentCount[$key] ) }} </td>
                        		
                        			<?php 
                        				$sum += $a;
                        				$tAmount += $b;
                        				$tPayable += $c;
                        			?>
                        	</tr>
                    	@endforeach
                    </tbody>

                    <tfoot>
                        <td colspan="4"><i> This is Expected Amount (Excluding Late Fee). </i> </td>
                        <td> 		{{ $tAmount }} 				</td>
                        <td> 		{{ $tPayable }} 			</td>
                        <td colspan="2"> </td>
                        <td> 		{{ $sum }} 					</td>
                    </tfoot>
                </table>
       

                </div>
            </div>         
        </div>
    </div>
</div>


@endsection



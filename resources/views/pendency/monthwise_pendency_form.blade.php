@extends('layouts.app')

@section('content')

 <form method="post" action="{{ url('/') }}/pendency-amt-monthwise-submit">
 {{ csrf_field() }}
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading"></div>

                <div class="panel-body">

                	<div class="form-group{{ $errors->has('month') ? ' has-error' : '' }}">
                    <label for="month" class="col-md-6 control-label">Select Fee Months / Fee Types Available*</label>

                    <div class="col-md-6"> 
                    <select name="month" class="form-control">
                    	@foreach ($options as $element)
                    	 	@foreach ($element as $ele)
                    	 		<option value="{{$ele}}"> {{$ele}} </option>
                    		@endforeach 
                    	@endforeach 
	                </select>
                    </div>
                </div>
                	
                <button type="submit" class="btn btn-success">Show Details</button>
                </div>
            </div>         
        </div>
    </div>
</div>

</form>          

@endsection
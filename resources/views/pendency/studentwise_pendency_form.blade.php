@extends('layouts.app')

@section('content')

 <form method="post" action="{{ url('/') }}/pendency-amt-studentwise-submit">
 {{ csrf_field() }}
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading"></div>

                <div class="panel-body">

                	<div class="form-group{{ $errors->has('month') ? ' has-error' : '' }}">
                    <label for="month" class="col-md-6 control-label">Select Fee Months / Fee Types Available*</label>

                    <div class="col-md-6"> 
                    <input type="number" id="admissionNumber" name="admissionNumber" class="form-control" placeholder="Enter the Admission Number">
                    </div>
                </div>
                	
                <button type="submit" class="btn btn-success">Show Details</button>
                </div>
            </div>         
        </div>
    </div>
</div>

</form>          

@endsection


@extends('layouts.app2')
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
@section('content')

 <form method="post" action="{{ url('/') }}/pendency-amt-once-req-fee-submit">
 {{ csrf_field() }}
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading"></div>

                <div class="panel-body">

                	<div class="form-group{{ $errors->has('onceRequired') ? ' has-error' : '' }}">
                    <label for="onceRequired" class="col-md-6 control-label">Select the Fee Type*</label>

                    <div class="col-md-6"> 
                    <select name="onceRequired" class="form-control">
                    	@foreach ($fee_type as $element)
                    	 		<option value="{{ $element->fee_type }}"> {{ $element->fee_type }} </option>
                    	@endforeach 
	                </select>
                    </div>
                </div>
                	
                <button type="submit" class="btn btn-success">Show Details</button>
                </div>
            </div>         
        </div>
    </div>
</div>

</form>          

@endsection
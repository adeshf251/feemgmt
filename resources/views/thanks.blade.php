@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-heading">
                    <h2>Thanks your data has been successfully modified</h2>
                    <br>
                    <h5>Modification means Add / Update / Delete of data</h5>
                </div>

                <div class="panel-body">

<h5> Please do not refresh or reload this page, we will redirect you to home page in 3 seconds</h5>
</div></div></div></div></div>


<script type="text/javascript">
	setTimeout(function () {    
    window.location.href = '{{ url('/') }}/home'; 
},2500); // 5 seconds
</script>
@endsection



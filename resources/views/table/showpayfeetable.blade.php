@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading">{{ $form_heading }}</div>

                <div class="panel-body">
                 

        @isset($data)
        <div id="colvis"></div>
        <table class="table table-bordered" id="users-table">
         <tr> <th>Student Name</th> <th>Class -Section</th> <th>Parent Name </th> <th>Date of Birth</th> <th>Admission No</th><th>Select</th></tr>
          @foreach ($data as $element)
              <tr>
                  @foreach ($element as $key => $val)
                      
                        @if($key != 'admission_no' )
                          <td>  <h4> {{ $val }} </h4> </td>
                        @endif
                      
                        @if($key == 'admission_no' )
                           <td>  <h4> {{ $val }} </h4> </td> <td> <a href="{{ url('/') }}/payfee/{{$val}}"> <button class="btn btn-success">Pay Fee  &#10159;</button> </a> </td>
                        @endif 
                      
                  @endforeach
              </tr>
          @endforeach
          </table>
        @endisset

                </div>
            </div>         
        </div>
    </div>
</div>
@endsection
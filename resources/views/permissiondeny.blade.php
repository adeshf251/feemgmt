@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-heading"><h2>Sorry Page Can't be displayed</h2></div>

                <div class="panel-body">

<h3> You Don't have Permission to view this page.</h3> 
<h4> Reasons :</h4> 
<h5> 1- Either You Entered a wrong password. </h5> 
<h5> 2- Your Session Expired. </h5> 
<h5> 3- Data You are looking, doesn't exists. </h5> 
<h5> 4- Tried to Access the banned URL for direct access. </h5> 
<h5> 5- Your ONE-TIMEUSE Password Period Over. </h5> 
<br>
<br>
<h4> What to do Now ?</h4> 
<h5> 1- Retry the page by going a step backward. </h5> 
<h5> 2- If Password Required than Enter the Correct Password. </h5> 
<h5> 3- Try to go to your Dashboard and Retry the Entire Procedure. </h5> 
<h5> 4- Check the presence of Data, (in case of missing). </h5> 
</div></div></div></div></div>



@endsection



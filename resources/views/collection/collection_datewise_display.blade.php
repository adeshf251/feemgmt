@extends('layouts.app2')
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
@section('content')

<?php $sum = 0; $counter=1; ?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading"> &#10070; Collections</div>

                <div class="panel-body">
        <div id="colvis"></div>
                <table  class="table table-bordered" id="users-table">
                    <thead>
                        <th>S.No</th>
                        <th>Reciept Number</th>
                        <th>Admission Number</th>
                        <th>Student Name</th>
                        <th>Class-Section</th>
                        <th>Fee Month/Type</th>
                        <th>Total Paid</th>
                    </thead>
                    <tbody>
                        @foreach ($student_details as $element)
                            <tr> <td> {{ $counter++ }} </td>
                                @foreach ($element as $key => $val)
                                    <td> {{ $val }} </td>
                                    @if ($key == 'total_paid')
                                        <?php $sum = $sum + $val ; ?>
                                    @endif
                                @endforeach
                            </tr>
                        @endforeach
                    </tbody>

                    <tfoot>
                        <td colspan="6"><strong> Total Collection between {{$start_date}} and {{$end_date}} </strong> </td>
                        <td><b> <strong> {{$sum}} </strong> </b></td>
                    </tfoot>
                </table>
       

                </div>
            </div>         
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function()
    {
          $("tr:even").css("background-color","#e5f9f9");

    });
</script>

@endsection
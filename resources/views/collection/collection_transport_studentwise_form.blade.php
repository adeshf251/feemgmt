
@extends('layouts.app')
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
@section('content')

 <form method="post" action="{{ url('/') }}/collection-student-transport-submit">
 {{ csrf_field() }}
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading"></div>

                <div class="panel-body">

                
      
                	<div class="form-group{{ $errors->has('admission_no') ? ' has-error' : '' }}">
	                    <label for="admission_no" class="col-md-6 control-label">Admission Number*</label>

	                    <div class="col-md-6">
	                        <input id="admission_no" type="text" class="form-control" name="admission_no" value="{{ old('admission_no') }}"  autofocus required="required">

	                        @if ($errors->has('admission_no'))
	                            <span class="help-block">
	                                <strong>{{ $errors->first('admission_no') }}</strong>
	                            </span>
	                        @endif
	                    </div>
                	</div>

                	
                	
                <button type="submit" class="btn btn-success">Show Details</button>
                </div>
            </div>         
        </div>
    </div>
</div>

</form>          

<script type="text/javascript">
	$(document).ready(function()
	{
		var todayUTC = new Date().toISOString().substr(0,10);
		$("#collection_start_date").attr("value", todayUTC);

		$("#collection_end_date").click(function()
		{
			var x=$('#collection_start_date').val();
			$("#collection_end_date").attr("min",x);

		});
		
		//$("tr:even").css('background-color', 'gray');
	});
</script>

@endsection
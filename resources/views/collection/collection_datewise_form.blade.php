@extends('layouts.app')
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
@section('content')

 <form method="post" action="{{ url('/') }}/collection-datewise-submit">
 {{ csrf_field() }}
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading"></div>

                <div class="panel-body">

                
      
                	<div class="form-group{{ $errors->has('collection_start_date') ? ' has-error' : '' }}">
	                    <label for="collection_start_date" class="col-md-6 control-label">Starting Date*</label>

	                    <div class="col-md-6">
	                        <input id="collection_start_date" type="date" class="form-control" name="collection_start_date" value="{{ old('collection_start_date') }}"  max="2030-12-31" autofocus required="required">

	                        @if ($errors->has('collection_start_date'))
	                            <span class="help-block">
	                                <strong>{{ $errors->first('collection_start_date') }}</strong>
	                            </span>
	                        @endif
	                    </div>
                	</div>
                
                	<div class="form-group{{ $errors->has('collection_end_date') ? ' has-error' : '' }}">
	                    <label for="collection_end_date" class="col-md-6 control-label">End Date*</label>

	                    <div class="col-md-6">
	                        <input id="collection_end_date" type="date" class="form-control" name="collection_end_date" value="{{ old('collection_end_date') }}"  max="2030-12-31" autofocus required="required">

	                        @if ($errors->has('collection_end_date'))
	                            <span class="help-block">
	                                <strong>{{ $errors->first('collection_end_date') }}</strong>
	                            </span>
	                        @endif
	                    </div>
                	</div>

                	<div class="form-group{{ $errors->has('payment_type') ? ' has-error' : '' }}">
                    <label for="payment_type" class="col-md-6 control-label">Payment Type*</label>

                    <div class="col-md-6"> 
                    <select name="payment_type" class="form-control"> 
	                    <option value="cash"> Cash </option>
	                    <option value="cheque"> Cheque </option>
	                    <option value="dd"> DD </option>
	                </select>
                    </div>
                </div>
                	
                <button type="submit" class="btn btn-success">Show Details</button>
                </div>
            </div>         
        </div>
    </div>
</div>

</form>          

<script type="text/javascript">
	$(document).ready(function()
	{
		var todayUTC = new Date().toISOString().substr(0,10);
		$("#collection_start_date").attr("value", todayUTC);

		$("#collection_end_date").click(function()
		{
			var x=$('#collection_start_date').val();
			$("#collection_end_date").attr("min",x);

		});
		
		//$("tr:even").css('background-color', 'gray');
	});
</script>

@endsection
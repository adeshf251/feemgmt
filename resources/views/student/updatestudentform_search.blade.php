@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-heading"> Update Student Details</div>

                <div class="panel-body">
<form method="post" action="{{ url('/') }}/update-student-detail-request-submit">
 {{ csrf_field() }}

 		<div class="form-group col-md-12">
	    <label class="control-label" for="admission_no">Search Student by Admission Number*</label>
	    <input type="text" class="form-control"  id="admission_no" name="admission_no" autofocus >
	    </div>


	    <div class="form-group col-md-12">
	    <label class="control-label" for="admission_number">OR</label>
	    </div>



	    <div class="form-group col-md-12">
	    <label class="control-label" for="stu_name">Search Student by Name*</label>
	    <input type="text" class="form-control"  id="stu_name" name="stu_name" autofocus >
	    </div>



 <br>
<input type="submit" name="submit" class="btn btn-info" value="Search Students">
</form>                 


                </div>
            </div>         
        </div>
    </div>
</div>
@endsection
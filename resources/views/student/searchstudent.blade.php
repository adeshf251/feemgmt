@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-heading">{{ $form_heading }}</div>

                <div class="panel-body">
                 

{!! Form::open(array('route' => $submit_to,'files'=>true)) !!}
            <div class="row">
                <div class="col-md-8">
    @isset($txtnm)
       @foreach($txtnm as $key => $row)
            {!! Form::label($txtnm[$key], $txtlabels[$key]) !!}
            {!! Form::text($txtnm[$key], '', array('class' => 'form-control')) !!}
       @endforeach
    @endisset

    @isset($optionsnm)
       @foreach($optionsnm as $key => $row)
            {!! Form::label($optionsnm[$key], $optionslabels[$key]) !!}
            {!! Form::select($optionsnm[$key], $optionsvalue[$key], array('class' => 'form-control')) !!} </br>
       @endforeach
    @endisset

</br>
                    <button type="submit" class="btn btn-success">Submit/Upload</button>
                </div>
            </div>
    {!! Form::close() !!}






                </div>
            </div>         
        </div>
    </div>
</div>
@endsection
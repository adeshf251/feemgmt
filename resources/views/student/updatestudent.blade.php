@extends('layouts.app')

@section('content')
<script src="/public/js/jquery-1.12.2.js"></script>

 {{ csrf_field() }}
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading">Discount Provider Registration
                    <a href="{{ url('/') }}/update-student-detail-request/0"> [ reset list ] </a>
                    <a href="{{ url('/') }}/update-student-detail-request/{{ $nextlimit }}"> [ next 100 students ] </a>
                    <div id="processing"></div>
                </div>

                <div class="panel-body">

 <div class="form-group">
		<label for="class_section">Select the class applicable for</label>
		<table class="table">
         <th>Admission No</th><th>Student Name</th><th>Class-Section</th><th>New Class-Section</th><th>Parent Name</th><th>DOB</th><th>Submit</th>
		@foreach ($data as  $k => $element) <tr> <form action="{{ url('/') }}/update-student-detail-request-submit"  method="POST" > {{ csrf_field() }}   
			@foreach ($element as $key => $e) 
				
                    @if($key == 'admission_no')
                      <td>  <input type='text' name='admission_no' id='admission_no' value='{{ $e }}' class="form-control" />  </td>
                    @endif
                    @if($key == 'student_name')
                      <td>  <input type='text' name='studentname' id='studentname' value='{{ $e }}' class="form-control" />
                    @endif
                    @if($key == 'class_section')
                     <td><input type='text' name='class_section' id='class_section' value='{{ $e }}' class="form-control" />
                     </td>
                      <td>  <select name="clsec" id="clsec" class="form-control">  <option value="0" selected="selected"> -- select-- </option>
                            @foreach ($classopt as $element)
                                @foreach ($element as $e)
                                    <option value="{{ $e }}"> {{ $e }} </option>
                                @endforeach
                            @endforeach
                        </select>
                        </td>
                    @endif
                    @if($key == 'parent_name')
                      <td>  <input type='text' name='parent' id='parent' value='{{ $e }}' class="form-control" /> </td>
                    @endif
                    @if($key == 'dob')
                       <td> <input type='text' name='dob' id='dob' value='{{ $e }}' class="form-control" /> </td>
                    @endif
                    @if($key == 'id')
                      <td>  <input type='hidden' name='id' id='id' value='{{ $e }}' class="form-control" />
                        <input type="submit" id="submit" class="btn btn-success" /></td>
                    @endif
               
			@endforeach </form>   </tr>
		@endforeach
		</table>
 </div>

     
                </div>
            </div>         
        </div>
    </div>
</div>


        
    






@endsection

 
	            
	             
	            
	   
@extends('layouts.app')

@section('content')
<form method="post" action="{{ url('/') }}/reg-submit">
 {{ csrf_field() }}
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading">New Student Registration</div>

                <div class="panel-body">
	    
	    <div class="form-group col-md-6">
	    <label class="control-label" for="stdname">Student Name (Required)*</label>
	    <input type="text" class="form-control"  id="stdname" name="stdname" autofocus required>
	    </div>

	    <div class="form-group col-md-3">
	    <label class="control-label" for="admission_no">Admission Number (Required)*</label>
	    <input type="text" class="form-control"  id="admission_no" name="admission_no" autofocus required>
	    </div>

	    <div class="form-group col-md-3">
	    <label class="control-label"  for="position">Class *</label>
	    <select name="clsec" class="form-control">
			@foreach ($classname as $element)
				@foreach ($element as $e)
					<option value="{{ $e }}"> {{ $e }} </option>
				@endforeach
			@endforeach
		</select>
	    </div>


	    <div class="form-group col-md-6">
	    <label class="control-label"  for="gender">Gender</label>
	    <select name="gender" class="form-control" id="gender">
	    	<option value="male"> Male </option>
	    	<option value="male"> Female </option>
	    </select>
	    </div>

	    <div class="form-group col-md-6">
	    <label class="control-label"  for="mother_name">Mother Name</label>
	    <input type="text" class="form-control"  id="mother_name" name="mother_name" autofocus required>
	    </div>

	    <div class="form-group col-md-6">
	    <label class="control-label"  for="father_name">Father Name</label>
	    <input type="text" class="form-control"  id="father_name" name="father_name" autofocus required>
	    </div>


	    <div class="form-group col-md-6">
	    <label class="control-label"  for="address">Address</label>
	    <input type="text" class="form-control"  id="address" name="address" autofocus required>
	    </div>

	    <div class="form-group col-md-6">
	    <label class="control-label"  for="mobile">Mobile No</label>
	    <input type="number" class="form-control"  id="mobile" name="mobile" autofocus required>
	    </div>

	    <div class="form-group col-md-6">
	    <label class="control-label"  for="transport_route">Transportation Route</label>
	    <input type="number" class="form-control"  id="transport_route" name="transport_route" value="0" autofocus required>
	    </div>

	    
	    <div class="form-group col-md-6">
	    <label class="control-label"  for="remark">Remark / Other Details </label>
	    <input type="text" class="form-control"  id="remark" name="remark" value="N.A." autofocus >
	    </div>

	    <div class="form-group col-md-6">
	    <label class="control-label"  for="dob">Date of birth*</label>
	    <input type="date" class="form-control"  id="dob" name="dob" autofocus required>
	    </div>

	    <div class="form-group col-md-6">
	   <button type="submit" class="btn btn-success col-md-6">Register Student</button>
	    </div>
   
     
     
                </div>
            </div>         
        </div>
    </div>
</div>
@endsection

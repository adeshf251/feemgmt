<?php $counter=1; ?>
@extends('layouts.app2')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading">List of Students </div>

                <div class="panel-body">
        <div id="colvis"></div>
        <table class="table table-bordered" id="users-table">
        <thead>
            <tr>
              <th>SNo</th>
              <th>Admission No</th>
              <th>Student Name</th>
              <th>Class-Section</th>
              <th>Parent's Name</th>
              <th>Gender</th>
              <th>Mobile</th>
              <th>Date of Birth</th>
            </tr>
        </thead>
         
         <tbody>
         	
         		@foreach ($data as $element)
         			<tr>
         				<td> {{ $counter++ }} </td>
	         			@foreach ($element as $e)
         			
			         			<td> {{ $e }} </td>
			         		
		         		@endforeach
	         		</tr>
         		@endforeach
         	
         </tbody>
          
          </table>
        
                </div>
            </div>         
        </div>
    </div>
</div>
@endsection


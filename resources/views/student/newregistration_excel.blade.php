@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading">New Student Registration</div>

                <div class="panel-body">
	 <form method="post" action="{{ url('/') }}/reg-submit-excel"  enctype="multipart/form-data">
 		{{ csrf_field() }}   
	    <div class="form-group col-md-6">
	    <label class="control-label" for="student">Select xls extension file*</label>
	    <input type="file" class="form-control"  id="student" name="student" autofocus required>
	     <div id="gap" style="width: 100%; height: 20px;"></div>
	     <button type="submit" class="btn btn-success col-md-6">Register Student</button>
	    </div>


     
     
                </div>
            </div>         
        </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading">Rules to follow for Excel Upload</div>

                <div class="panel-body">
                # There should be not more than 200 rows at a time, if more than upload in two part i.e two times. <br>
	 			# There should be 6 column only <br>
	 			* first column for addmission no, header name = an <br>
	 			* second column for student name, header name = sn <br>
	 			* third column for parent name, header name = pn <br>
	 			* fourth column for class, header name = cl <br>
	 			* fifth column for section, header name = sec <br>
	 			* sixth column for date of birth, header name = dob <br>
                </div>
            </div>         
        </div>
    </div>
</div>
@endsection

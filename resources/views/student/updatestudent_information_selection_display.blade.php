

@extends('layouts.app')

@section('content')
<form method="post" action="{{ url('/') }}/update-student-commit">
 {{ csrf_field() }}
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading"> &#9851; Update Student's Information / Registration Details</div>

                <div class="panel-body">
	 
	    @foreach ($data as $element)
	    	@foreach ($element as $key => $dataholded)
	    		@if ($key == 'id')
	    			<input type="hidden" name="id" value="{{ $dataholded }}">
	    		@endif
	    		@if ($key == 'admission_no')
	    			<div class="form-group col-md-3">
				    <label class="control-label" for="admission_no">Admission Number (Required)*</label>
				    <input type="text" class="form-control"  id="admission_no" name="admission_no" value="{{ $dataholded }}" autofocus required>
				    </div>
	    		@endif
	    		@if ($key == 'student_name')
	    			<div class="form-group col-md-6">
				    <label class="control-label" for="student_name">Student Name (Required)*</label>
				    <input type="text" class="form-control"  id="student_name" name="student_name" value="{{ $dataholded }}" autofocus required>
				    </div>
	    		@endif
	    		@if ($key == 'class_section')
	    			<div class="form-group col-md-3">
				    <label class="control-label"  for="class_section">Class *</label>
				    <select name="class_section" class="form-control">
						@foreach ($classopt as $element)
							@foreach ($element as $e)
								<option value="{{ $e }}" @if($dataholded == $e) selected="selected" @endif  > {{ $e }} </option>
							@endforeach
						@endforeach
					</select>
				    </div>
	    		@endif
	    		@if ($key == 'father_name')
	    			<div class="form-group col-md-6">
				    <label class="control-label"  for="father_name">Father Name</label>
				    <input type="text" class="form-control"  id="father_name" name="father_name" value="{{ $dataholded }}" autofocus required>
				    </div>
	    		@endif
	    		@if ($key == 'mother_name')
	    			<div class="form-group col-md-6">
				    <label class="control-label"  for="mother_name">Mother Name</label>
				    <input type="text" class="form-control"  id="mother_name" value="{{ $dataholded }}" name="mother_name" autofocus required>
				    </div>
	    		@endif
	    		@if ($key == 'gender')
	    			<div class="form-group col-md-6">
				    <label class="control-label"  for="gender">Gender</label>
				    <select name="gender" class="form-control" id="gender">
				    	<option value="M" @if($dataholded == 'M') selected="selected" @endif > Male </option>
				    	<option value="F"  @if($dataholded == 'F') selected="selected" @endif > Female </option>
				    </select>
				    </div>
	    		@endif
	    		@if ($key == 'address')
	    			<div class="form-group col-md-6">
				    <label class="control-label"  for="address">Address</label>
				    <input type="text" class="form-control"  id="address" name="address" value="{{ $dataholded }}" autofocus required>
				    </div>
	    		@endif
	    		@if ($key == 'mobile')
	    			<div class="form-group col-md-6">
				    <label class="control-label"  for="mobile">Mobile No</label>
				    <input type="number" class="form-control"  id="mobile" name="mobile" value="{{ $dataholded }}" autofocus required>
				    </div>
	    		@endif
	    		@if ($key == 'transport_route')
	    			<div class="form-group col-md-6">
				    <label class="control-label"  for="transport_route">Transport Route (Check Transport ID from Update Transport) *</label>
				    <select name="transport_route" class="form-control"> <option value="0">Not a Transport User</option>
						@foreach ($routess as $element)
							@foreach ($element as $key => $e)
								<option value="{{ $e }}" @if($dataholded == $e) selected="selected" @endif  > {{ $e }} </option>
							@endforeach
						@endforeach
					</select>
				    </div>
	    		@endif
	    		@if ($key == 'dob')
	    			 <div class="form-group col-md-6">
				    <label class="control-label"  for="dob">Date of birth*</label>
				    <input type="text" class="form-control"  id="dob" name="dob" value="{{ $dataholded }}" autofocus required>
				    </div>
	    		@endif
	    		@if ($key == 'remark')
	    			<div class="form-group col-md-6">
				    <label class="control-label"  for="remark">Remark / Other Info (like mobile2, siblings information) </label>
				    <input type="text" class="form-control"  id="remark" name="remark" value="{{ $dataholded }}" autofocus >
				    </div>
	    		@endif

	    	@endforeach
	    @endforeach
	    

	    
	    <div style="width: 100%; height: 20px"></div>
	    


	    

	    

	    


	    

	    

	    

	    
	    

	   

	    <div class="form-group col-md-6">
	   <button type="submit" class="btn btn-success col-md-6">Update Information  &#10004; </button>
	    </div>
   
     
     
                </div>
            </div>         
        </div>
    </div>
</div>
@endsection

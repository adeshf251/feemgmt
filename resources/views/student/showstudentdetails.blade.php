@extends('layouts.app2')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading">{{ $form_heading }}</div>

                <div class="panel-body">
                 
<?php $x = 0; ?>
        @isset($data)
        <table class="table table-bordered" id="users-table">
          <tr> <th>Column</th> <th>Information</th> </tr>
          @foreach ($data as $element)
              
                  @foreach ($element as $key => $val)
                     <tr>
                        <td> <h4> {{$data_name[$x]}} </h4> </td>
                        <td>  <h4> {{ $val }} </h4> </td> 
                      </tr>

                      <?php $x += 1 ; ?>

                   @endforeach
             
          @endforeach
          </table>
        @endisset

                </div>
            </div>         
        </div>
    </div>
</div>
@endsection
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
     <ol class="breadcrumb">
            <li class="breadcrumb-item active"><i class="fa fa-tachometer" aria-hidden="true"></i>
              Fee Management </li>
          </ol>
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

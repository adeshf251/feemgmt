@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-heading">{{ $form_heading }}</div>

                <div class="panel-body">
                 

{!! Form::open(array('route' => $submit_to,'files'=>true)) !!}
            <div class="row">
                <div class="col-md-8">
    @isset($txtnm)
       @foreach($txtnm as $key => $row)
            {!! Form::label($optionsnm[$key], $optionslabels[$key]) !!}
            {!! Form::$optionstype[$key]($optionsnm[$key], '', array('class' => 'form-control')) !!}
       @endforeach
    @endisset

   
{!! Form::select('classname', $products, '2', ['class' => 'form-control']) !!}
</br>
                    <button type="submit" class="btn btn-success">Submit/Upload</button>
                </div>
            </div>
    {!! Form::close() !!}






                </div>
            </div>         
        </div>
    </div>
</div>
@endsection


@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-heading">{{ isset($table_heading)?$table_heading : 'Generated list is given below, click on the Modify and Remove Buttons Provided to perform actions as specified' }}</div>

                <div class="panel-body">
                 
<table class="table table-bordered" id="users-table">
  @isset($table_data)
       @foreach($table_data as $keymain => $row)
            <tr> 
                @foreach($row as $keysub => $element)
                    <td> {!! $element !!}  </td>
                @endforeach
            </tr>
       @endforeach
    @endisset
</table>






                </div>
            </div>         
        </div>
    </div>
</div>
@endsection
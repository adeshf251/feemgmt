@extends('layouts.app')

@section('content')
<div class="container"  >
    

    <div class="row">

    <ol class="breadcrumb">
            <li class="breadcrumb-item active"><i class="fa fa-area-chart" aria-hidden="true"></i>
              Additional Setting </li>
    </ol>
        <div class="col-md-10 col-md-offset-1" style="text-align:center">
            <div class=" panel panel-default"  style="width: 150px; height: 150px; float: left;">
              <a href="sessionchoose">  <div class=" panel-body">
                           <img src="{{ url('/') }}/icons/024-technology-1.png" style="width: 60px; height: 60px; margin-bottom:10px; margin-left: 20px; margin-right:30px;">
                           Change Working Session
                          </div>
               </a>
            </div> 

            <div class="panel panel-default"  style="width: 150px; height: 140px; float: left; margin-left: 40px;">
              <a href="sendmailform">  <div class=" panel-body">
                           <img src="{{ url('/') }}/icons/041-message-1.png" style="width: 60px; height: 60px; margin-bottom:10px; margin-left: 20px;  margin-right:30px;">
                           Issue Email Notification
                          </div>
               </a>
            </div> 

            <div class="panel panel-default"  style="width: 150px; height: 140px; float: left; margin-left: 40px;">
              <a href="sendmailform">  <div class=" panel-body">
                           <img src="{{ url('/') }}/icons/040-checked.png" style="width: 60px; height: 60px; margin-bottom:10px; margin-left: 20px;  margin-right:30px;">
                           Reminder
                          </div>
               </a>
            </div> 

        </div>

        </div>



        <div class="row">

    <ol class="breadcrumb">
            <li class="breadcrumb-item active"><i class="fa fa-area-chart" aria-hidden="true"></i>
              Upgrade & Session Management </li>
    </ol>
        <div class="col-md-10 col-md-offset-1" style="text-align:center">
            <div class=" panel panel-default"  style="width: 150px; height: 150px; float: left;">
              <a href="upgrade">  <div class=" panel-body">
                           <img src="{{ url('/') }}/icons/018-multimedia-1.png" style="width: 60px; height: 60px; margin-bottom:10px; margin-left: 20px;   margin-right:30px;">
                           Upgrade to new Session
                          </div>
               </a>
            </div> 

            <div class="panel panel-default"  style="width: 150px; height: 140px; float: left; margin-left: 40px;">
              <a href="session-create">  <div class=" panel-body">
                           <img src="{{ url('/') }}/icons/049-monitor.png" style="width: 60px; height: 60px; margin-bottom:10px; margin-left: 20px;  margin-right:30px;">
                           Create New Session
                          </div>
               </a>
            </div> 

            <div class="panel panel-default"  style="width: 150px; height: 140px; float: left; margin-left: 40px;">
              <a href="session-read">  <div class=" panel-body">
                           <img src="{{ url('/') }}/icons/004-workplace.png" style="width: 60px; height: 60px; margin-bottom:10px; margin-left: 20px;  margin-right:30px;">
                           Show All Sessions
                          </div>
               </a>
            </div> 

        </div>

        </div>


        <div class="row">

            <ol class="breadcrumb">
                    <li class="breadcrumb-item active"><i class="fa fa-area-chart" aria-hidden="true"></i>
                      Password And User Management </li>
            </ol>
            <div class="col-md-10 col-md-offset-1" style="text-align:center">
                <div class=" panel panel-default"  style="width: 150px; height: 150px; float: left;">
                  <a href="adduserform">  <div class=" panel-body">
                               <img src="{{ url('/') }}/icons/026-social.png" style="width: 60px; height: 60px; margin-bottom:10px; margin-left: 20px;   margin-right:30px;">
                               Add New User/Accountants
                              </div>
                   </a>
                </div> 

                <div class="panel panel-default"  style="width: 150px; height: 140px; float: left; margin-left: 40px;">
                  <a href="showallusers">  <div class=" panel-body">
                               <img src="{{ url('/') }}/icons/022-lock.png" style="width: 60px; height: 60px; margin-bottom:10px; margin-left: 20px;   margin-right:30px;">
                               Show All User
                              </div>
                   </a>
                </div>

                <div class="panel panel-default"  style="width: 150px; height: 140px; float: left; margin-left: 40px;">
                  <a href="showalladmins">  <div class=" panel-body">
                               <img src="{{ url('/') }}/icons/037-users.png" style="width: 60px; height: 60px; margin-bottom:10px; margin-left: 20px;   margin-right:30px;">
                               Show All Admins
                              </div>
                   </a>
                </div> 

                <div class="panel panel-default"  style="width: 150px; height: 140px; float: left; margin-left: 40px;">
                  <a href="rept-password-reset">  <div class=" panel-body">
                               <img src="{{ url('/') }}/icons/050-settings.png" style="width: 60px; height: 60px; margin-bottom:10px; margin-left: 20px;   margin-right:30px;">
                               Change Reciept Control Password
                              </div>
                   </a>
                </div> 

            </div>
        </div>




    </div>



@endsection


@extends('layouts.app')
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
@section('content')

 <form method="post" action="{{ url('/') }}/session-create-submit">
 {{ csrf_field() }}
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading"></div>

                <div class="panel-body">

                	<div class="form-group col-md-6">
                    <label class="control-label" for="cr_session">Session YYYY-YYYY format Only*</label>
                    <input type="text" class="form-control"  id="cr_session" name="cr_session" minlength="9" maxlength="9"  autofocus required>
                    </div>

                    <div class="form-group col-md-6">
                    <label class="control-label" for="comment">Comment (If any)*</label>
                    <input type="text" class="form-control"  id="comment" name="comment" value="N.A." autofocus required>
                    </div>

                	
                <button type="submit" class="btn btn-success">Create New Session</button>
                </div>
            </div>         
        </div>
    </div>
</div>

</form>          

@endsection

@extends('layouts.app2')
@section('content')



<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading"> Number of Sessions </div>

                <div class="panel-body">
     
                <table  class="table table-bordered" id="users-table">
                    <thead>
                        <th>ID</th>
                        <th>Session Period</th>
                        <th>Status</th>
                        <th>Comment</th>
                        <th>Action</th>
                        <!-- <th>Action</th> -->
                    </thead>
                    <tbody>
                    	@foreach ($data as $valueholder)
                        	<tr>
                        		<td> {{ $valueholder->id }} </td>
                        		<td> {{ $valueholder->session_period }} </td>
                        		<td> {{ $valueholder->status }} </td>
                        		<td> {{ $valueholder->description }} </td>
                        		<td> <a href="session-activate/{{ $valueholder->id }}">Activate</a> </td>
                        		<!-- <td> <a href="session-delete/{{ $valueholder->id }}">Delete</a> </td> -->
                        	</tr>
                    	@endforeach
                    </tbody>

                    <tfoot>
                        <td colspan="6"><i> Please Note You Should Only Use YYYY-YYYY format. </i> </td>
                    </tfoot>
                </table>
       

                </div>
            </div>         
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function()
    {
          $("tr:even").css("background-color","#e5f9f9");

    });
</script>

@endsection
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-heading">Add College Details</div>

                <div class="panel-body">
                    

{!! Form::open(array('route' => 'f.usersubmitform','files'=>true)) !!}
            <div class="row">
                <div class="col-md-8">
                    {!! Form::label('class_section', 'class_section') !!}
                    {!! Form::text('class_section', 'class_section', array('class' => 'form-control')) !!}

                    {!! Form::label('description', 'description') !!}
                    {!! Form::text('description', 'N.A.', array('class' => 'form-control')) !!}

                    <button type="submit" class="btn btn-success">Upload</button>
                </div>
            </div>
    {!! Form::close() !!}






                </div>
            </div>         
        </div>
    </div>
</div>
@endsection
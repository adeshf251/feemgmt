@extends('layouts.app')

@section('content')
<script src="{{ url('/') }}/public/js/jquery-1.12.2.js"></script>

 {{ csrf_field() }}
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading">Discount Provider Registration</div>

                <div class="panel-body">
<div class="form-group">
		<label for="feetype">Select the Fee type</label>
		<select name="feetype" id="feetype"  class="custom-select form-control" >
		@foreach ($feetypes as  $k => $element)
			@foreach ($element as  $e)
				<option value="{{$e}}">{{$e}}</option> 
			@endforeach
		@endforeach
		</select>
 </div>

 <div class="form-group">
		<label for="class_section">Select the Class Applicable for</label>
		<select name="class_section" id="class_section"  class="custom-select form-control" >
		@foreach ($classmgmts as  $k => $element)
			@foreach ($element as  $e)
				<option value="{{$e}}">{{$e}}</option> 
			@endforeach
		@endforeach
		</select>
 </div>

 <div class="form-group">
	    <label class="control-label"  for="amount">Amount (Required)*</label>
	    <input type="text" class="form-control"  id="amount" name="amount" value="0" autofocus required>
	    </div>


 <button type="submit" id="submit" class="btn btn-success col-md-3">Register the fee amount</button>
     <div id="processing"></div>
                </div>
            </div>         
        </div>
    </div>
</div>








<script>
$(document).ready(function(){

$("#submit").click(function()
                     {  
                     $("#processing").html('<img src="/public/images/ajax-loader.gif" style="height : 20px; margin-top : 10px;" />');
                     var feetype = document.getElementById("feetype").value;
                     var class_section = document.getElementById("class_section").value;
                     var amount = document.getElementById("amount").value;
                     $.ajax({
                             type : 'GET',
                             url  : 'fee-value-registration-classwise-submit',
                             data : {'feetype': feetype , 'class_section': class_section , 'amount': amount},
                             success : function(data)
                                            {
                                             $("#processing").html('<img src="/public/images/available.png" style="height : 20px; margin-top : 10px;" />');
                                             alert(data);
                                             }
                           });
                    });                
});
</script>

@endsection

 
	            
	             
	            
	   
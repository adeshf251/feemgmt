@extends('layouts.app')
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
@section('content')

 <form method="post" action="{{ url('/') }}/payfees-confirm-cheque-payment-commit">
 {{ csrf_field() }}
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading">{{ $form_heading }}</div>

                <div class="panel-body">
                    
                    <!-- ******************** starting body ******************* -->
                    <div style="width: 100%; height: auto;">







                        <!-- ******************** section 1a ******************* -->
                        <div style="width: 65%; height: 100px; padding: 10px; float: left;">
                            <table class="table table-bordered" id="users-table">
                             <tr> <th>Admission No</th> <th>Student Name</th> <th>Class -Section</th> </tr>
                             <tr> <th>{{$admission_no}}</th> <th>{{$student_name}}</th> <th>{{$class_section}}</th> </tr>
                            </table>
                            <input type="hidden" name="admission_no" id="admission_no" value="{{$admission_no}}"/>
                            <input type="hidden" name="student_name" id="student_name" value="{{$student_name}}"/>
                            <input type="hidden" name="class_section" id="class_section" value="{{$class_section}}"/>
                            <input type="hidden" name="parent_name" id="parent_name" value="{{$parent_name}}"/>
                        </div>
                        <!-- #################### Ending Section 1a ################# -->



                        <!-- ******************** section 1b ******************* -->
                        <div style="width: 35%; height: 100px; padding: 10px; float: left;">
                            <table class="table table-bordered" id="users-table">
                            <tr> <th>Months Applicable :</th>
                            <tr><td>
                            @foreach ($payingmonths as $element)
                               {{$element}} , <input type="hidden" name="months[]" id="months[]" value="{{$element}}"/>
                            @endforeach
                            
                            @if (!empty ($once_required_fee_array) )
                                    @foreach ($once_required_fee_array as $element)
                                        {{$element}} <input type="hidden" name="months[]" id="months[]" value="{{$element}}"/>
                                    @endforeach
                            @endif
                            </td></tr>
                            </table>
                        </div>
                        <!-- #################### Ending Section 1b ################# -->









                        <!-- ******************** section 2 ******************* -->
                        <div style="width: 50%; height: auto; padding: 10px; float: left;">
                            <h3>Fee Amounts :</h3>
                            <table class="table table-bordered" id="users-table">
                            @foreach ($feetypeslist as $key => $e)
                              <tr> <td>{{$e}}</td><td>{{$amount[$key]}}</td></tr> 
                              <input type="hidden" name="feetype[]" id="feetype[]" value="{{$e}}"/>
                              <input type="hidden" name="amount[]" id="amount[]" value="{{$amount[$key]}}"/>
                            @endforeach
                            
                             <tr> <td><h4>Total Payable Amount [<span style="font-weight: bold; color: red;"> {{$total}} </span>]</h4></td>
                             <td><input type="text" name="tt" id="tt" style="color: green; font-weight: bold;"  value="{{$total}}"  class="form-control"  disabled="disabled" /></td></tr>

                            <input type="hidden" name="total" id="total" value="{{$total}}"/>
                            <input type="hidden" name="latefee" id="latefee" value="{{$latefee}}"/>
                            </table>
                        </div>
                        <!-- #################### Ending Section 2 ################# -->








                        <!-- ******************** section 3 ******************* -->
                        <div style="width: 50%; height: auto; padding: 10px; float: left;">
                        <h3>DD Status :</h3>
                            <div class="form-group{{ $errors->has('discountamount') ? ' has-error' : '' }}">
                            <label for="discountamount" class="col-md-6 control-label">Discount</label>

                            <div class="col-md-6" style="padding-bottom: 6px;">
                                <input id="discountamount" type="text" class="form-control" name="discountamount" style="color: brown; font-weight: bold;"  value="{{ old('discountamount') }}" autofocus>

                                @if ($errors->has('discountamount'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('discountamount') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('dd_no') ? ' has-error' : '' }}">
                            <label for="dd_no" class="col-md-6 control-label">DD No*</label>

                            <div class="col-md-6" style="padding-bottom: 6px;">
                                <input id="dd_no" type="text" class="form-control" name="dd_no" value="{{ old('dd_no') }}" autofocus required="required">

                                @if ($errors->has('dd_no'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('dd_no') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('dd_date') ? ' has-error' : '' }}">
                            <label for="dd_date" class="col-md-6 control-label">DD Date*</label>

                            <div class="col-md-6" style="padding-bottom: 6px;">
                                <input id="dd_date" type="Date" class="form-control" name="dd_date" value="{{ old('dd_date') }}" autofocus required="required">

                                @if ($errors->has('dd_date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('dd_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('dd_bank') ? ' has-error' : '' }}">
                            <label for="dd_bank" class="col-md-6 control-label">DD Bank*</label>

                            <div class="col-md-6" style="padding-bottom: 6px;">
                                <input id="dd_bank" type="text" class="form-control" name="dd_bank" value="{{ old('dd_bank') }}" autofocus required="required">

                                @if ($errors->has('dd_bank'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('dd_bank') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('dd_branch') ? ' has-error' : '' }}">
                            <label for="dd_branch" class="col-md-6 control-label">DD Branch*</label>

                            <div class="col-md-6" style="padding-bottom: 6px;">
                                <input id="dd_branch" type="text" class="form-control" name="dd_branch" value="{{ old('dd_branch') }}" autofocus required="required">

                                @if ($errors->has('dd_branch'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('dd_branch') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        

                        <div  style="color: green; background-color: #f1f1f1" class="form-group{{ $errors->has('dd_amount') ? ' has-error' : '' }}">
                            <label for="dd_amount" class="col-md-6 control-label">DD Amount (In case different, Optional)</label>

                            <div class="col-md-6" style="padding-bottom: 6px;">
                                <input id="dd_amount" type="text" class="form-control" style="color: green; font-weight: bold;" name="dd_amount" value="{{ old('dd_amount') }}" autofocus >

                                @if ($errors->has('dd_amount'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('dd_amount') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div>
                            

                            <div style="padding-top: 6px;">
                                <button type="submit" class="btn btn-success" style="width: 100%;">Confirm and Pay Now</button>
                            </div>
                        </div>

                             
                        </div>
                        <!-- #################### Ending Section 3 ################# -->

                            



                            <?php  $pm = base64_encode(serialize($payingmonths)); ?>
                           <input type="hidden" name="pm" value="<?php echo $pm; ?>">

                           <?php  $ftl = base64_encode(serialize($feetypeslist)); ?>
                           <input type="hidden" name="ftl" value="<?php echo $ftl; ?>">

                           <?php  $amt = base64_encode(serialize($amount)); ?>
                           <input type="hidden" name="amt" value="<?php echo $amt; ?>">

                              <?php  $onetimefee = base64_encode(serialize($once_required_fee_array)); ?>
                           <input type="hidden" name="once_required_fee_array" value="<?php echo $onetimefee; ?>">

                    </div>
                    <!-- #################### Ending body ################# -->

                </div>
            </div>         
        </div>
    </div>
</div>

</form>          



<script type="text/javascript">
  $(document).ready(function()
  {
        document.getElementById("discountamount").onkeyup = function()
        {
            myFunction();

        };

            function myFunction()
                {
                var x = document.getElementById("discountamount");
                var totalhidden = document.getElementById("total");
                var totaldisplay = document.getElementById("tt");
                var chequevalue = document.getElementById("dd_amount");
                var total = totalhidden.value - x.value;
                totaldisplay.value = total;
                chequevalue.value = total;
                
                }   
    });    
</script>

@endsection
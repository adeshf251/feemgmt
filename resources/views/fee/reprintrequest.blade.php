@extends('layouts.app2')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading"> &#9851; {{ $form_heading }}</div>

                <div class="panel-body">
<?php $x; ?>
 @isset($data)
 <div id="colvis"></div>
        <table class="table table-bordered" id="users-table">
        <thead>
            <tr>
              <th>Reciept No</th>
              <th>Admission No</th>
              <th>Student Name</th>
              <th>Class-Section</th>
              <th>Parent's Name</th>
              <th>Fee Months</th>
              <th>Total Paid</th>
              <th style="min-width:100px;">Date Time</th>
              <th>Select</th>
            </tr>
        </thead>
         
         <tbody>
            @foreach ($data as $element)
              <tr>
                  @foreach ($element as $key => $val)
                      
                        @if($key == 'rept_no' )
                          <?php $x=$val; ?>
                        @endif
                      
                  
                           <td>  <h4> {{ $val }} </h4> </td> 
                
                      
                  @endforeach
                  <td> <a href="{{ url('/') }}/Reprint-fee-Reciept/{{$x}}"> <button  class="btn btn-success">Reprint Now &#10148;</button> </a> </td>
              </tr>
            @endforeach
         </tbody>
          
          </table>
        @endisset
             
                </div>
            </div>         
        </div>
    </div>
</div>
@endsection


@extends('layouts.app')
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
@section('content')

 <form method="post" action="{{ url('/') }}/payfees-confirm-payment" id="formPayment">
 {{ csrf_field() }}
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading">{{ $form_heading }}</div>

                <div class="panel-body">
      

        @isset($data)
        <table class="table table-bordered" id="users-table">
         <tr> <th>Student Name</th> <th>Class -Section</th> <th>Parent Name </th> <th>Date of Birth</th> <th>Admission No</th> <th>Transport Route</th></tr>
          @foreach ($data as $element)
              <tr></br>
                  @foreach ($element as $key => $val)
                      
                        @if(($key != 'admission_no' ) && ($key != 'father_name' ) && ($key != 'transport_route' ) )
                          <td>  <h4> {{ $val }} </h4> <input type="hidden" id="{{$key}}" name="{{$key}}" value="{{$val}}"></input> </td>
                        @endif

                        @if($key == 'father_name' )
                          <td>  <h4> {{ $val }} </h4> <input type="hidden" id="parent_name" name="parent_name" value="{{$val}}"></input> </td>
                        @endif
                      
                        @if($key == 'admission_no' )
                           <td>  <h4> {{ $val }} </h4> <input type="hidden" id="{{$key}}" name="{{$key}}" value="{{$val}}"></input> </td> 
                        @endif 

                        @if($key == 'transport_route' )
                           <td> <h4> {{ $val }} </h4> </td> 
                        @endif 

                  @endforeach
              </tr>
          @endforeach
          </table>
        @endisset

        <table class="table table-bordered" id="users-table">

<?php   
    $latefee = 50; 
    $date2 = date("d");
    if($date2 <= 15 )
    {
      $latefee = 0;
    }

?>

        <tr>
        <div class="col-md-12">
        <div class="col-md-4">
        <label> Late Fee (Amount):</label>
        </div>
        <div class="col-md-8">
        <input type="number" name="latefee" class="form-control" value="{{$latefee}}" /> <br>
        </div>
        </div><br>
        </tr>

        
        <tr> <label> Select Months:</label>
        @if($april == 0)
         <td> <input type="checkbox" id="april" name="april"> april </input>  </td>
        @endif
        @if($may == 0)
         <td> <input type="checkbox" id="may" name="may"> may </input>  </td>
        @endif
        @if($june == 0)
         <td> <input type="checkbox" id="june" name="june"> june </input>  </td>
        @endif
        @if($july == 0)
        <td>  <input type="checkbox" id="july" name="july"> july </input>  </td>
        @endif
        @if($aug == 0)
         <td> <input type="checkbox" id="aug" name="aug"> aug </input>  </td>
        @endif
        @if($sep == 0)
        <td>  <input type="checkbox" id="sep" name="sep"> sept </input>  </td>
        @endif
        @if($oct == 0)
         <td> <input type="checkbox" id="oct" name="oct"> oct </input>  </td>
        @endif
        @if($nov == 0)
         <td> <input type="checkbox" id="nov" name="nov"> nov </input>  </td>
        @endif
        @if($dec == 0)
         <td> <input type="checkbox" id="dec" name="dec"> dec </input>  </td>
        @endif
        @if($jan == 0)
         <td> <input type="checkbox" id="jan" name="jan"> jan </input>  </td>
        @endif
        @if($feb == 0)
         <td> <input type="checkbox" id="feb" name="feb"> feb </input>  </td>
        @endif
        @if($march == 0)
         <td> <input type="checkbox" id="march" name="march"> march </input>  </td>
        @endif
        </tr>
        </table>
        
        
        
<table class="table table-bordered" id="users-table">
  @foreach ($once_required as $key => $element)
    <tr>
      <td> <label> Include This once required Fee :</label> </td>
      <td> <input type="checkbox" id="{{$element}}" name="{{$element}}"> {{$element}} </input>  </td>
    </tr>
  @endforeach
</table>
      
   <?php  $once_required_fee_array = base64_encode(serialize($once_required)); ?>
   <input type="hidden" name="once_required_fee_array" value="<?php echo $once_required_fee_array; ?>">
     
     <br>
     <button type="submit" class="btn btn-success">Cash Payment</button>

      <button type="button" id="cheque-payment" class="btn btn-danger">Cheque Payment</button>

      <button type="button" id="dd-payment" class="btn btn-warning">DD Payment</button>
     
                </div>
            </div>         
        </div>
    </div>
</div>

</form>          
<script src="/public/js/jquery.min.js"></script>

<script type="text/javascript">
  $(document).ready(function()
  {
    
    $( "#cheque-payment" ).click(function() {
            $("#formPayment").attr('action', "{{ url('/') }}/payfees-confirm-cheque-payment" );
            $("#formPayment").submit();
            e.preventDefault();
      });

    $( "#dd-payment" ).click(function() {
            $("#formPayment").attr('action', "{{ url('/') }}/payfees-confirm-dd-payment" );
            $("#formPayment").submit();
            e.preventDefault();
      });
  });
</script>





@endsection
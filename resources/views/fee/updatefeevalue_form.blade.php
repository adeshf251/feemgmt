@extends('layouts.app')

@section('content')
<div class="container" style="background-color: #f1f1f1;">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-heading">{{ $form_heading }}</div>

                <div class="panel-body">


                <?php $clr=1; $cons = 1; ?>
  <table class="table table-bordered">
       <tr><th> ID </th><th> Fee Type </th><th>Class Applicable</th><th>Fee Value</th><th> </th></tr>
   @foreach ($data as $key => $element)  <tr <?php if(($clr % 2)==0) {echo 'style="background-color: #f1f1f1;"';  } $clr+=1;  ?> >
        @foreach ($element as $k => $v)
            @if($k=='id')
               <td> <form method="post" action="{{ url('/') }}/update-fee-value/{{$v}}"> {{ csrf_field() }} <input type="text"  class="form-control" name="fee_type" value="{{$v}}"  disabled="disabled">  </td>
            @endif

            @if($k=='fee_type')
             <td>   <input type="text"  class="form-control" name="fee_type" value="{{$v}}" disabled="disabled"> </td>
            @endif

            @if($k=='class_section')
              <td>  <input type="text"  class="form-control" name="class_section" value="{{$v}}"  disabled="disabled"> </td>
            @endif

            @if($k=='amount')
               <td> <input type="number"  class="form-control" name="amount" value="{{$v}}"> </td>
            @endif
        @endforeach
            <td> <input type="submit"  class="btn btn-success" name="submit" value="Update Now">  </td>
           </form>
           </tr>
   @endforeach

</br>
   </table>
             
                </div>
            </div>         
        </div>
    </div>
</div>
@endsection
@extends('layouts.app')


@section('content')
<script language="javascript" type="text/javascript">
    //this code handles the F5/Ctrl+F5/Ctrl+R
    document.onkeydown = checkKeycode
    function checkKeycode(e) {
        var keycode;
        if (window.event)
            keycode = window.event.keyCode;
        else if (e)
            keycode = e.which;

        // Mozilla firefox
        if ($.browser.mozilla) {
            if (keycode == 116 ||(e.ctrlKey && keycode == 82)) {
                if (e.preventDefault)
                {
                    e.preventDefault();
                    e.stopPropagation();
                }
            }
        } 
        // IE
        else if ($.browser.msie) {
            if (keycode == 116 || (window.event.ctrlKey && keycode == 82)) {
                window.event.returnValue = false;
                window.event.keyCode = 0;
                window.status = "Refresh is disabled";
            }
        }
    }
</script>

<style type="text/css">
	@page {
  size: A4;
  margin: 0;
}
@media print {
  html, body {
    width: 297mm;
    height: 210mm;
  }
  /* ... the rest of the rules   w= 210, h= 297 ... */
}
</style> 

  <input type="button" onclick="printDiv('pageprintarea')" value="Print Reciept" />
<br>


<div id="pageprintarea" style="width : 297mm; height: 210mm; padding-right: 10px; padding-left: 10px; background-color: white; " >
<!--- part 1 of print area -->
 <div style="width: 45%; float:left; border: 2px solid black; padding: 5px;"> 
    <div style="width: 100%"> <p style="text-align: center;"> Student Copy {{Session('valid_period')}} </p>  <p style="text-align: right;"> Tel: 7533850888</p>
    </div>
    <div>
        <div style="width: 80px; height :80px; float: left;"><img src="{{ url('/') }}/images/logo.jpg" style="height: 100%"></div>
        <div style="width: ; height :80px; float : left; text-align: left;">
          <h4 style="margin-left: 65px;"> <strong> Dellmond International School </strong></h4>
          <h5 style="margin-left: 45px;"> Dehradun Road Near Fatehpur Thana Chhutmalpur </h5> </div>
    </div>
    <div>
        <!-- ********************* student details ********************** -->
         <table class="table " id="users-table" style="font-size: 12px; text-transform: capitalize; padding: 0px; margin: 0px;">
        <tr>  
          <td>Rept No</td>
          <td> @foreach ($reciept as $e)
            {{$e}},
          @endforeach 
          </td>
          <td style=" border-left: 1px solid #ddd;"> Adm No : &nbsp; &nbsp;  {{$admission_no}} </td>
          <td style=" border-left: 1px solid #ddd;"> class : &nbsp; &nbsp; {{$class_section}}</td>
        </tr>
        </table>

        <table class="table " id="users-table" style="font-size: 12px; text-transform: capitalize;  padding: 0px; margin: 0px;">
        <tr>
          <td>Stu. Name</td> <td> {{$student_name}} </td>
          <td style=" border-left: 1px solid #ddd;"> Parent N.</td> <td> {{$parent_name}}</td>
          <td style=" border-left: 1px solid #ddd;"> Pay : </td> <td> {{$payment_mode}}</td>
         


        </tr>
        </table>
        <table class="table " id="users-table" style="font-size: 12px; text-transform: capitalize;  padding: 0px; margin: 0px;">
        <tr>
          <td>Fee For :</td> 
          <td > @foreach ($payingmonths as $e)
            {{$e}},
          @endforeach </td>
          <td style=" border-left: 1px solid #ddd;"> Date : </td> <td> {{ substr($datetime, 0, 11) }}</td>
        </tr>
      </table>
      <!-- ################### student details end ##################### -->
    </div>
    <div>
         <table class="table " id="users-table" style="font-size: 12px; text-transform: capitalize;">
          
        <tr>  <td><STRONG> PARTICULARS </STRONG></td> 
          <td><STRONG> AMOUNT  </STRONG></td>  </tr>
        
        
        @foreach ($feetypeslist as $key => $e)
              <tr> <td>{{$e}}</td><td>{{$amount[$key]}}</td></tr> 
             @endforeach

             @if($discount>0)
             <tr> <td> Adjustment Given </td><td>{{$discount}}</td></tr>
             @endif

        <tr>  <td><span style="font-weight: bold; font-size: 15px;"> TOTAL AMOUNT </span> </td> <td><span style="font-weight: bold; font-size: 15px;"> {{$total}} </span> </td> </tr>
        
      </table>
    </div>

    <div  style="width: 100%; height :40px; float: left;">
    </div>

    <div>
       <p style="float: right; margin-right: 120px ">  Authorized Signature  </p>
    </div>
    
    <div id="blank-space-gap" style="width: 100%; height :20px; float: left;">
    </div>
     
    @if ($payment_mode != 'cash')
      <div>
       <p style="float: left; margin-bottom: : 0px "> <span style="font-size: 10px;"> Payment is comfirmed only if Cheque/DD is cleared. </span> </p>
      </div>
    @endif


</div>

<!--- part 2 ie gaps of print area -->
   <div style="width: 5%; height:180mm; float:left">   </div>
  <div style="width: 5%; height:180mm; float:left"> 
  <?php for ($i = 0; $i < 34 ; $i++){
    echo '*<br>';
  }
   ?>
  </div>


<!--- part 3 ie college copy of print area -->
 <div style="width: 45%; float:left; border: 2px solid black;  padding: 5px;"> 
 <div style="width: 100%"><p style="text-align: center;"> School Copy {{Session('valid_period')}}</p>  <p style="text-align: right;"> Tel: 7533850888</p>
    </div>
    <div>
        <div style="width: 80px; height :80px; float: left;"><img src="{{ url('/') }}/images/logo.jpg" style="height: 100%"></div>
        <div style="width: ; height :80px; float : left; text-align: left;">
          <h4 style="margin-left: 65px;"> <strong> Dellmond International School </strong></h4>
          <h5 style="margin-left: 45px;"> Dehradun Road Near Fatehpur Thana Chhutmalpur</h5> </div>
    </div>
    <div>
        <!-- ********************* student details ********************** -->
         <table class="table " id="users-table" style="font-size: 12px; text-transform: capitalize; padding: 0px; margin: 0px;">
        <tr>  
          <td>Rept No</td>
          <td> @foreach ($reciept as $e)
            {{$e}},
          @endforeach 
          </td>
          <td style=" border-left: 1px solid #ddd;"> Adm No : &nbsp; &nbsp;  {{$admission_no}} </td>
          <td style=" border-left: 1px solid #ddd;"> class : &nbsp; &nbsp; {{$class_section}}</td>

        </tr>
        </table>

        <table class="table " id="users-table" style="font-size: 12px; text-transform: capitalize;  padding: 0px; margin: 0px;">
        <tr>
          <td>Stu. Name</td> <td> {{$student_name}} </td>
          <td style=" border-left: 1px solid #ddd;"> Parent N.</td> <td> {{$parent_name}}</td>
          <td style=" border-left: 1px solid #ddd;"> Pay :</td><td>{{$payment_mode}}</td>

        </tr>
        </table>
        <table class="table " id="users-table" style="font-size: 12px; text-transform: capitalize;  padding: 0px; margin: 0px;">
        <tr>
          <td>Fee For :</td> 
          <td > @foreach ($payingmonths as $e)
            {{$e}},
          @endforeach </td>
          <td style=" border-left: 1px solid #ddd;"> Date time.</td> <td> {{ substr($datetime, 0, 11) }}</td>
        </tr>
      </table>
      <!-- ################### student details end ##################### -->
    </div>
    <div>
         <table class="table " id="users-table" style="font-size: 12px; text-transform: capitalize;">
          
        <tr>  <td><STRONG> PARTICULARS </STRONG></td> 
          <td><STRONG> AMOUNT  </STRONG></td>  </tr>
        
        
        @foreach ($feetypeslist as $key => $e)
              <tr> <td>{{$e}}</td><td>{{$amount[$key]}}</td></tr> 
             @endforeach

             @if($discount>0)
             <tr> <td> Adjustment Given </td><td>{{$discount}}</td></tr>
             @endif

        <tr>  <td><span style="font-weight: bold; font-size: 15px;"> TOTAL AMOUNT </span> </td> <td><span style="font-weight: bold; font-size: 15px;"> {{$total}} </span> </td> </tr>
        
      </table>
    </div>

    <div  style="width: 100%; height :40px; float: left;">
    </div>

    <div>
       <p style="float: right; margin-right: 120px ">  Authorized Signature  </p>
    </div>

     
    <div id="blank-space-gap" style="width: 100%; height :20px; float: left;">
    </div>
     
    @if ($payment_mode != 'cash')
      <div>
       <p style="float: left; margin-bottom: : 0px "> <span style="font-size: 10px;"> Payment is comfirmed only if Cheque/DD is cleared. </span> </p>
      </div>
    @endif

    
</div>

</div>




<script>
function myFunction() {
    window.print();
}
function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
</script>
@endsection
@extends('layouts.app')

@section('content')

<script type="text/javascript">
  $(document).ready(function() { 
    $("table") 
    .tablesorter({widthFixed: true, widgets: ['zebra']}) 
    .tablesorterPager({container: $("#pager")}); 
}); 
</script>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading">{{ $form_heading }}</div>

                <div class="panel-body">
                 

        @isset($data)
        <table class="table table-bordered" id="table">
         <tr> <th>Student Name</th> <th>Class -Section</th> <th>Parent Name </th> <th>Date of Birth</th> <th>Admission No</th><th>Add student</th></tr>
          @foreach ($data as $element)
              <tr>
                  @foreach ($element as $key => $val)
                       @if($key == 'admission_no')  <td>  <h4> {{ $val }} </h4> </td><td>  <a href ="{{ $val }}">aaaaa </a></td>  @endif
                       @if($key != 'admission_no')  <td>  <h4> {{ $val }} </h4> </td>  @endif
                   @endforeach
              </tr>
          @endforeach
          </table>
        @endisset

                </div>
            </div>         
        </div>
    </div>
</div>
@endsection



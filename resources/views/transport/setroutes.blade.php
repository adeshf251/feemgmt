@extends('layouts.app')

@section('content')
<form method="post" action="{{ url('/') }}/insert-transport-routes-request-submit">
 {{ csrf_field() }}
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading">New Transport Route Registration</div>

                <div class="panel-body">
	    
	    <div class="form-group col-md-6">
	    <label class="control-label" for="route_name">Route Name*</label>
	    <input type="text" class="form-control"  id="route_name" name="route_name" autofocus required>
	    </div>

	    <div class="form-group col-md-6">
	    <label class="control-label" for="source">Source*</label>
	    <input type="text" class="form-control" value="school" id="source" name="source" autofocus required>
	    </div>


	    <div class="form-group col-md-6">
	    <label class="control-label"  for="destination">Destination</label>
	    <input type="text" class="form-control"  id="destination" name="destination" autofocus required>
	    </div>

	    <div class="form-group col-md-6">
	    <label class="control-label"  for="amount">Amount</label>
	    <input type="number" class="form-control"  id="amount" name="amount" autofocus required>
	    </div>


	    <div class="form-group col-md-6">
	    <label class="control-label"  for="remark">Remark</label>
	    <input type="text" class="form-control" value="N.A." id="remark" name="remark" autofocus required>
	    </div>
     
	    <input type="submit" name="submit" class="btn" />
                </div>
            </div>         
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading">Transport Routes List</div>

                <div class="panel-body">
       <div id="colvis"></div>
        <table class="table table-bordered" id="users-table">
         <tr> <th>Route Name</th> <th>Source</th> <th>Destination </th> <th>Amount</th> <th>Remark</th><th>Transport ID (For Allotment)</th><th>Delete</th><th>Update</th></tr>
          @foreach ($data as $element)
          <tr>
               <form method="post" action="{{ url('/') }}/insert-transport-routes-request-submit"> 
 								{{ csrf_field() }} 
                  @foreach ($element as $key => $val)
                       
                       @if($key != 'id')  <td>  <input type="text" class="form-control"  id="{{$key}}" name="{{$key}}" value="{{$val}}"  autofocus required>  </td>
                       @endif
                       @if($key == 'id')  
	                       
	                       <td>  {{ $val }} </td> 

                          <td>  <a href ="{{ url('/') }}/delete-transport-routes/{{ $val }}"><button class="btn btn-danger">Delete</button> </a></td>  
                       @endif
                       
                   @endforeach
              <td>   <input type="submit" name="submit" class="btn btn-success" value="Update"> </td></form> </tr> 
          @endforeach
          </table>
       

                </div>
            </div>         
        </div>
    </div>
</div>
@endsection



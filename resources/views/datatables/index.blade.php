@extends('a')

@section('content')
<div id="colvis"></div>
    <table class="table table-bordered" id="users-table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Email</th>
                <th>Created At</th>
                <th>Updated At</th>
            </tr>
        </thead>
    </table>
@stop

@push('scripts')
<script type="text/javascript">
    $(document).ready(function(){
    $('.table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('get.posts') !!}',

        //You will have to give the column names of the table.
        columns: [
            { data: 'admission_no', name: 'admission_no' },
            { data: 'student_name', name: 'student_name' },
            { data: 'class_section', name: 'class_section' },

        ]
    });
});
</script>
@endpush
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-heading">{{ $form_heading }}</div>

                <div class="panel-body">
<form method="post" action="{{ url('/') }}/classwise-student-form-submit">
 {{ csrf_field() }}
<select name="classname" class="form-control">
@foreach ($optionsvalue as $element)
	@foreach ($element as $e)
		<option value="{{ $e }}"> {{ $e }} </option>
	@endforeach
@endforeach
</select> <br>
<input type="submit" name="submit" class="btn btn-info" value="Search Students">
</form>                 


                </div>
            </div>         
        </div>
    </div>
</div>
@endsection
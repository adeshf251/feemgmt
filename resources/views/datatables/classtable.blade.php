@extends('a')

@section('content')
<div id="colvis"></div>
    <table class="table table-bordered" id="users-table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Email</th>
                <th>Created At</th>
                <th>Updated At</th>
            </tr>
        </thead>
    </table>
@stop

@push('scripts')
<script>
$(function() {
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('classes.data') !!}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'class_section', name: 'class_section' },
            { data: 'description', name: 'description' },
        ]
    });
});
</script>
@endpush
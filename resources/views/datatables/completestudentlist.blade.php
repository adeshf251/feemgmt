

@extends('layouts.app2')

@section('content')
<link href="{{ url('/') }}/public/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ url('/') }}/public/plugins/datatables/jquery.dataTables.min.css">
<script src="{{ url('/') }}/public/plugins/datatables/jquery.js"></script>
    <script src="{{ url('/') }}/public/plugins/datatables/dataTables.bootstrap.min.js"></script>
     <script src="{{ url('/') }}/public/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ url('/') }}/public/plugins/datatables/bootstrap.min.js"></script>


<div class="container" style="background-color: white;">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div id="colvis"></div>
        <table id="users-table">
        <tr> <tr></tr> <td>
			<form method="get" action="{{ url('/') }}/complete-student-list-export-xlsx"><input type="submit" name="submit" value="export in xlsx" class="btn btn-info"></form>
			</td>
			<td>
			<form method="get" action="{{ url('/') }}/complete-student-list-export-csv"><input type="submit" name="submit" value="export in csv" class="btn btn-success"></form>
			</td></tr>
		</table><br>
    <table class="table table-bordered" id="admission_no">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Class-Section</th>
                <th>Father Name</th>
                <th>Mobile</th>
                <th>Date of Birth</th>
            </tr>
        </thead>
    </table>
    </div>
    </div>
    </div>
@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function(){ 
    $('.table').DataTable({
        processing: true,
        serverSide: true,
        ajax: 'complete-student-list-ajax',

        //You will have to give the column names of the table.
        columns: [
            { data: 'admission_no', name: 'admission_no' },
            { data: 'student_name', name: 'student_name' },
            { data: 'class_section', name: 'class_section' },
            { data: 'father_name', name: 'father_name' },
            { data: 'dob', name: 'dob' },

        ]
    })
});
</script>
@endpush
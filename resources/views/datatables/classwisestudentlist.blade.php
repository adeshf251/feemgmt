@extends('layouts.app2')

@section('content')

<?php $sno=0; ?>

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1"><div class="panel panel-default">

                

                <div class="panel-body" style="background-color: white;"><div class="panel-heading">Student's List</div>

<!-- printable area starts -->
<div id="colvis"></div>
<div id="pageprintarea">
        
<table class="table table-bordered" id="admission_no">
        <thead>
            <tr>
                <th>S No</th>
                <th>Id</th>
                <th>Name</th>
                <th>Class-Section</th>
                <th>Parent Name</th>
                <th>Mobile</th>
                <th>Date of Birth</th>
                <th id="non-printable">Delete</th>
            </tr>
        </thead>
<?php $idcatch; ?>
@foreach ($data as $element) <tr>

<td> <?php echo $sno = $sno + 1 ; ?> </td>
    @foreach ($element as $key => $e)
        @if ($key == 'admission_no')
            <?php $idcatch = $e; ?>
        @endif
        <td> {{$e}} </td>
    @endforeach <td id="non-printable"> <a href="{{ url('/') }}/student-delete/{{$idcatch}}" > <button type="button" class="btn btn-success">Delete  &#10006;</button></a> </td> </tr>
@endforeach

    </table>
    </div> 
    <!-- printable area ends -->
    </div>
    </div>
    </div>
</div>
</div>



<script type="text/javascript">
    $(document).ready(function(){ 
        $('#overlay').fadeOut();
    $('.table').DataTable({
        "paging": true,
            "lengthChange": true,
            "dom": 'Bfrtip',
            "searching": true,
            "responsive": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
             "buttons": ['copyHtml5', 'excelHtml5', 'csvHtml5', 'pdfHtml5' , 'pageLength', ],
             
             "lengthMenu": [
                                [ 10, 25, 50, -1 ],
                                [ '10 rows', '25 rows', '50 rows', 'Show all' ]
                            ],
           

    })
});
</script>

@endsection



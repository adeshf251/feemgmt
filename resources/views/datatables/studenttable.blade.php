@extends('layouts.app')

@section('content2')
<div id="colvis"></div>
    <table class="table table-bordered" id="users-table">
        <thead>
            <tr>
                <th>Admission No</th>
                <th>Student Name</th>
                <th>Class-Section</th>
            </tr>
        </thead>
    </table>
@stop

@push('scripts')
<script type="text/javascript">
    $(document).ready(function(){
    $('.table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('student.data') !!}',

        //You will have to give the column names of the table.
        columns: [
            { data: 'admission_no', name: 'admission_no' },
            { data: 'student_name', name: 'student_name' },
            { data: 'class_section', name: 'class_section' },
        ]
    });
});
</script>
@endpush
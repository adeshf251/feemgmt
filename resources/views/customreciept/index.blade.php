@extends('layouts.app2')

@section('content')


<div class="container" >
    <div class="row" >
        <div class=""><div class="panel panel-default">

                

<div class="panel-body" style="overflow:scroll; background-color: white;"><div class="panel-heading">Custom Reciepts List</div>

<a href="{{ URL::to('custom-reciept/create') }}">
    <button type="button" class="btn btn-primary" style="margin: 10px 10px 10px 10px;">New Reciept</button>
</a>


<!-- printable area starts -->
<div id="colvis"></div>
<div id="pageprintarea">
        
    <table class="table table-bordered" id="users-table" style="width:100%; overflow:scroll;">
        <thead>
            <tr>
                <th>  Action  </th>
                <th>  admission no  </th>
                <th>  class section  </th>
                <th>  student name  </th>
                <th>  father name  </th>
                <th>  mother name  </th>
                <th>  address  </th>
                <th>  gender  </th>
                <th>  pay  </th>
                <th>  fee for  </th>
                <th>  mobile  </th>
                <th>  email  </th>
                <th>  dob  </th>
                <th>  fee name1  </th>
                <th>  fee value1  </th>
                <th>  fee name2  </th>
                <th>  fee value2  </th>
                <th>  fee name3  </th>
                <th>  fee value3  </th>
                <th>  fee name4  </th>
                <th>  fee value4  </th>
                <th>  fee name5  </th>
                <th>  fee value5  </th>
                <th>  remark  </th>
                
            </tr>
        </thead>
    <tbody>
       
        @foreach($data as $reptlists)
        <tr>
            <td>  <a href="{{ URL::to('custom-reciept/') }}/{{$reptlists->id}}">
                <button type="button" class="btn btn-primary" style="margin: 10px 10px 10px 10px;">Print</button>
                </a>
            </td>
            <td>  {{ $reptlists->admission_no    }}  </td>
            <td>  {{ $reptlists->class_section    }}  </td>
            <td>  {{ $reptlists->student_name    }}  </td>
            <td>  {{ $reptlists->father_name    }}  </td>
            <td>  {{ $reptlists->mother_name    }}  </td>
            <td>  {{ $reptlists->address    }}  </td>
            <td>  {{ $reptlists->gender    }}  </td>
            <td>  {{ $reptlists->pay    }}  </td>
            <td>  {{ $reptlists->fee_for    }}  </td>
            <td>  {{ $reptlists->mobile    }}  </td>
            <td>  {{ $reptlists->email    }}  </td>
            <td>  {{ $reptlists->dob    }}  </td>
            <td>  {{ $reptlists->fee_name1    }}  </td>
            <td>  {{ $reptlists->fee_value1    }}  </td>
            <td>  {{ $reptlists->fee_name2    }}  </td>
            <td>  {{ $reptlists->fee_value2    }}  </td>
            <td>  {{ $reptlists->fee_name3    }}  </td>
            <td>  {{ $reptlists->fee_value3    }}  </td>
            <td>  {{ $reptlists->fee_name4    }}  </td>
            <td>  {{ $reptlists->fee_value4    }}  </td>
            <td>  {{ $reptlists->fee_name5    }}  </td>
            <td>  {{ $reptlists->fee_value5    }}  </td>
            <td>  {{ $reptlists->remark    }}  </td>
        </tr>
            @endforeach
        
    </tbody>

    </table>
    </div> 
    <!-- printable area ends -->
    </div>
    </div>
    </div>
</div>
</div>


@endsection



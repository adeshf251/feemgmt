@extends('layouts.app2')

@section('content')
<form method="post" action="{{ url('/') }}/custom-reciept">
 {{ csrf_field() }}
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading">Custom Reciept Generation Form</div>

                <div class="panel-body">
	    
	    <div class="form-group col-md-6">
	    <label class="control-label" for="admission_no">admission no </label>
	    <input type="text" class="form-control"  id="admission_no" name="admission_no" autofocus>
	    </div>

        <div class="form-group col-md-6">
	    <label class="control-label" for="stdname">Student Name </label>
	    <input type="text" class="form-control"  id="stdname" name="stdname" autofocus>
	    </div>

        <div class="form-group col-md-6">
	    <label class="control-label"  for="position">Class *</label>
	    <select name="clsec" class="form-control">
			@foreach ($classname as $element)
				@foreach ($element as $e)
					<option value="{{ $e }}"> {{ $e }} </option>
				@endforeach
			@endforeach
		</select>
	    </div>


	    <div class="form-group col-md-6">
	    <label class="control-label"  for="gender">Gender</label>
	    <select name="gender" class="form-control" id="gender">
	    	<option value="male"> Male </option>
	    	<option value="male"> Female </option>
	    </select>
	    </div>

	    <div class="form-group col-md-6">
	    <label class="control-label"  for="mother_name">Mother Name</label>
	    <input type="text" class="form-control"  id="mother_name" name="mother_name" autofocus>
	    </div>

	    <div class="form-group col-md-6">
	    <label class="control-label"  for="father_name">Father Name</label>
	    <input type="text" class="form-control"  id="father_name" name="father_name" autofocus>
	    </div>


	    <div class="form-group col-md-6">
	    <label class="control-label"  for="address">Address</label>
	    <input type="text" class="form-control"  id="address" name="address" autofocus>
	    </div>

	    <div class="form-group col-md-6">
	    <label class="control-label"  for="mobile">Mobile No</label>
	    <input type="number" class="form-control"  id="mobile" name="mobile" autofocus>
	    </div>

 
	    <div class="form-group col-md-12">
	    <label class="control-label"  for="remark">Remark / Other Details </label>
	    <input type="text" class="form-control"  id="remark" name="remark" value="N.A." autofocus >
	    </div>

	    <div class="form-group col-md-6">
	    <label class="control-label"  for="dob">Date of birth*</label>
	    <input type="date" class="form-control"  id="dob" name="dob" autofocus>
	    </div>

        <div class="form-group col-md-6">
	    <label class="control-label"  for="pay">pay</label>
	    <select name="pay" class="form-control" id="pay">
	    	<option value="cash"> cash </option>
	    	<option value="cheque"> cheque </option>
	    	<option value="dd"> dd </option>
	    </select>
	    </div>

        <div class="form-group col-md-6">
	    <label class="control-label" for="fee_for">Fee For (eg- Months, Registration) </label>
	    <input type="text" class="form-control"  id="fee_for" name="fee_for" autofocus>
	    </div>

        <div class="form-group col-md-6">
	    <label class="control-label" for="email">Email ID </label>
	    <input type="text" class="form-control"  id="email" name="email" autofocus>
	    </div>

        <div class="form-group col-md-6">
	    <label class="control-label" for="fee_name1">Fee Type 1 </label>
	    <input type="text" class="form-control"  id="fee_name1" name="fee_name1" autofocus>
	    </div>

        <div class="form-group col-md-6">
	    <label class="control-label" for="fee_value1">Fee Amount 1 </label>
	    <input type="number" class="form-control"  id="fee_value1" name="fee_value1" autofocus>
	    </div>

        <div class="form-group col-md-6">
	    <label class="control-label" for="fee_name2">Fee Type 2 </label>
	    <input type="text" class="form-control"  id="fee_name2" name="fee_name2" autofocus>
	    </div>

        <div class="form-group col-md-6">
	    <label class="control-label" for="fee_value2">Fee Amount 2 </label>
	    <input type="number" class="form-control"  id="fee_value2" name="fee_value2" autofocus>
	    </div>

        <div class="form-group col-md-6">
	    <label class="control-label" for="fee_name3">Fee Type 3 </label>
	    <input type="text" class="form-control"  id="fee_name3" name="fee_name3" autofocus>
	    </div>

        <div class="form-group col-md-6">
	    <label class="control-label" for="fee_value3">Fee Amount 3 </label>
	    <input type="number" class="form-control"  id="fee_value3" name="fee_value3" autofocus>
	    </div>

        <div class="form-group col-md-6">
	    <label class="control-label" for="fee_name4">Fee Type 4 </label>
	    <input type="text" class="form-control"  id="fee_name4" name="fee_name4" autofocus>
	    </div>

        <div class="form-group col-md-6">
	    <label class="control-label" for="fee_value4">Fee Amount 4 </label>
	    <input type="number" class="form-control"  id="fee_value4" name="fee_value4" autofocus>
	    </div>

        <div class="form-group col-md-6">
	    <label class="control-label" for="fee_name5">Fee Type 5 </label>
	    <input type="text" class="form-control"  id="fee_name5" name="fee_name5" autofocus>
	    </div>

        <div class="form-group col-md-6">
	    <label class="control-label" for="fee_value5">Fee Amount 5 </label>
	    <input type="number" class="form-control"  id="fee_value5" name="fee_value5" autofocus>
	    </div>


	    <div class="form-group col-md-6">
	   <button type="submit" class="btn btn-success col-md-6">Save & Generate</button>
	    </div>
   
     
     
                </div>
            </div>         
        </div>
    </div>
</div>
@endsection

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Important Message </title>
</head>
<body>
    <h1 style="font-size:18px; font-weight:bold;">{{$title}}</h1>
    <h1 style="font-size:16px;">{{$content}}</h1>
    <h6>Please Ignore if you get the duplicate email.</h6>
</body>
</html>
@extends('layouts.app')

@section('content')
<form method="post" action="{{ url('/') }}/temporary-student">
 {{ csrf_field() }}
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading">&#10020; New Registration [DO NOT LEAVE ANY FIELD EMPTY, USE 0 FOR EMPTY FIELDS]</div>

                <div class="panel-body">
	    
	    <div class="form-group col-md-6">
	    <label class="control-label" for="stdname">Student Name (Required)*</label>
	    <input type="text" class="form-control"  id="stdname" name="stdname" autofocus required>
	    </div>

	    <div class="form-group col-md-3">
	    <label class="control-label"  for="gender">Gender</label>
	    <select name="gender" class="form-control" id="gender">
	    	<option value="male"> Male </option>
	    	<option value="male"> Female </option>
	    </select>
	    </div>

	    <div class="form-group col-md-3">
	    <label class="control-label"  for="position">Class *</label>
	    <select name="clsec" class="form-control">
			@foreach ($classname as $element)
				
					<option value="{{ $element->class_section }}"> {{ $element->class_section }} </option>
				
			@endforeach
		</select>
	    </div>


	    <div class="form-group col-md-6">
	    <label class="control-label"  for="mother_name">Mother Name</label>
	    <input type="text" class="form-control"  id="mother_name" name="mother_name" autofocus required>
	    </div>

	    <div class="form-group col-md-6">
	    <label class="control-label"  for="father_name">Father Name</label>
	    <input type="text" class="form-control"  id="father_name" name="father_name" autofocus required>
	    </div>


	    <div class="form-group col-md-6">
            <div class="col-md-12"><label class="control-label"  for="address">Address</label></div>
            <div class="col-md-12">
                <textarea name="address" id="address" cols="60" rows="6"  autofocus required></textarea>
            </div>
	    
	    <!-- <input type="text" class="form-control"  id="address" name="address" autofocus required> -->
        
	    </div>

	    <div class="form-group col-md-6">
	    <label class="control-label"  for="mobile">Mobile No</label>
	    <input type="number" class="form-control"  id="mobile" name="mobile" autofocus required>
	    </div>

	    <div class="form-group col-md-6">
	    <label class="control-label"  for="transport_route">Transportation Route</label>
	    <select name="transport_route" class="form-control">
            <option value="0"> Not a Transport User </option>
			@foreach ($transport_route as $element)
				
					<option value="{{ $element->id }}"> {{ $element->id }} </option>
				
			@endforeach
		</select>
	    </div>

	    
	    <div class="form-group col-md-6">
	    <label class="control-label"  for="remark">Remark / Other Details </label>
	    <input type="text" class="form-control"  id="remark" name="remark" value="N.A." autofocus >
	    </div>

        <div class="form-group col-md-6">
	    <label class="control-label"  for="remark">Email ID (Automatic Email Send if provided) </label>
	    <input type="text" class="form-control"  id="email" name="email" value="" autofocus >
	    </div>

	    <div class="form-group col-md-6">
	    <label class="control-label"  for="dob">Date of birth*</label>
	    <input type="date" class="form-control"  id="dob" name="dob" autofocus required>
	    </div>

	    <div class="form-group col-md-6">
	   <button type="submit" class="btn btn-success col-md-6">Register Student</button>
	    </div>
   
     
     
                </div>
            </div>         
        </div>
    </div>
</div>
@endsection

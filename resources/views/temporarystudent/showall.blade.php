<?php $counter=1; ?>
@extends('layouts.app2')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12" >
            <div class="panel panel-default">

                <div class="panel-heading">List of Temporary Students </div>

                <div class="panel-body " style="overflow-x:auto ; overflow-y: auto; padding-bottom:10px;">
                    <div id="colvis"></div>
        <table class="table table-bordered" id="users-table" >
        <thead>
            <tr>
              <th>SN</th>
              <th>Action</th>
              <th>Temp. Admission No</th>
              <th>Student Name</th>
              <th>Class section</th>
              <th>Father's Name</th>
              <th>Mother's Name</th>
              <th>Email</th>
              <th>Mobile</th>
              <th>Transport Route</th>
              <th>DOB</th>
              <th>Delete</th>
            </tr>
        </thead>

         <tbody>

         		@foreach ($data as $element)
         			<tr>
         				<td> {{ $counter++ }} </td>
			            <td> <a href="{{ url('/') }}/temporary-student/{{$element->id}}"> <button  class="btn btn-success">Reprint Now &#10148;</button> </a> </td>
			            <td> {{ $element->admission_no }} </td>
			            <td> {{ $element->student_name }} </td>
			            <td> {{ $element->class_section }} </td>
			            <td> {{ $element->father_name }} </td>
			            <td> {{ $element->mother_name }} </td>
			            <td> {{ $element->email }} </td>
			            <td> {{ $element->mobile }} </td>
			            <td> {{ $element->transport_route }} </td>
			            <td> {{ $element->dob }} </td>
                        <td id="non-printable"> <a href="{{ url('/') }}/temporary-student-delete/{{$element->id}}" > <button type="button" class="btn btn-danger">Delete  &#10006;</button></a> </td>
	         		</tr>
         		@endforeach

         </tbody>

          </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@extends('layouts.app')

@section('content')

<style>
  .panel {
    box-shadow: 3px 3px 3px 0px rgb(10, 100, 2);
    /* transform: scale(0.9); */
    width : 150px;
    height: 160px;
  }
  .panel:hover {
    opacity: 1;
    background-color: rgb(255, 187, 1);
  }
  a {
    text-decoration: none;
    
    color: black;
  }
  .routerLink {
    font-weight: 600;
    text-align: center;
  }
  a:hover {
    text-decoration: none;
    color: rgb(245, 91, 91);
  }
  .icons {
    height : 80px;
    width : 80px;
    margin : 5px;
  }
</style>



<div class="container"  >
    <div class="row">
    
     <ol class="breadcrumb">
            <li class="breadcrumb-item active"><i class="fa fa-cc-visa" aria-hidden="true"></i>
              Fee Management </li>
     </ol>
        <div class="col-md-10 col-md-offset-1">
            <div class=" panel panel-default" style="float: left;">
              <a class="routerLink" href="search-student">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/pay-fee.jpg" class="icons">
                           Pay Fee
                          </div>
               </a>
            </div> 

            <div class="   panel panel-default"  style="float: left; margin-left: 40px;">
              <a class="routerLink" href="delete-paid-fee-verify-admin">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/delete.png" class="icons">
                           Delete Paid Fee
                          </div>
               </a>
            </div> 

            

            <div class="   panel panel-default"  style="float: left; margin-left: 40px;">
              <a class="routerLink" href="Reprint-fee-Reciept-request">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/printer.png" class="icons">
                           Reprint Fee Reciept
                          </div>
               </a>
            </div> 
        </div>

    </div>
    <div class="row">

        <ol class="breadcrumb">
            <li class="breadcrumb-item active"><i class="fa fa-tachometer" aria-hidden="true"></i>
              Student </li>
     </ol>
        <div class="col-md-10 col-md-offset-1">
            <div class=" panel panel-default"  style="float: left;">
              <a class="routerLink" href="new-registration">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/new-reg.png" class="icons">
                           New Registration
                          </div>
               </a>
            </div> 

            <div class="panel panel-default"  style="float: left; margin-left: 40px;">
              <a class="routerLink" href="update-student-detail-request/0">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/update-info.png" class="icons">
                           Update Student Information
                          </div>
               </a>
            </div> 

            <div class="panel panel-default"  style="float: left; margin-left: 40px;">
              <a class="routerLink" href="classwise-student-list">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/classes.jpg" class="icons">
                           Classwise student list
                          </div>
               </a>
            </div> 

            <div class="panel panel-default"  style="float: left; margin-left: 40px;">
              <a class="routerLink" href="complete-student-list">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/student-list.png" class="icons">
                           Complete Student List
                          </div>
               </a>
            </div>
          

            <div class=" panel panel-default"  style="float: left; margin-left: 40px;">
              <a class="routerLink" href="search-student-details">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/search.png" class="icons">
                           Search Student
                          </div>
               </a>
            </div> 

            
        </div>


    </div>
    <div class="row">

        <ol class="breadcrumb">
            <li class="breadcrumb-item active"><i class="fa fa-btc" aria-hidden="true"></i>
              Core Management </li>
     </ol>
        <div class="col-md-10 col-md-offset-1">
            <div class=" panel panel-default"  style="float: left;">
              <a class="routerLink" href="new-class-reg">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/add-class.png" class="icons">
                           Add Class
                          </div>
               </a>
            </div> 

            <div class="panel panel-default"  style="float: left; margin-left: 40px;">
              <a class="routerLink" href="class-delete-req">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/delete.png" class="icons">
                           Remove Class
                          </div>
               </a>
            </div> 

            <div class="panel panel-default"  style="float: left; margin-left: 40px;">
              <a class="routerLink" href="add-fee-type">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/add-feetype.png" class="icons">
                           Add Fee type
                          </div>
               </a>
            </div> 

            <div class="panel panel-default"  style="float: left; margin-left: 40px;">
              <a class="routerLink" href="remove-fee-type-request">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/delete_fee_type.png" class="icons">
                           Remove Fee type
                          </div>
               </a>
            </div> 

            <div class="panel panel-default"  style="float: left; margin-left: 40px;">
              <a class="routerLink" href="fee-value-registration-classwise">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/add-feevalue.gif" class="icons">
                           Add Fee Value (individual class)
                          </div>
               </a>
            </div> 
             </div>

          <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default"  style="float: left;">
              <a class="routerLink" href="update-fee-value">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/update-fee.png" class="icons">
                           Update Fee (individual class)
                          </div>
               </a>
            </div>

            <div class="panel panel-default" style="float: left; margin-left: 40px;">
              <a class="routerLink" href="remove-fee-value">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/erase.png" class="icons">
                           Remove Value (individual class)
                          </div>
               </a>
            </div>  
        </div>

    </div>
    <div class="row">

        <ol class="breadcrumb">
            <li class="breadcrumb-item active"><i class="fa fa-inr" aria-hidden="true"></i>
              Discount </li>
       </ol>
        <div class="col-md-10 col-md-offset-1">
            <div class=" panel panel-default"  style="float: left;">
              <a class="routerLink" href="discount-provider-registration">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/discount.png" class="icons">
                           Discount Provider Reg.
                          </div>
               </a>
            </div> 


            <div class="panel panel-default"  style="float: left; margin-left: 40px;">
              <a class="routerLink" href="discount-beneficiary-provider">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/list.png" class="icons">
                           Provider Member List/delete
                          </div>
               </a>
            </div>

            <div class="panel panel-default"  style="float: left; margin-left: 40px;">
              <a class="routerLink" href="discount-provider-details-request">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/benificry.png" class="icons">
                           Discount given by individual
                          </div>
               </a>
            </div> 

            <div class="panel panel-default"  style="float: left; margin-left: 40px;">
              <a class="routerLink" href="discount-beneficiary-student">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/benificry.png" class="icons">
                           Benificiry Student List
                          </div>
               </a>
            </div>   
        </div>

            

    </div>
    <div class="row">

        <ol class="breadcrumb">
            <li class="breadcrumb-item active"><i class="fa fa-tachometer" aria-hidden="true"></i>
              Defaulters </li>
     </ol>
        <div class="col-md-10 col-md-offset-1">
            <div class=" panel panel-default"  style="float: left;">
              <a class="routerLink" href="monthwise-defaulter">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/month.png" class="icons">
                           Monthwise
                          </div>
               </a>
            </div> 

            <div class="panel panel-default"  style="float: left; margin-left: 40px;">
              <a class="routerLink" href="classwise-defaulter">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/classwise.png" class="icons">
                           Classwise
                          </div>
               </a>
            </div> 
<!--
            <div class="panel panel-default"  style="float: left; margin-left: 40px;">
              <a class="routerLink" href="#">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/overall.png" class="icons">
                           Overall
                          </div>
               </a>
            </div> 
-->
        </div>

        </div>

        <div class="row">

        <ol class="breadcrumb">
            <li class="breadcrumb-item active"><i class="fa fa-bus" aria-hidden="true"></i>
              Transport Management Route </li>
     </ol>
        <div class="col-md-10 col-md-offset-1">
            <div class=" panel panel-default"  style="float: left;">
              <a class="routerLink" href="insert-transport-routes-request">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/transport-add.png" class="icons">
                           Add Transport Route
                          </div>
               </a>
            </div> 

            <div class="panel panel-default"  style="float: left; margin-left: 40px;">
              <a class="routerLink" href="update-transport-routes-request">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/transport-route.png" class="icons">
                           Update/delete Transport Route
                          </div>
               </a>
            </div> 

            <div class="panel panel-default"  style="float: left; margin-left: 40px;">
              <a class="routerLink" href="transport-user-list">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/transport-user.png" class="icons">
                           Transport User List
                          </div>
               </a>
            </div> 

            
        </div>

        </div>

        <div class="row">

        <ol class="breadcrumb">
            <li class="breadcrumb-item active"><i class="fa fa-tachometer" aria-hidden="true"></i>
              Collection Management </li>
     </ol>
        <div class="col-md-10 col-md-offset-1">
            <div class=" panel panel-default"  style="float: left;">
              <a class="routerLink" href="collection-datewise">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/money-collection1.jpg" class="icons">
                           Collection Between Dates
                          </div>
               </a>
            </div> 
<!--
            <div class="panel panel-default"  style="float: left; margin-left: 40px;">
              <a class="routerLink" href="collection-monthtwise-or-feetype">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/money-bag.png" class="icons">
                           Collection By Fee-Month / Fee-Type
                          </div>
               </a>
            </div> 
-->

            <div class="panel panel-default"  style="float: left; margin-left: 40px;">
              <a class="routerLink" href="collection-by-student-request">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/money-bag.png" class="icons">
                           Collection By Student
                          </div>
               </a>
            </div>

            <div class="panel panel-default"  style="float: left; margin-left: 40px;">
              <a class="routerLink" href="collection-datewise-transport">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/school-bus.png" class="icons">
                           Dateswise Transport Col.
                          </div>
               </a>
            </div>

            <div class="panel panel-default"  style="float: left; margin-left: 40px;">
              <a class="routerLink" href="collection-by-student-transport-request">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/transport-collection2.png" class="icons">
                           Studentwise Transport Col.
                          </div>
               </a>
            </div> 
            
        </div>

        </div>

    <div class="row">

        <ol class="breadcrumb">
            <li class="breadcrumb-item active"><i class="fa fa-money" aria-hidden="true"></i>
              Pendency Amount </li>
     </ol>
        <div class="col-md-10 col-md-offset-1">
        <!--
            <div class=" panel panel-default"  style="float: left;">
              <a class="routerLink" href="pendency-amt-monthwise">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/a.png" class="icons">
                           Mothwise
                          </div>
               </a>
            </div> 
-->
            <div class="panel panel-default"  style="float: left;">
              <a class="routerLink" href="pendency-amt-classwise">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/pendency-classwise.png" class="icons">
                           Classwise Amount
                          </div>
               </a>
            </div> 

            <div class="panel panel-default"  style="float: left; margin-left: 40px;">
              <a class="routerLink" href="pendency-amt-monthwise">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/pendency-monthwise.png" class="icons">
                           Monthwise Amount
                          </div>
               </a>
            </div> 

            <div class="panel panel-default"  style="float: left; margin-left: 40px;">
              <a class="routerLink" href="pendency-amt-overall-submit">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/pendency-overall.png" class="icons">
                           Overall Amount
                          </div>
               </a>
            </div> 

            <div class="panel panel-default"  style="float: left; margin-left: 40px;">
              <a class="routerLink" href="pendency-amt-studentwise">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/pendency-student.png" class="icons">
                           Studentwise
                          </div>
               </a>
            </div> 

            <div class="panel panel-default"  style="float: left; margin-left: 40px;">
              <a class="routerLink" href="overall-classwise-defaulter">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/money-bag.png" class="icons">
                           Overall Classwise List
                          </div>
               </a>
            </div> 

        </div>

        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default"  style="float: left;">
              <a class="routerLink" href="pendency-amt-once-req-fee">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/pendency-classwise.png" class="icons">
                           One Time Fee
                          </div>
               </a>
            </div>

        </div>

        <!-- <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default"  style="float: left;">
              <a class="routerLink" href="overall-yearly-defaulter-list">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/money-bag.png" class="icons">
                           Overall Yearly List
                          </div>
               </a>
            </div>

        </div> -->

    </div>




    <div class="row">

        <ol class="breadcrumb">
            <li class="breadcrumb-item active">&#10020; 
              Additional Control Board</li>
     </ol>
        <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default"  style="float: left;">
              <a class="routerLink" href="sessionchoose">  <div class=" panel-body">
                           <img src="{{ url('/') }}/icons/007-calendar.png" class="icons">
                           Change Working Session
                          </div>
               </a>
            </div>

            <div class="panel panel-default"  style="float: left; margin-left: 40px;">
              <a class="routerLink" href="temporary-student/create">  <div class=" panel-body">
                           <img src="{{ url('/') }}/icons/026-social.png" class="icons">
                           Register Guest Student
                          </div>
               </a>
            </div>

            <div class="panel panel-default"  style="float: left; margin-left: 40px;">
              <a class="routerLink" href="temporary-student">  <div class=" panel-body">
                           <img src="{{ url('/') }}/icons/037-users.png" class="icons">
                           Show All Guest Students
                          </div>
               </a>
            </div>

            <div class="panel panel-default"  style="float: left; margin-left: 40px;">
              <a class="routerLink" href="temporary-student-collection">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/pendency-classwise.png" class="icons">
                           Guest Students Collections
                          </div>
               </a>
            </div>

            <div class="panel panel-default"  style="float: left; margin-left: 40px;">
              <a class="routerLink" href="custom-reciept">  <div class=" panel-body">
                           <img src="{{ asset('images') }}/printer.png" class="icons">
                           Custom Reciepts
                          </div>
               </a>
            </div>


        </div>

       

    </div>



    </div>



@endsection

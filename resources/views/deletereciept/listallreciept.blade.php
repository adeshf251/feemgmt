@extends('layouts.app2')

@section('content')
<?php
$val =  Session::get('adminpermission');
if($val != 'true')
{
  redirect('/');
}
?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
{{ $form_heading }}
 <div class="panel-body">  <?php $x=""; ?> <p> Please choose the reciept to delete. <br> * Once deleted will not be rollback.
        @isset($data)
        <div id="colvis"></div>
        <table class="table table-bordered" id="users-table">
        <thead>
         <tr>
          <th>Reciept Number</th>
          <th>Admission No</th>
          <th>Student Name </th>
          <th>Class Section</th>
          <th>Parent Name</th>
          <th>Total Fee (Collected)</th>
          <th>Extra Details</th>
          <th>Last Modified</th>
          <th>Action</th>
          </tr>
          </thead>
         <tbody>
          @foreach ($data as $element)
              <tr> 
                  @foreach ($element as $key => $val)
                      @if($key == 'rept_no')
                      <?php $x=$val; ?>
                      @endif
                        <td>  <h5> {{ $val }} </h5> </td> 

                  @endforeach
                  <td> <a href="{{ url('/') }}/delete-paid-fee-reciept/months/{{$x}}" > <button type="button" class="btn btn-success">Delete &#10006;</button></a> </td>
              </tr>
          @endforeach
          </tbody>
          </table>
        @endisset

                </div>
            </div>         
        </div>
    </div>
</div>


@endsection


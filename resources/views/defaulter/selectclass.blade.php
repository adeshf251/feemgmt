@extends('layouts.app')

@section('content')

 <form method="post" action="{{ url('/') }}/classwise-defaulter-list">
 {{ csrf_field() }}
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading">{{ $form_heading }}</div>

                <div class="panel-body">
                    <div class="col-md-6">
             <select id="cl" name="cl" class="form-control">
            @foreach ($data_cl as $el)
                 @foreach ($el as $element)
                <option value="{{$element}}">{{$element}}</option>
                @endforeach
            @endforeach
            </select>
<br>
            <select id="month" name="month" class="form-control">
            @foreach ($data as $element)
                <option value="{{$element}}">{{$element}}</option>
            @endforeach
            </select>

            </div>
   
     <button type="submit" class="btn btn-success col-md-3">Proceed</button>
     
                </div>
            </div>         
        </div>
    </div>
</div>


@endsection
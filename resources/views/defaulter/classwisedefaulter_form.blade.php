@extends('layouts.app')
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
@section('content')

 <form method="post" action="{{ url('/') }}/classwise-defaulter-list">
 {{ csrf_field() }}
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading"></div>

                <div class="panel-body">

                	<div class="form-group{{ $errors->has('month') ? ' has-error' : '' }}">
                        <label for="month" class="col-md-6 control-label">Fee Months / Fee Types*</label>

                        <div class="col-md-6"> 
                        <select name="month" class="form-control">
                            @foreach ($options as $element)
                                @foreach ($element as $ele)
                                    <option value="{{$ele}}"> {{$ele}} </option>
                                @endforeach 
                            @endforeach 
                        </select>
                        </div>
                    </div>
<br><br>

                    <div class="form-group{{ $errors->has('class_section') ? ' has-error' : '' }}">
                        <label for="class_section" class="col-md-6 control-label">Class-Section*</label>

                        <div class="col-md-6"> 
                        <select name="class_section" class="form-control">
                            @foreach ($options_class as $element)
                                @foreach ($element as $ele)
                                    <option value="{{$ele}}"> {{$ele}} </option>
                                @endforeach 
                            @endforeach 
                        </select>
                        </div>
                    </div>


                	
                <button type="submit" class="btn btn-success">Show Details</button>
                </div>
            </div>         
        </div>
    </div>
</div>

</form>          

@endsection
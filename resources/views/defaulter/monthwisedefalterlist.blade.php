@extends('layouts.app2')

@section('content')




 <form method="post" action="{{ url('/') }}/monthwise-defaulter-list">
 {{ csrf_field() }}
<div class="container">
    <div class="row">
        <div class="">
            <div class="panel panel-default">

                <div class="panel-heading">List of Fee Unpaid Students</div>

                <div class="panel-body">
                    <div class="">
                        <div id="colvis"></div>
             <table  class="table " id="users-table" style="font-size: 14px"> 
    	    @foreach ($data as $element)
    	    <tr>
        		   @foreach ($element as $e)
        			<td> {{$e}}</td>
       			  @endforeach
       			</tr>
       		@endforeach
            </table>
            </div>
  
     
     
                </div>
            </div>         
        </div>
    </div>
</div>


@endsection
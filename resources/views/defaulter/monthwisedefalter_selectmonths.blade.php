@extends('layouts.app')
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
@section('content')

 <form method="post" action="{{ url('/') }}/monthwise-defaulter-list">
 {{ csrf_field() }}
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading">{{ $form_heading }}</div>

                <div class="panel-body">
                    
        <table class="table table-bordered" id="users-table">


        
        <tr> <label> Select Months:</label>
    
         <td> <input type="checkbox" id="april" name="april"> april </input>  </td>  
         <td> <input type="checkbox" id="may" name="may"> may </input>  </td>
         <td> <input type="checkbox" id="june" name="june"> june </input>  </td>
         <td> <input type="checkbox" id="july" name="july"> july </input>  </td>
         <td> <input type="checkbox" id="aug" name="aug"> aug </input>  </td>
         <td> <input type="checkbox" id="sep" name="sep"> sept </input>  </td>
         <td> <input type="checkbox" id="oct" name="oct"> oct </input>  </td>
         <td> <input type="checkbox" id="nov" name="nov"> nov </input>  </td>
         <td> <input type="checkbox" id="dec" name="dec"> dec </input>  </td>
         <td> <input type="checkbox" id="jan" name="jan"> jan </input>  </td>
         <td> <input type="checkbox" id="feb" name="feb"> feb </input>  </td>
         <td> <input type="checkbox" id="march" name="march"> march </input>  </td>

        </tr>
        </table>
        
        
        
<table class="table table-bordered" id="users-table">
  @foreach ($once_required as $key => $element)
    <tr>
      <td> <label> Include This once required Fee :</label> </td>
      <td> <input type="checkbox" id="{{$element}}" name="{{$element}}"> {{$element}} </input>  </td>
    </tr>
  @endforeach
</table>
      
   <?php  $once_required_fee_array = base64_encode(serialize($once_required)); ?>
   <input type="hidden" name="once_required_fee_array" value="<?php echo $once_required_fee_array; ?>">
     
     <br>
     <button type="submit" class="btn btn-success">Proceed to Defaulters List</button>
     
                </div>
            </div>         
        </div>
    </div>
</div>

</form>          

<script src="/public/js/jquery.min.js"></script>



@endsection
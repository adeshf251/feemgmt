@extends('layouts.app2')
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
@section('content')

<?php $sum = 0; ?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading">Defaulters List Monthwise</div>

                <div class="panel-body">
        <div id="colvis"></div>
                <table  class="table table-bordered" id="users-table">
                    <thead>
                        <th>S. No</th>
                        <th>Admission Number</th>
                        <th>Student Name</th>
                        <th>Class-Section</th>
                        <th>Father's Name</th>
                        <th>Gender</th>
                    </thead>
                    <tbody>
                        @foreach ($student_list as $element)
                            <tr> <td> {{ $sum = $sum + 1 }} </td>
                                @foreach ($element as $key => $val)
                                    <td> {{ $val }} </td>
                                @endforeach
                            </tr>
                        @endforeach
                    </tbody>

                    <tfoot>
                        <td colspan="4"><strong> Total Defaulter Counts </strong> </td>
                        <td colspan="2"><b> <strong> {{$sum}} </strong> </b></td>
                    </tfoot>
                </table>
       

                </div>
            </div>         
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function()
    {
          $("tr:even").css("background-color","#e5f9f9");

    });
</script>

@endsection
@extends('layouts.app')

@section('content')

 <form method="post" action="{{ url('/') }}/monthwise-defaulter-list">
 {{ csrf_field() }}
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading">{{ $form_heading }}</div>

                <div class="panel-body">
                    <div class="col-md-12">
                        <div id="colvis"></div>
             <table  class="table " id="users-table" style="font-size: 14px">
    	    @foreach ($data as $element)
    	    
        		   @foreach ($element as $e)
                   <tr>
        			@foreach ($e as $xx)
                    <td> {{$xx}}</td>
                    
                @endforeach</tr>
       			@endforeach
       			
       		@endforeach
            </table>
            </div>
  
     
     
                </div>
            </div>         
        </div>
    </div>
</div>


@endsection
 @extends('layouts.app2')
 
 @section('content')
<?php $counter = 1; $sum = 0; ?>

<style>
    tfoot {
    display: table-header-group;
}
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading"> List of Students whose amount is Still Pending</div>

                <div class="panel-body">
                    <div class="col-md-12">
                        <div id="colvis"></div>
                        <table id="users-table" class="display" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>SN.</th>
                                    <th>Admission No</th>
                                    <th>Student Name</th>
                                    <th>Class-Section</th>
                                    <th>Father Name/Parent Name</th>
                                    <th>Mobile</th>
                                    <th>Transport Route</th>

                                    <th>Fee Months Pending</th>
                                    <th>Fee Amount Pending</th>
                                    <th>Transport Amount Pending</th>
                                    <th>Total Amount</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach ($students as $key => $studentDetails)
                                    @if($fee_totals[$key] != 0)
                                        <tr>
                                            <td> {{ $counter++ }} </td>
                                            <td> {{ $studentDetails->admission_no }} </td>
                                            <td> {{ $studentDetails->student_name }} </td>
                                            <td> {{ $studentDetails->class_section }} </td>
                                            <td> {{ $studentDetails->father_name }} </td>
                                            <td> {{ $studentDetails->mobile }} </td>
                                            <td> {{ $studentDetails->transport_route }} </td>

                                            <td> {{ $fee_months[$key] }} </td>
                                            <td> {{ $fee_totals[$key] }} </td>
                                            <td> {{ $transport_totals[$key] }} </td>
                                            <td> {{ $a = ( $fee_totals[$key] + $transport_totals[$key] ) }} </td>
                                            <?php $sum = $sum + $a; ?>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>

                            <tfoot>
                                <td colspan="10">
                                    <i> Original Pending Amount May be Less than the shown, due to discount and Other Factors. </i>
                                </td>
                                <td>
                                    {{$sum}}
                                </td>
                            </tfoot>

                        </table>
                    </div>



                </div>
            </div>
        </div>
    </div>
</div>


@endsection


@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading">{{ $form_heading }}</div>

                <div class="panel-body">
                    <div class="col-md-12">
                        <div id="colvis"></div>
             <table  class="table " id="users-table" style="font-size: 14px">
             <th>Admission No</th><th>Student Name</th><th>Class-Section</th><th>Father Name/Parent Name</th><th>Gender</th><th>Address</th>
    	    @foreach ($data as $element)
    	    <tr>
        		   @foreach ($element as $e)
        			<td> {{$e}}</td>
       			@endforeach
       			</tr>
       		@endforeach
            </table>
            </div>
  
     
     
                </div>
            </div>         
        </div>
    </div>
</div>


@endsection
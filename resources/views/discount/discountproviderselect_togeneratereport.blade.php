@extends('layouts.app')
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
@section('content')

 <form method="post" action="{{ url('/') }}/discount-provider-details-request-submit">
 {{ csrf_field() }}
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading">{{ $form_heading }}</div>

                <div class="panel-body">
      

@isset($data)
	<select name="discountprovider" class="form-control"> 
            @foreach ($data as $e)
               
                        @foreach ($e as $element)
                            <option value="{{$element}}"> {{$element}} </option>
                        @endforeach
               
            @endforeach
    </select>
@endisset

     <br>
     <button type="submit" class="btn btn-success">Proceed to details</button>
     
                </div>
            </div>         
        </div>
    </div>
</div>

</form>          

<script src="{{ url('/') }}/public/js/jquery.min.js"></script>



@endsection
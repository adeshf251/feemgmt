@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
<div class="panel-heading"> &#10070; {{ $form_heading }}</div>
 <div class="panel-body">  
        @isset($data)
        <div id="colvis"></div>
        <table class="table table-bordered" id="users-table">
         <tr> <th>Provider Name</th> <th>Position</th> <th>Department</th> <th>EmailID</th> <th>Gender</th><th>Mobile No</th><th>Other detail</th><th>Registered On</th><th>Delete</th></tr>
          @foreach ($data as $element)
              <tr> 
                  @foreach ($element as $key => $val)
                      @if($key == 'id')
                          <td> <a href="{{ url('/') }}/discount-beneficiary-provider-delete/{{$val}}" > <button type="button" class="btn btn-success">Delete  &#10006;</button></a> </td>
                      @endif
                      @if($key != 'id')
                        <td>  <h5> {{ $val }} </h5> </td> 
                      @endif
                  @endforeach
                   
              </tr>
          @endforeach
          </table>
        @endisset

                </div>
            </div>         
        </div>
    </div>
</div>
@endsection
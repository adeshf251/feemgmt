@extends('layouts.app')

@section('content')
<input type="button" onclick="printDiv('pageprintarea')" value="Print this page" />
<div id="pageprintarea">
<form method="post" action="{{ url('/') }}/discount-provider-details-request-submit">
 {{ csrf_field() }}
<div class="container">
    <div class="row">
        <div class="col-md-12">  
            <div class="panel panel-default">

                <div class="panel-heading">Discount Provider Complete Details</div>

                <div class="panel-body">
      <table class="table table-bordered" id="users-table">
      <tr> <th>Name</th><th>Position</th><th>Department</th><th>Email</th><th>gender</th><th>Mb</th> </tr>
            @foreach ($providerdetail as $e)
               
                        @foreach ($e as  $element)
                           <td> {{$element}} </td> 
                        @endforeach
               
            @endforeach
      </table>
     
                </div>
            </div>         
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading">Month & One Time Fee vs Discounts, Taking max Discount as 100% </div>

                <div class="panel-body">
<?php $max=max($amount); $total=array_sum($amount); $aa = 40; ?>
<table class="table table-bordered" id="users-table" style="text-transform: capitalize;">
<tr><th>Month / Fee Type</th><th>Amount</th><th>Progressbar</th></tr>
@foreach ($month as $key => $element)
	<tr>
		<td> {{ $element }} </td>
		<td> {{ $amount[$key] }} </td>
		<td>	
			<div class="progress">
    			<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="<?php $aa=($amount[$key] / $max) * 100 ; echo $aa; ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $aa; ?>%"> <?php echo $aa; ?> %
   				 </div>
 			 </div>
  		</td>
	</tr>
@endforeach

<tr><td><strong>Total Discount Offered :</strong> </td><td><strong> {{$total}} </strong> </td><td></td></tr>
</table>
      

     
                </div>
            </div>         
        </div>
    </div>
</div>
</form>
</div> 



@endsection


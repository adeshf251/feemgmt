@extends('layouts.app')

@section('content')

 <form method="post" action="{{ url('/') }}/discount-provider-registration-submit">
 {{ csrf_field() }}
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading">Discount Provider Registration</div>

                <div class="panel-body">
	    
	    <div class="form-group col-md-6">
	    <label class="control-label" for="name">Name (Required)*</label>
	    <input type="text" class="form-control"  id="name" name="name" autofocus required>
	    </div>

	    <div class="form-group col-md-6">
	    <label class="control-label"  for="position">Position *</label>
	    <input type="text" class="form-control"  id="position" name="position" autofocus required>
	    </div>

	    <div class="form-group col-md-6">
	    <label class="control-label"  for="department">Department</label>
	    <input type="text" class="form-control"  id="department" name="department" autofocus >
	    </div>

	    <div class="form-group col-md-6">
	    <label class="control-label"  for="email">Email (Required)*</label>
	    <input type="email" class="form-control"  id="email" name="email" autofocus required>
	    </div>

	    <div class="form-group col-md-6">
	    <label for="gender">Gender*</label>
	            <select name="gender"  class="custom-select form-control" >
	             <option value="male">Male</option>
	             <option value="female">Female</option>
	            </select>
	    </div>

	    <div class="form-group col-md-6">
	    <label class="control-label" for="mobile">Mobile Number*</label>
	    <input type="number" class="form-control"  id="mobile" name="mobile" autofocus>
	    </div>

	    <div class="form-group col-md-12">
	    <label class="control-label" for="extra1">Any Comment*</label>
	    <input type="text" class="form-control"  id="extra1" name="extra1" autofocus>
	    </div>
   <br>
     <button type="submit" class="btn btn-success col-md-3">Register Discount Provider</button>
     
                </div>
            </div>         
        </div>
    </div>
</div>


@endsection
@extends('layouts.app2')

<?php $sum = 0; $counter=1; ?>

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
<div class="panel-heading">{{ $form_heading }}</div>
 <div class="panel-body">  
        @isset($data)
        <div id="colvis"></div>
        <table class="table table-bordered" id="users-table">
          
            <thead>
               <th>S.No</th>
               <th>Student Admission No</th>
               <th>Student Name</th>
               <th>Class Section</th>
               <th>Father Name</th> 
               <th>Fee For</th>
               <th>Fee Amount</th>
               <th>Discount Provider</th>
               <th>Reason</th>
            </thead>

            <tbody>
            @foreach ($data as $element)
                <tr>  <td> {{ $counter++ }} </td>
                    @foreach ($element as $key => $val)
                        @if($key == 'rept_no')
      
                        @endif
                          <td>  <h5> {{ $val }} </h5> </td> 
                    @endforeach
                </tr>
            @endforeach
            </tbody>

        </table>
        @endisset

                </div>
            </div>         
        </div>
    </div>
</div>
@endsection
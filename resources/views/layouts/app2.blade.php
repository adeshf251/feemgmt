
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'DELLMOND INTERNATIONAL SCHOOL') }}  @if(Session('valid_period')) {{Session('valid_period')}} @endif</title>

    <!-- Styles Sheet-->
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/styl.css') }}">
  <link rel="stylesheet" href="{{ url('/') }}/plugins/datatables/jquery.dataTables.min.css">

  <link rel="stylesheet" type="text/css" href="{{ asset('/css/font-awesome.min.css') }}">

  <!--
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.4.0/css/buttons.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
-->

<link rel="stylesheet" href="{{ url('/') }}/css/font-awesome.min.css">
  <link rel="stylesheet" href="{{ url('/') }}/css/buttons.dataTables.min.css">
  <link rel="stylesheet" href="{{ url('/') }}/css/jquery.dataTables.min.css">
  
  <script src="{{ asset('/js/jquery-1.12.2.js') }}"></script>
    <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
  <style type="text/css">
    <style>
body {
    font-family: "Lato", sans-serif;
    background-color: #f1f1f1;
    text-transform: capitalize;

}

.sidenav {
    height: 100%;
    width: 0;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: rgb(141, 36, 10);
    overflow-x: hidden;
    transition: 0s;
    padding-top: 60px;
}

.sidenav a {
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 20px;
    color: #f0eeee;
    display: block;
    transition: 0.3s;
    z-index: 1;
}

.sidenav a:hover, .offcanvas a:focus{
    color: #7de02bfd;
}

.sidenav .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    font-size: 36px;
    margin-left: 50px;
    z-index: 1;
}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}
.container {
        opacity: 0.95;
}
div {
    text-transform: capitalize;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>
</head>
<body style="background-image: url('{{ url('/') }}/images/sc.jpg'); background-size: 100%; background-attachment: fixed;">
<div id="mySidenav" class="sidenav" style="z-index: 1;">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a href="{{ url('/') }}">Main Page</a>
  @if (Auth::guest())
  @else
    <a href="{{ url('/') }}/home"> &#9921; Dashboard</a>
    <a href="{{ url('/') }}/new-registration"> &#9921; New Registration</a>
    <a href="{{ url('/') }}/update-student-detail-request/0"> &#9921; Search Student</a>
    <a href="{{url('/')}}/custom-reciept"> &#9921; Custom Reciept</a>
    <a href="{{url('/')}}/temporary-student/create"> &#9921; Guest Student</a>
    <a href="{{url('/')}}/sessionchoose"> &#9921; Choose Session</a>
    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
             &#9921; Logout
    </a>
  @endif
  
</div>

    <div id="app">
        <nav class="navbar navbar-default ">
            <div class="container">
                <div class="navbar-header">
                 <!-- Branding Image -->
                    <span class="navbar-brand" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; {{ config('app.name', 'Laravel') }}  @if(Session('valid_period')) {{Session('valid_period')}} @endif </span>
                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                  
                   
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <!-- <li><a href="{{ route('register') }}">Register</a></li> -->
                        @else
                            <!-- <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span> <span><img src="{{ url('/') }}/icons/010-business.png" style="width: 20px; height: 20px;"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu"  style="z-index:100">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li> -->
                            <li>
                                <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ Auth::user()->name }}, <span><img src="{{ url('/') }}/icons/024-technology-1.png" style="width: 20px; height: 20px;"></span>  LogOut !
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>
<div class="container">
            @yield('content2')
        </div>
    <!-- Scripts -->
    <script src="{{ url('/') }}/plugins/datatables/jquery.js"></script>
    <script src="{{ url('/') }}/plugins/datatables/dataTables.bootstrap.min.js"></script>
     <script src="{{ url('/') }}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ url('/') }}/plugins/datatables/bootstrap.min.js"></script>


    <script src="{{ url('/') }}/js/jquery-1.12.4.js"></script>
    <script src="{{ url('/') }}/js/jquery.dataTables.min.js"></script>
    <script src="{{ url('/') }}/js/dataTables.buttons.min.js"></script>
    <script src="{{ url('/') }}/js/jszip.min.js"></script>
    <script src="{{ url('/') }}/js/pdfmake.min.js"></script>
    <script src="{{ url('/') }}/js/vfs_fonts.js"></script>
    <script src="{{ url('/') }}/js/buttons.html5.min.js"></script>



<!--

    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.4.0/js/dataTables.buttons.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.0/js/buttons.html5.min.js"></script>

-->
        <!-- App scripts -->
        @stack('scripts')
        <script type="text/javascript">
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>

<script type="text/javascript">
    $(document).ready(function()
    {
          $("tr:even").css("background-color","#e5f9f9");

    });
</script>


<!-- <script type="text/javascript">
    $(document).ready(function(){ 
    $('.table').DataTable({
        "paging": true,
            "lengthChange": true,
            "dom": 'Bfrtip',
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
             "buttons": ['copyHtml5', 'excelHtml5', 'csvHtml5', 'pdfHtml5' , 'pageLength', ],
             "lengthMenu": [[10, 25, 50, 100, 250, 500, -1], [10, 25, 50, 100, 250, 500, "All"]],
            "columnDefs": [{
              "defaultContent": "-",
              "targets": "_all"
            }],

    })
});
</script> -->

<script>
    $(document).ready(function () {
            var table = $('#users-table').DataTable({
            "paging": true,
            "lengthChange": true,
            "dom": 'Bfrtip',
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "buttons": ['copyHtml5', 'excelHtml5', 'csvHtml5', 'pdfHtml5', 'pageLength',],
            "lengthMenu": [[10, 25, 50, 100, 250, 500, -1], [10, 25, 50, 100, 250, 500, "All"]],
            "columnDefs": [{
                "defaultContent": "-",
                "targets": "_all"
            }],

        });

            // for each column in header add a togglevis button in the div
            $("#users-table thead th").each(function (i) {
                var name = table.column(i).header();
                var spanelt = document.createElement("button");
                spanelt.innerHTML = name.innerHTML;

                $(spanelt).addClass("colvistoggle");
                $(spanelt).attr("colidx", i);		// store the column idx on the button

                $(spanelt).on('click', function (e) {
                    e.preventDefault();
                    // Get the column API object
                    var column = table.column($(this).attr('colidx'));
                    // Toggle the visibility
                    column.visible(!column.visible());
                });
                $("#colvis").append($(spanelt));
            });
        });
</script>

<!-- <script type="text/javascript">
    $(document).ready(function(){ 
        $('#overlay').fadeOut();
    $('.table').DataTable({
        "paging": true,
            "lengthChange": true,
            "dom": 'Bfrtip',
            "searching": true,
            "responsive": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
             "buttons": ['copyHtml5', 'excelHtml5', 'csvHtml5', 'pdfHtml5' , 'pageLength', ],
             
             "lengthMenu": [
                                [ 10, 25, 50, -1 ],
                                [ '10 rows', '25 rows', '50 rows', 'Show all' ]
                            ],
            "columnDefs": [{
              "defaultContent": "-",
              "targets": "_all",
              "visible" : false,
            }],

    })
});
</script> -->


</body>
</html>

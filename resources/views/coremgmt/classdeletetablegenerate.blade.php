@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
 <div class="panel-heading"> &#10070; {{ $form_heading }}</div>

               
 <div class="panel-body">  <?php $x=""; ?>  <p> <span style="color:red">*</span> Please remember after deleting the class, you have to check the student list.</p> 
        @isset($data)
        <div id="colvis"></div>
        <table class="table table-bordered" id="users-table">
         <tr>  <th>Class Section</th> <th>Description</th><th>class id</th><th>Delete it</th></tr>
          @foreach ($data as $element)
              <tr> 
                  @foreach ($element as $key => $val)
                      @if($key == 'id')
                       <?php $x=$val; ?>
                      @endif
                        <td>  <h5> {{ $val }} </h5> </td> 
                                            
                  @endforeach
                  <td> <a href="{{ url('/') }}/delete-class/{{$x}}" > <button type="button" class="btn btn-success">Delete this Class</button></a> </td>
              </tr>
          @endforeach
          </table>
        @endisset

                </div>
            </div>         
        </div>
    </div>
</div>
@endsection
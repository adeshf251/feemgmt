@extends('layouts.app')

@section('content')
<script src="/public/js/jquery-1.12.2.js"></script>
 <form method="post" action="{{ url('/') }}/new-feetype-submit">
 {{ csrf_field() }}
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading"> &#10070; Add Fee type (Apart from special fee like- academic fee and late fee)</div>

                <div class="panel-body">
    
    <div class="col-md-12">
        <div class="col-md-4">
        <label> Fee Type ex - tution fee.</label>
        </div>
        <div class="col-md-8">
        <input type="text" name="fee_type" class="form-control"/> <br>
        </div>
    </div><br>

    <div class="col-md-12">
        <div class="col-md-4">
        <label> Description about fee</label>
        </div>
        <div class="col-md-8">
        <input type="text" name="description" class="form-control"/> <br>
        </div>
    </div><br>

    <h3>Choose the aplicable Month for the fee</h3> <br>
    <div class="col-md-12">

        <div class="col-md-2">
        <label> Once Required  --> </label>
        </div>
        <div class="col-md-1">
        <input type="checkbox" id="reqonce" name="reqonce" class="form-control">  </input>
        </div>

        <div class="col-md-2">
        <label> Jan --></label>
        </div>
        <div class="col-md-1">
        <input type="checkbox" id="jan" name="jan" class="form-control">  </input><br>
        </div>
        <div class="col-md-2">
        <label> Feb --> </label>
        </div>
        <div class="col-md-1">
        <input type="checkbox" id="feb" name="feb" class="form-control">  </input>
        </div>
        <div class="col-md-2">
        <label> March --> </label>
        </div>
        <div class="col-md-1">
        <input type="checkbox" id="march" name="march" class="form-control">  </input><br>
        </div>
    </div><br>

     <div class="col-md-12">
        <div class="col-md-2">
        <label> April --></label>
        </div>
        <div class="col-md-1">
        <input type="checkbox" id="april" name="april" class="form-control">  </input>
        </div>
        <div class="col-md-2">
        <label> May --> </label>
        </div>
        <div class="col-md-1">
        <input type="checkbox" id="may" name="may" class="form-control">  </input><br>
        </div>
        <div class="col-md-2">
        <label> June --> </label>
        </div>
        <div class="col-md-1">
        <input type="checkbox" id="june" name="june" class="form-control">  </input>
        </div>
        <div class="col-md-2">
        <label> July --> </label>
        </div>
        <div class="col-md-1">
        <input type="checkbox" id="july" name="july" class="form-control">  </input><br>
        </div>
    </div><br>

    <div class="col-md-12">
        <div class="col-md-2">
        <label> Aug --> </label>
        </div>
        <div class="col-md-1">
        <input type="checkbox" id="aug" name="aug" class="form-control">  </input>
        </div>
        <div class="col-md-2">
        <label> Sept --> </label>
        </div>
        <div class="col-md-1">
        <input type="checkbox" id="sep" name="sep" class="form-control">  </input><br>
        </div>
        <div class="col-md-2">
        <label> Oct --> </label>
        </div>
        <div class="col-md-1">
        <input type="checkbox" id="oct" name="oct" class="form-control">  </input>
        </div>
        <div class="col-md-2">
        <label> Nov --></label>
        </div>
        <div class="col-md-1">
        <input type="checkbox" id="nov" name="nov" class="form-control">  </input><br>
        </div>
    </div><br>

    <div class="col-md-12">
        <div class="col-md-2">
        <label> Dec --> </label>
        </div>
        <div class="col-md-1">
        <input type="checkbox" id="dec" name="dec" class="form-control">  </input>
        </div><br>
        </div>
    </div><br> 

    <div class="col-md-12">
        <div class="col-md-4">
        </div>
        <div class="col-md-7">
        <button type="submit" class="btn btn-success col-md-4">Submit/Upload</button>
        </div><br>
        </div>
    </div><br> 

    
    
                </div>
            </div>         
        </div>
    </div>
</div>

</form>          

<script>
$(document).ready(function(){

$("#reqonce").click(function()
                     {  
                     alert('If you click on required once! than all the 12 month will not be taken into consideration') ;
                     }); 
});
</script>


@endsection
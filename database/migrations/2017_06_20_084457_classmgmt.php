<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Classmgmt extends Migration
{
    /**
     * Run the migrations.
     *public $fillable = ['admission_no','class_section','description'];
     * @return void
     */
    public function up()
    {
        Schema::create('classmgmts', function (Blueprint $table) {
           $table->increments('id');
           $table->string('class_section', '100')->unique();
           $table->text('description');
           $table->timestamps();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Feeamount extends Migration
{
    /**
     * Run the migrations.
     *public $fillable = ['fee_type','class_section' ,'amount'];
     * @return void
     */
    public function up()
    {
        Schema::create('feeamounts', function (Blueprint $table) {
           $table->increments('id');
           $table->text('fee_type');
           $table->text('class_section');
           $table->bigInteger('amount');
           $table->timestamps();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

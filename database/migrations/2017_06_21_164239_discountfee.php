<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Discountfee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discountfees', function (Blueprint $table) {
           $table->increments('id');
           $table->text('discountprovider');
           $table->text('discountreason');
           $table->text('discountamount');
           $table->text('discountmonth');
           $table->text('admission_no');
           $table->text('student_name');
           $table->text('class_section');
           $table->text('parent_name');
           $table->text('extra1');
           $table->timestamps();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

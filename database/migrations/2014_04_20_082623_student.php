<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Student extends Migration
{
    /**
     * Run the migrations.
     *public $fillable = ['admission_no','student_name','class_section','parent_name'];
     * @return void
     */
    public function up()
    {
       Schema::create('students', function (Blueprint $table) {
           $table->increments('admission_no');
           $table->text('session');
           $table->text('student_name');
           $table->text('class_section');
           $table->text('father_name');
           $table->text('mother_name');
           $table->text('gender');
           $table->text('address');
           $table->text('mobile');
           $table->text('transport_route');
           $table->text('dob');
           $table->text('status');
           $table->text('priority');
           $table->text('ref');
           $table->text('remark');
           $table->timestamps();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

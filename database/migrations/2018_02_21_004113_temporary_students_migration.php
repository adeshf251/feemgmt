<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TemporaryStudentsMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    // public function up()
    // {
    //     Schema::create('temporarystudents', function (Blueprint $table) {
    //         $table->increments('id');
    //         $table->timestamps();
    //     });
    // }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('temporarystudents', function (Blueprint $table) {
           $table->increments('id');
           $table->text('admission_no')->nullable();
           $table->text('session')->nullable();
           $table->text('student_name')->nullable();
           $table->text('class_section')->nullable();
           $table->text('father_name')->nullable();
           $table->text('mother_name')->nullable();
           $table->text('gender')->nullable();
           $table->text('address')->nullable();
           $table->text('mobile')->nullable();
           $table->text('transport_route')->nullable();
           $table->text('dob')->nullable();
           $table->text('status')->nullable();
           $table->text('priority')->nullable();
           $table->text('ref')->nullable();
           $table->text('remark')->nullable();
           $table->text('email')->nullable();
           $table->text('parent_sessionid')->nullable();
           $table->text('sessionid')->nullable();
           $table->timestamps();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}


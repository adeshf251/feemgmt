<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Feetype extends Migration
{
    /**
     * Run the migrations.
     *public $fillable = ['fee_type','description','once_required','jan','feb','march','april', 'may','june','july','aug','sep','oct','nov','dec'];
     * @return void
     */
    public function up()
    {
        Schema::create('feetypes', function (Blueprint $table) {
           $table->increments('id');
           $table->string('fee_type', '150')->unique();
           $table->text('description');
           $table->text('once_required');
           $table->text('jan');
           $table->text('feb');
           $table->text('march');
           $table->text('april');
           $table->text('may');
           $table->text('june');
           $table->text('july');
           $table->text('aug');
           $table->text('sep');
           $table->text('oct');
           $table->text('nov');
           $table->text('dec');
           $table->timestamps();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

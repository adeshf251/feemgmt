<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FeerecieptStudent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feereciept_students', function (Blueprint $table) {
           $table->increments('rept_no');
           $table->string('admission_no', '100');
           $table->text('student_name');
           $table->text('class_section');
           $table->text('father_name');
           $table->text('fee_months');
           $table->text('feetypearray');
           $table->text('feeamountarray');
           $table->text('discount_provider');
           $table->text('discount_amount');
           $table->text('total_payable');
           $table->text('total_paid');
           $table->text('extradetails');
           $table->timestamps();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

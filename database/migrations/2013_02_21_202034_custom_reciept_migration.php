<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CustomRecieptMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customreciepts', function (Blueprint $table) {
            $table->increments('id');
            $table->text('admission_no')->nullable();
            $table->text('class_section')->nullable();
            $table->text('student_name')->nullable();
            $table->text('father_name')->nullable();
            $table->text('mother_name')->nullable();
            $table->text('address')->nullable();
            $table->text('gender')->nullable();
            $table->text('pay')->nullable();
            $table->text('fee_for')->nullable();
            $table->text('mobile')->nullable();
            $table->text('email')->nullable();
            $table->text('dob')->nullable();
            
            $table->text('fee_name1')->nullable();
            $table->text('fee_value1')->nullable();
            $table->text('fee_name2')->nullable();
            $table->text('fee_value2')->nullable();
            $table->text('fee_name3')->nullable();
            $table->text('fee_value3')->nullable();
            $table->text('fee_name4')->nullable();
            $table->text('fee_value4')->nullable();
            $table->text('fee_name5')->nullable();
            $table->text('fee_value5')->nullable();
            $table->text('remark')->nullable();
            $table->text('parent_sessionid')->nullable();
            $table->text('sessionid')->nullable();
            $table->softDeletes()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customreciepts', function (Blueprint $table) {
            //
        });
    }
}

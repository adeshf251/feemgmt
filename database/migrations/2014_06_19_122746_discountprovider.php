<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Discountprovider extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discountproviders', function (Blueprint $table) {
           $table->increments('id');
           $table->text('discountprovider_name');
           $table->text('discountprovider_position');
           $table->text('discountprovider_department');
           $table->string('discountprovider_email', '100')->unique();
           $table->text('discountprovider_gender');
           $table->text('discountprovider_mb');
           $table->text('extra1');
           $table->timestamps();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FeerecieptSc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feereciept_scs', function (Blueprint $table) {
           $table->increments('rept_no');
           $table->string('admission_no', '100');
           $table->text('student_name');
           $table->text('class_section');
           $table->text('father_name');
           $table->text('fee_months');
           $table->string('month_admno_code', '100')->unique();
           $table->text('requireonce_included');
           $table->text('requireonce_amount');
           $table->text('discount_included');
           $table->text('discount_given');
           $table->text('discount_total_offered');
           $table->text('transportation_included');
           $table->text('transportation_amount');
           $table->text('total_payable');
           $table->text('total_paid');
           $table->text('payment_mode');
           $table->text('feetype_array');
           $table->text('feeamountarray');
           $table->text('associated_reciept_array');
           $table->text('extradetails');
           $table->timestamps();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

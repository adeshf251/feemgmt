<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(DELLMOND\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(DELLMOND\student::class, function (Faker\Generator $faker) {
  
    return [
        'student_name' => $faker->name,
        'class_section' => rand(1,12),
        'parent_name' => $faker->name,
        'dob' => rand(1,31)."/".rand(1,12)."/".rand(1990,2007),
    ];
});

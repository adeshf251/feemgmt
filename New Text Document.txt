ALTER TABLE `feereciept_scs` ADD `dd_no` TEXT NULL AFTER `payment_mode`;
ALTER TABLE `feereciept_scs` ADD `dd_date` TEXT NULL AFTER `dd_no`;
ALTER TABLE `feereciept_scs` ADD `dd_bank` TEXT NULL AFTER `dd_date`;
ALTER TABLE `feereciept_scs` ADD `dd_branch` TEXT NULL AFTER `dd_bank`;
ALTER TABLE `feereciept_scs` ADD `dd_amount` TEXT NULL AFTER `dd_branch`;
ALTER TABLE `feereciept_scs` ADD `dd_verify` TEXT NULL AFTER `dd_amount`;
ALTER TABLE `feereciept_scs` ADD `cheque_no` TEXT NULL AFTER `dd_verify`;
ALTER TABLE `feereciept_scs` ADD `cheque_date` TEXT NULL AFTER `cheque_no`;
ALTER TABLE `feereciept_scs` ADD `cheque_bank` TEXT NULL AFTER `cheque_date`;
ALTER TABLE `feereciept_scs` ADD `cheque_branch` TEXT NULL AFTER `cheque_bank`;
ALTER TABLE `feereciept_scs` ADD `cheque_amount` TEXT NULL AFTER `cheque_branch`;
ALTER TABLE `feereciept_scs` ADD `cheque_verify` TEXT NULL AFTER `cheque_amount`;

ALTER TABLE `feereciept_scs` CHANGE `requireonce_included` `requireonce_included` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `feereciept_scs` CHANGE `requireonce_amount` `requireonce_amount` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `feereciept_scs` CHANGE `discount_included` `discount_included` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `feereciept_scs` CHANGE `discount_given` `discount_given` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `feereciept_scs` CHANGE `discount_total_offered` `discount_total_offered` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `feereciept_scs` CHANGE `discountprovider` `discountprovider` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `feereciept_scs` CHANGE `discountreason` `discountreason` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `feereciept_scs` CHANGE `transportation_included` `transportation_included` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `feereciept_scs` CHANGE `transportation_amount` `transportation_amount` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `feereciept_scs` CHANGE `total_payable` `total_payable` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;

ALTER TABLE `feereciept_students` ADD `payment_mode` VARCHAR(500) NOT NULL DEFAULT 'cash' AFTER `father_name`;
ALTER TABLE `feereciept_students` CHANGE `payment_mode` `payment_mode` VARCHAR(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'cash';

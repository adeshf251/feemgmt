<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/





// main routes
Route::get('/', function () {  return view('welcome2');  });
Route::get('admin/login', 'AditionalSetting@getAdminLogin');
Route::post('admin/login', 'AditionalSetting@adminLogin');
// administrator authentication and management (apart from normal)
Route::group(['middleware' => 'admin'], function()
{
	Route::get('/admin-controller', function () {  return view('admin.admin-home');  });
});


// starting authentication user
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/sessionchoose', 'HomeController@selectSession')->name('sessionchoose');
Route::post('/change-session-submit', 'HomeController@switchSession');

Route::group(['middleware' => 'auth'], function()
{


// fee management routes
Route::get('/search-student', 'feecontroller@searchstudentreq' );
Route::post('/search-student-submit', ['uses'=>'feecontroller@searchstudentreqsubmit', 'as' => 'f.searchstudent',]);
Route::get('/payfee/{admission_no}', 'feesubmitter@payselectedstudent');
Route::post('/payfees-confirm-payment', 'feesubmitter@confirmpayment');
Route::post('/payfees-confirm-payment-commit', 'feesubmitter@confirmpaymentcommit');
Route::any('/Reprint-fee-Reciept-request', 'reprintrecieptcontroller@reprintrequest' );
Route::any('/Reprint-fee-Reciept/{id}', 'reprintrecieptcontroller@reprint' );
Route::post('/payfees-confirm-dd-payment', 'feesubmitter@confirm_dd_payment');
Route::post('/payfees-confirm-dd-payment-commit', 'feesubmitter@confirm_dd_paymentcommit');

Route::post('/payfees-confirm-cheque-payment', 'feesubmitter@confirm_cheque_payment');
Route::post('/payfees-confirm-cheque-payment-commit', 'feesubmitter@confirm_cheque_paymentcommit');



// delete fee reciept with password of admin
Route::get('/delete-paid-fee-verify-admin', 'RecieptDeleteController@verifyadmin');
Route::post('/delete-paid-fee-verify-admin-submit', 'RecieptDeleteController@verifyadminpassword');
Route::get('/delete-paid-fee', 'RecieptDeleteController@deletepaidfeereq');
Route::any('/delete-paid-fee-list', 'RecieptDeleteController@deletepaidfeelistgenerate');
Route::get('/delete-paid-fee-reciept/{month}/{x}', 'RecieptDeleteController@deletepaidreciept');





// student section controller
Route::get('/new-registration', 'studentcontroller@addstudentrequest');
Route::post('/reg-submit', 'studentcontroller@insert');
Route::get('/new-registration-excel', 'studentcontroller@addstudentrequestexcel');
Route::post('/reg-submit-excel', 'studentcontroller@insertexcel');

Route::get('/update-student-detail-request/{limit}', function(){
     return view('student.updatestudentform_search');
});
Route::any('/update-student-detail-request-submit', 'studentcontroller@searchstudentreqsubmit_for_update');
Route::any('/update-student-commit', 'studentcontroller@updatestudentdetails_req_submit');

Route::get('/classwise-student-list', 'datatablestudentclasswisecontroller@classwiselistreq');
Route::any('/classwise-student-form-submit', 'datatablestudentclasswisecontroller@submitform' );
Route::any('/classwise-student-list/{id}', 'datatablestudentclasswisecontroller@Index' );
Route::any('/student-delete/{id}', 'datatablestudentclasswisecontroller@deletestudent' );
Route::any('/classwise-student-list-ajax/{id}', 'datatablestudentclasswisecontroller@ajaxresponse' );
Route::any('/classwise-student-list-export-xlsx', 'datatablestudentclasswisecontroller@exportexcel_xlsx' );
Route::any('/classwise-student-list-export-csv', 'datatablestudentclasswisecontroller@exportexcel_csv' );

Route::get('/getPostsstudent', 'studentcontroller@getPosts' )->name('student.data'); // table target through ajax
Route::get('/complete-student-list', 'studentcontroller@CompleteStudentList' );


Route::get('/search-student-details', 'studentcontroller@searchstudentreq' );
Route::post('/get-student-details', ['uses'=>'studentcontroller@searchstudentreqsubmit', 'middleware'=>'auth', 'as' => 'f.studetails',]);
Route::resource('student', 'studentcontroller', [
    'anyData'  => 'student.data',
    'getIndex' => 'student',
]);








// core management section routes
Route::get('/new-class-reg', 'classcontroller@addclassrequest');
Route::get('/class-delete-req', 'classcontroller@deleteclassrequest');
Route::get('/delete-class/{id}', 'classcontroller@deleteclass');
Route::get('/add-fee-type', 'feecontroller@addfeetype');
Route::get('/new-class-reg', 'classcontroller@addclassrequest');
Route::post('/new-feetype-submit', ['uses'=>'feecontroller@addfeetypesubmit', 'middleware'=>'auth', 'as' => 'f.addfeetype',]);
Route::get('/remove-fee-type-request', 'feecontroller@deletefeetyperequest');
Route::get('/remove-fee-type-submit/{id}', 'feecontroller@deletefeetyperequestsubmit');
Route::get('/update-fee-value', 'feecontroller@updatefeerequest');
Route::post('/update-fee-value/{id}', 'feecontroller@updatefeerequestsubmit');
Route::get('/remove-fee-value', 'feecontroller@removefeerequest');
Route::post('/remove-fee-value/{id}', 'feecontroller@removefeerequestsubmit');
Route::get('/fee-value-registration-classwise', 'feeamountcontroller@addreq_form' );
Route::get('/fee-value-registration-classwise-submit', 'feeamountcontroller@addreq_form_submit' );







// Discount section routes
Route::get('/discount-provider-registration', 'discountprovidercontroller@addreq_form' );
Route::post('/discount-provider-registration-submit', 'discountprovidercontroller@addreq_form_submit' );
Route::get('/discount-provider-details-request', 'discountprovidercontroller@requestprovideramountdetails' );
Route::post('/discount-provider-details-request-submit', 'discountprovidercontroller@requestprovideramountdetailssubmit' );
Route::get('/discount-beneficiary-provider', 'discountprovidercontroller@discountbeneficiaryprovider' );
Route::get('/discount-beneficiary-provider-delete/{id}', 'discountprovidercontroller@deleteprovider' );
Route::get('/discount-beneficiary-student', 'discountprovidercontroller@discountbeneficiarystudent' );







// Defaulter section routes
Route::get('/monthwise-defaulter', 'DefaulterController@defaulter_monthwise_index' );
Route::post('/monthwise-defaulter-list', 'DefaulterController@defaulter_monthwise_show' );

Route::get('/overall-classwise-defaulter', 'DefaulterController@overall_classwise' );
Route::post('/overall-classwise-defaulter', 'DefaulterController@overallClasswiseSubmit' );
Route::get('/overall-yearly-defaulter-list', 'DefaulterController@overallDefaulter' );
// Route::get('/monthwise-defaulter', 'DefaulterController@defaulter_monthwise_index' );

Route::any('/monthwise-defaulter-list-export-xlsx/{month}', 'defaulter@exportexcel_monthwise_xlsx' );
Route::any('/monthwise-defaulter-list-export-csv/{month}', 'defaulter@exportexcel_monthwise_csv' );
Route::any('/monthwise-classwise-defaulter-list-export-xlsx/{month}/{cl}', 'defaulter@exportexcel_monthwise_classwise_xlsx' );
Route::any('/monthwise-classwise-defaulter-list-export-csv/{month}/{cl}', 'defaulter@exportexcel_monthwise_classwise_csv' );
Route::get('/classwise-defaulter', 'DefaulterController@defaulter_classwise_index' );
Route::post('/classwise-defaulter-list', 'DefaulterController@defaulter_classwise_show' );

Route::get('/list-defaulter', 'defaulter@overall' );





// collection Amount section routes
Route::get('/collection-datewise', 'CollectionController@index' );
Route::post('/collection-datewise-submit', 'CollectionController@collectidatewise' );
Route::get('/collection-monthtwise-or-feetype', 'CollectionController@collection_monthwise_index' );
Route::post('/collection-monthtwise-or-feetype-submit', 'CollectionController@collection_monthwise_submit' );
Route::get('/collection-by-student-request', 'CollectionController@collection_student_index' );
Route::post('/collection-student-submit', 'CollectionController@collection_student_submit' );
Route::get('/collection-datewise-transport', 'CollectionController@transport_index' );
Route::post('/collection-datewise-transport-submit', 'CollectionController@collectidatewisetransport_submit' );
Route::get('/collection-by-student-transport-request', 'CollectionController@collection_student_transport_index' );
Route::post('/collection-student-transport-submit', 'CollectionController@collection_student_transport_submit' );




// Pendency Amount section routes
Route::get('/pendency-amt-classwise',        'PendencyController@classwise_pendency_index' );
Route::post('/pendency-amt-classwise-submit', 'PendencyController@classwise_pendency_show' );
Route::get('/pendency-amt-monthwise',        'PendencyController@monthwise_pendency_index' );
Route::post('/pendency-amt-monthwise-submit', 'PendencyController@monthwise_pendency_show' );
Route::get('/pendency-amt-overall-submit', 'PendencyController@overall_pendency_show' );
// Route::get('/ab', 'PendencyController@ab' );
Route::get('/pendency-amt-studentwise',        'PendencyController@studentwise_pendency_index' );
Route::post('/pendency-amt-studentwise-submit', 'PendencyController@studentwise_pendency_show' );
Route::get('/pendency-amt-once-req-fee',        'PendencyController@once_req_fee_pendency_index' );
Route::post('/pendency-amt-once-req-fee-submit', 'PendencyController@once_req_fee_pendency_show' );






// transport section routes
Route::get('/insert-transport-routes-request', 'transportcontroller@insert_route_request' );
Route::post('/insert-transport-routes-request-submit', 'transportcontroller@insert_route_request_submit' );
Route::get('/update-transport-routes-request', 'transportcontroller@update_route_request' );
Route::post('/update-transport-routes-request-submit', 'transportcontroller@update_route_submit' );
Route::get('/delete-transport-routes/{id}', 'transportcontroller@delete' );

Route::get('/transport-user-list', 'transportcontroller@transportuserlist' );


// Guest Students routes
Route::get('/temporary-student/create', 'TemporaryStudents@create' );
Route::resource('temporary-student', 'TemporaryStudents');
Route::get('temporary-student-collection', 'TemporaryStudents@displayCollection');
Route::post('temporary-student-collection', 'TemporaryStudents@displayCollectionSubmit');
Route::get('temporary-student-delete/{id}', 'TemporaryStudents@destroy');

Route::resource('custom-reciept', 'CustomRecieptController');


//*********************************** Mail *************************
Route::get('/sendmailform', 'SendMail@sendmailreq');
Route::post('/sendmailsubmit', 'SendMail@sendmailsubmit');

// Route::group(['middleware' => ['admin']], function () {


});  // ending auth route group here





Route::group(['middleware' => ['admin']], function () {
	Route::get('admin/dashboard', ['as'=>'admin.dashboard','uses'=>'AdminLogin@dashboard']);

	Route::any('/upgrade',        	        'UpgradeController@index' );
	Route::any('/upcomingsession',        	 'UpgradeController@setupcomingsession' );
	Route::any('/upgradeclassselect',        	 'UpgradeController@showclasslist' );
	Route::any('/upgradestudentform',        	 'UpgradeController@showclasslistgenerate' );
	Route::any('/upgradestudentsubmit',        	 'UpgradeController@upgradestudents' );
	
	Route::any('/upgradeclasssubmit',        	 'UpgradeController@upgradeClassStructure' );
	Route::any('/upgradediscountprovidersubmit',        	 'UpgradeController@upgradeDiscountProvider' );
	Route::any('/upgradefeeamountssubmit',        	 'UpgradeController@upgradeFeeAmounts' );
	Route::any('/upgradefeetypessubmit',        	 'UpgradeController@upgradeFeeTypes' );
	Route::any('/upgradetransportsubmit',        	 'UpgradeController@upgradeTransports' );
	


	Route::get('/rept-password-reset',     'AditionalSetting@passwordReset' );
	Route::post('/rept-password-reset-submit',     'AditionalSetting@passwordResetSubmit' );

	Route::get('/showallusers',     'AditionalSetting@showusers' );
	Route::get('/adduserform',     'AditionalSetting@adduserform' );
	Route::post('/addusersubmit',     'AditionalSetting@adduser' );

	Route::get('/showalladmins',     'AditionalSetting@showadmins' );
	Route::get('/addadminform',     'AditionalSetting@addadminform' );
	Route::post('/addadminsubmit',     'AditionalSetting@addadmin' );
	});





Route::group(['middleware' => 'auth'], function()
{




// SessionManager Section Route for Admin
Route::get('/session-create',                'SessionManagerController@createIndex' );
Route::post('/session-create-submit',        'SessionManagerController@createSubmit' );
Route::get('/session-read',                  'SessionManagerController@readIndex' );
Route::post('/session-update',               'SessionManagerController@updateIndex' );
Route::post('/session-update-submit',        'SessionManagerController@updateSubmit' );
Route::get('/session-delete/{id}',           'SessionManagerController@delete' );
Route::get('/session-activate/{id}',           'SessionManagerController@activate' );


Route::get('/admin-new-user',                'AditionalSetting@createNewUserReq' );
Route::get('/admin-new-user-submit',                'AditionalSetting@createNewUserSubmit' );
Route::get('/admin-password-reset',                'AditionalSetting@createPasswordResetReq' );
Route::get('/admin-password-reset-submit',                'AditionalSetting@createPasswordResetSubmit' );
Route::get('/admin-user-delete',                'AditionalSetting@deleteUser' );
Route::get('/admin-user-showall',                'AditionalSetting@showAllUser' );

Route::get('/admin-send-sms',                'AditionalSetting@sendSMSreq' );
Route::get('/admin-send-sms-submit',                'AditionalSetting@sendSMSSubmit' );
Route::get('/admin-send-email',                'AditionalSetting@sendEmailreq' );
Route::get('/admin-send-email-submit',                'AditionalSetting@sendEmailSubmit' );


});  // ending auth route group here




Route::any('/testt', function () {
    echo date('Y-m-d H:i:s');
                            });





// Session Archive and Backup section routes
    // coming soon

Route::get('/contact-us',  function () {  return view('contact_us');  });
Route::get('/{__missing1}',  function () {  return view('pagenotfound');  });
Route::get('/{__missing1}/{__missing2}',  function () {  return view('pagenotfound');  });
Route::get('/{__missing1}/{__missing2}/{__missing3}',  function () {  return view('pagenotfound');  });
Route::get('/{__missing1}/{__missing2}/{__missing3}/{__missing4}',  function () {  return view('pagenotfound');  });
Route::get('/{__missing1}/{__missing2}/{__missing3}/{__missing4}/{__missing5}', function (){ return view('pagenotfound');});


















// testing routes

Route::post('/class-submit', ['uses'=>'classcontroller@insert', 'middleware'=>'auth', 'as' => 'f.regclass',]);

Route::group(['middleware'=>'auth',
             'as'=>'admin-1'], function(){
                Route::get('/abc', function () { return view('classmgmt'); });
             });

Route::get('class/{id}', 'classcontroller@delete');
Route::post('/submited', ['uses'=>'classcontroller@insert', 'as' => 'f.usersubmitform',]);


Route::get('/a', 'studentcontroller@exportUserList')->name('home');

Route::get('/trans', 'transportlistparse@insertexcel');



Route::resource('classes', 'classcontroller', [
    'anyData'  => 'classes.data',
    'getIndex' => 'classes',
]);
Route::get('/getPosts', 'classcontroller@getPosts' )->name('classes.data'); // table target through ajax


// student section 
// main routes




//Route::any('/tester', 'errorcontroller@tester' ); - to save value to student table fron sc_student table

